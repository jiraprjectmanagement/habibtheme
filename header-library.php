﻿<?php
/**
 * Header template for Library
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/tab.min.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tab.min.js"></script>


<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157746106-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-157746106-1');
</script>


</head>
<body <?php body_class(); ?>>
	
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                <?php if(is_front_page()) : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png">
                <?php else : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png">
                <?php endif; ?>
                </a>
                </div>
                <div id="nav-trigger">
                        <div>- <span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></div>
                </div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                        <div class="search_mid_nav"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'mid_nav' ) ); ?> </div>
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav_inner' ) ); ?>
                
      
                <div class="search">
                <div class="search_mid_nav"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'mid_nav' ) ); ?> </div>
                	<?php
						
						//if ( is_user_logged_in() ) {
					?>
                    <?php		
						//}
						
					?>
                    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                       
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" id="s"/>
                            <button type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search"/><i class="fa fa-search"></i></button>
                    </form>
                </div>
                </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i>---</i>≡</div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
	</div><!-- #header -->
	<div id="main" class="library">
    	
     