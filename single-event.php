<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<div id="content" role="main" class="singleEvent">

			<?php
			/*
			 * Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
//			get_template_part( 'loop', 'single' );
			?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="nav-above" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
				</div><!-- #nav-above -->

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                	<?php $imgurl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                    
                        <div class="featuredimg"><img src="<?php echo $imgurl; ?>"></div>
                  	<div class="eventPad">     
                        <h1 class="entry-title"><?php the_title(); ?></h1>
					</div>
					<?php /*?><div class="entry-meta">
						<?php twentyten_posted_on(); ?>
					</div><!-- .entry-meta --><?php */?>
                    <?php /*if(has_post_thumbnail() && is_singular('event')){ ?>
                        <div class="featuredimg">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <img src="<?php echo $url; ?>"  />
                        </div>
					<?php }*/ ?>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'habib' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->
					<div class="entry-utility">
						<?php // twentyten_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'habib' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-## -->

				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'habib' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'habib' ) . '</span>' ); ?></div>
				</div><!-- #nav-below -->

				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>
			</div><!-- #content -->
			<?php get_sidebar('event'); ?>
		</div><!-- #container -->


<?php get_footer(); ?>
