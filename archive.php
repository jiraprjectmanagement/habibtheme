<?php
/**
 * Template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

// if(is_post_type_archive('arts-faculty')) {
// 	header('Location: http://habib.edu.pk/academics/ahss/faculty/');
// }
// if(is_post_type_archive('science-faculty')) {
// 	header('Location: http://habib.edu.pk/academics/sse/faculty/');
// }

// get_header(''); ?>

<?php get_header('with-megamenu-live'); ?>

<style>

#main .widget-container{
	display:none;
}

.entry-summary p a {
    display: none;
}
.addtoany_shortcode {
    display: inline;
}

#primary {
    
    margin-top: 0px;
}

.page-title{
	padding-left: 15px;
    padding-right: 15px;
	margin-top: 20px !important;
    font-size: 35px;
}

.contenthead{
	padding-left: 15px;
    padding-right: 15px;
	margin-top: 25px;
}

#content .entry-title{
	font-size:18px;
}

.entry-title a:active, .entry-title a:hover {
    color: #5e2568;
}

.entry-summary a {
    display: none;
}

#content{

	background-color : #fbfbfb !important
}

div#primary.widget-area {
    display: none;
}

.search_mid_nav{
	width:800px;
}

#wrapper .search input#s{
	margin-top: 6px;
 	height: 24px;
}
</style>
		<div id="container">
			<div id="content" role="main">

<?php
	/*
	 * Queue the first post, that way we know
	 * what date we're dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */

	 
	if ( have_posts() )
		the_post();
?>

			<h1 class="page-title">
<?php if ( is_day() ) : ?>
				<?php printf( __( 'Daily Archives: <span>%s</span>', 'twentyten' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( 'Monthly Archives: <span>%s</span>', 'twentyten' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyten' ) ) ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( 'Yearly Archives: <span>%s</span>', 'twentyten' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyten' ) ) ); ?>
<?php else : ?>
				<?php _e( 'Administrative Job Positions', 'twentyten' ); ?>
				
<?php endif; ?>
			</h1>

			<p class="contenthead"> Habib University is seeking applicants for the following positions: </p>

<?php
	/*
	 * Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/*
	 * Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archive.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'archives' );
?>

	<p style="padding-left:15px; padding-right:15px;"> To apply for any of the positions mentioned above, kindly fill in the Application Form and submit it along with your most recent resume. You can also fill in the Application Form if you feel that our current staff openings are not relevant to your qualifications or expertise. We will be keeping your information in our database for a period of 12 months and contact you whenever there is an opening that matches your profile. Thank you for showing interest in joining Habib University!

</p>

			</div><!-- #content -->
			
		
	<div id="primary" class="widget-area jobs-sidebar" style="display:block;" role="complementary">
		<ul class="tb_side huit_wid career_wid">
		<?php dynamic_sidebar( 'left_sidebar' ); ?>
		</ul>
	</div>

</div><!-- #container -->

<?php get_sidebar(); ?>

<?php //get_footer(); ?>

<?php get_footer('footer-live'); ?>



<script>
var ret = document.title.replace('Archive','');
document.title = ret;
</script>
