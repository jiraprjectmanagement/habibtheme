<?php
/**
 * Template for displaying Working Paper Series 
 *
 */

get_header(''); ?>

		<div id="container" class="workingPaperSeries">
        	<div id="wps_feature">
            	<?php
					$image_or_slider = get_field('image_or_slider', 'option');
				if($image_or_slider == 'image') { ?>
                	<img src="<?php the_field('feature_image', 'option'); ?>" />
                <?php } elseif($image_or_slider == 'slider') {  
					$revslider = get_field('revolution_slider_code', 'option');
					echo do_shortcode($revslider);
				} ?>
                <?php /*?><img src="<?php bloginfo('template_url'); ?>/images/wps_header.jpg" /><?php */?>
            </div>
			<div id="content" role="main">
            	
<?php
	if ( have_posts() )
		the_post();
?>

			<h1 class="page-title">
<?php if ( is_day() ) : ?>
				<?php printf( __( 'Daily Archives: <span>%s</span>', 'habib' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( 'Monthly Archives: <span>%s</span>', 'habib' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'habib' ) ) ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( 'Yearly Archives: <span>%s</span>', 'habib' ), get_the_date( _x( 'Y', 'yearly archives date format', 'habib' ) ) ); ?>
<?php else : ?>
				<?php _e( 'Working Paper Series', 'habib' ); ?>
<?php endif; ?>
			</h1>
			<?php 
			$wps_description = get_field('wps_description', 'option');
			if($wps_description){ ?> 
            <div class="wps_description">
            	<?php the_field('wps_description', 'option'); ?>
            </div>
            <?php } ?>
<?php
	/*
	 * Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/*
	 * Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archive.php and that will be used instead.
	 */
	// get_template_part( 'loop', 'archive' );
	
	while ( have_posts() ) : the_post();
?>
	
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        	<div class="entry-meta">
            	<span class="icn_cal"></span>
                <span class="date"><?php echo get_the_date('d/m/Y'); ?></span>
			</div><!-- .entry-meta -->
            <div class="wps_info">
                <h2 class="entry-title">
                <?php 
                /*if(get_field("pdf_upload")) { ?>
                    <a href="<?php the_field('pdf_upload'); ?>" rel="bookmark"><?php the_title(); ?></a>
    			<?php 
				} else {
                    the_title();
                }*/ ?>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h2>
                <?php
                $rows = get_field('author');
                $numRows = count($rows);
                $numCount = 0;
                foreach ($rows as $row) {
                    echo '<a href="'.$row[author_link].'">'.$row[author_name].'</a>';
                $numCount++;	
                    if($numRows != $numCount){
                        echo ", ";
                    }
                }
				?>
                <div class="wps_abstract">
                	<a class="mwabs" href="#">Abstract</a>
                  	<div class="wps_abstractHide">
                		<?php the_excerpt(); ?>
                    </div>
                </div>	
			</div>
        </div>    
<?php endwhile; ?>
			</div><!-- #content -->
 	        <div id="primary" class="widget-area">
            	<div class="wpsUlCat">
                    <h3>Categories</h3>
                    <ul>
                        <?php
                            $taxonomy = 'wps_categories';
                            $tax_terms = get_terms($taxonomy);
                            foreach ($tax_terms as $tax_term) {
                                echo '<li>' . '<a class="wps_cat" href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '><span class="tag_name">' . $tax_term->name.'</span></a></li>';
                            } ?> 
                                
                    </ul>
                </div>
                <div class="wpsArchive">
                	<h3>By Year</h3>
                    <ul>
                    	<?php
                            $taxonomy = 'wps_archives';
                            $tax_terms = get_terms($taxonomy);
//							var_dump($tax_terms);
                            foreach ($tax_terms as $tax_term) {
                                echo '<li>' . '<a class="wps_cat" href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '><span class="tag_name">' . $tax_term->name.'</span></a></li>';
                            } ?> 
                    </ul>
                </div>
            </div><!-- #primary .widget-area -->
            
            <?php  // endif; ?>          
		</div><!-- #container -->

<?php get_footer(); ?>
<script>
jQuery('.wps_abstract a.mwabs').on('click', function(e) {
	e.preventDefault();
	jQuery(this).next('.wps_abstractHide').toggle('slow');
});
</script>