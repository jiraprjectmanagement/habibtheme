<?php
/*
 Template Name: Student Affair 2018 temp
*/

 //get_header('oge'); ?>

<?php  get_header('with-megamenu-live'); ?>


<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/oge.css" />
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/ogehasan.css" /> 
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/tab.min.css" />
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css">

<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet"/>
<link href="<?php //echo esc_url( get_template_directory_uri() ); ?>/css/lity.css" rel="stylesheet"/>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tab.min.js"></script>

<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>

<!-- <div id="header"></div> -->
   <div id="container_missions" > 
			<div id="content_stdaffairs" role="main" class="adm_inner" >
                    
                    <?php
        			if ( have_posts() ) :
        				while (have_posts()) : the_post();
                        	the_content();
                   		endwhile;
        			endif; ?>
			       
            </div> 
           
         <div id="primary_stdaffairs" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs1" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_1_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs2" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_2_sidebar' ); ?>
         </div> 
            
            
    </div>

 

<?php // get_footer(); ?>
<?php get_footer('footer-live'); ?>