<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div role="complementary" class="widget-area" id="primary">
			

      <h3 class="widget-title side"><a href="http://habib.edu.pk/academics/sse/faculty/">DSSE Faculty</a></h3>
      <ul class="tb_side">
         <?php query_posts("post_type=science-faculty&posts_per_page=30&orderby=menu_order&order=ASC"); 
				   if (have_posts() ) : while (have_posts() ) : the_post(); { ?>
        	<li class="page_item page-item-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            
		<?php }  endwhile; endif;  wp_reset_query();?>	
      </ul>
</div>
<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'newsibar' ) ) : ?>
		<div id="primary2" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'newsibar' ); ?>
		</div>
<?php endif; ?>
<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
