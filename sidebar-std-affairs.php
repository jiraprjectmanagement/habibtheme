<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'std_affairs_sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'std_affairs_sidebar' ); ?>
<?php
	endif; 
?>
</div>