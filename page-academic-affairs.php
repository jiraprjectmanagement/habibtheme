<?php
/*
 Template Name: Academic Affairs
*/

get_header('academic-affairs'); ?>


<script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
-->

</script>
    <div class="container">
        <div class="oaf_content">
			<?php	
			if ( have_posts() ) :
            	while (have_posts()) : the_post();
					the_content();
            	endwhile;
			endif; ?>
         
        </div>
        
      <!--  <div class="sideBar">
           <?php get_sidebar('president-office'); ?>
        </div>-->
        
    </div>

	
<?php get_footer('hpv1footer'); ?>

<script>
var video = document.getElementById('vid');

video.play();
console.log(video)
</script>
