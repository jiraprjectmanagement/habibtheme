<?php
/*
 Template Name: Admissions-inner-sep-21
*/

// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<!-- <div id="header"></div> -->
   <div id="container_missions" >
		
              <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
               <!-- <img src="<?php echo $url ?>" /> -->
         <div class="featured-image" style="background-image: url('<?php echo $url ?>');">
            <div class="featured-image-inner">
               <div class="breadcrum-tag-main">
                  <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
                  <div class="new-add-btn">
					  <!-- <a class="btn_new_admissions_sep" href="#">Undergraduate Program</a> -->
					</div>
               </div>
               <h1 class="entry-title"><?php echo get_the_title();?></h1>
            </div>
         </div>
         <div id="menu-sticky-bar" class="menu-sticky-bar sep-add-sticky-menu" role="complementary">
            <!-- <?php//dynamic_sidebar( 'admission_sidebar' ); ?> -->
			<div class="sticky-logo"><a href="https://habib.edu.pk" title="Habib University"><img src="https://habib.edu.pk/wp-content/themes/habib/images/sticky-logo.png" alt="Habib-logo" class="img-sticky-logo"></a></div>
			         <?php 
                    $parentpageid = wp_get_post_parent_id( get_the_ID() ) ;
                   if($parentpageid == 266052) {
                       wp_nav_menu( array( 'container_class' => 'sticky-menu-header', 'theme_location' => 'admission-sep-hutops' ) ); 	}
			            else
		            	{		wp_nav_menu( array( 'container_class' => 'sticky-menu-header', 'theme_location' => 'admission-sep-regular' ) );   	}
					?>
					<div class="sticky-search"><form role="search" method="get" id="searchform" action="https://habib.edu.pk/"><input type="text" value="" name="s" id="s"><input type="submit" id="searchsubmit" value="Search"></form></div>
         </div>
      <div id="content" role="main" class="adm_inner" >
                    <?php
        			if ( have_posts() ) :
        				while (have_posts()) : the_post();
                        	the_content();
                   		endwhile;
        			endif; ?>
			       
      </div>
   </div>

   <button onclick="topFunction()" id="minorstopBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button><script>var mybutton = document.getElementById("minorstopBtn");window.onscroll = function() {scrollFunction()};
function scrollFunction() { if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 500;
}
</script>

 <script>$("#accordion").accordion({ collapsible: true, active: false });
</script>
  <!------------------------------------------ for first Tab ACtive -----------------------------
   <script>jQuery("ul.arconix-tabs li a:not(:first)").addClass("inactive");
			jQuery(".arconix-panes .arconix-pane").hide();
			jQuery(".arconix-panes .arconix-pane:first").show();
			jQuery("ul.arconix-tabs li a").click(function(){
				jQuery("ul.v-arconix-tabs a:not(:first)").addClass("inactive");
				jQuery("ul.v-arconix-tabs").find("a:first").removeClass("inactive");
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery(".v-arconix-panes .v-arconix-pane:first-child").show();
				
				var t = jQuery(this).attr("id");
				jQuery("ul.arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".arconix-panes .arconix-pane").hide();
				jQuery("#pane-"+ t ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition 
				jQuery("ul.arconix-tabs li a").addClass("inactive");         
				jQuery(this).removeClass("inactive");
				jQuery(".arconix-panes .arconix-pane").hide();
				jQuery(t).fadeIn("slow");    
			}
</script>
<script>jQuery("ul.v-arconix-tabs li a:not(:first)").addClass("inactive");
			jQuery(".v-arconix-panes .v-arconix-pane").hide();
			jQuery(".v-arconix-panes .v-arconix-pane:first-child").show();
			jQuery("ul.v-arconix-tabs li a").click(function(){
				var v = jQuery(this).attr("id");
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery("#v-pane-"+ v ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition 
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");         
				jQuery(this).removeClass("inactive");
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery(t).fadeIn("slow");    
			}
</script>  -->
<!------------------------------------------ for Second Tab ACtive ----------------------------->
	<script>
			jQuery("ul.arconix-tabs li a:not(:first)").addClass("inactive");
			jQuery(".arconix-panes .arconix-pane").hide();
			jQuery(".arconix-panes .arconix-pane:first").show();
			jQuery("ul.arconix-tabs li a").click(function(){
				jQuery("ul.v-arconix-tabs a:not(:nth-child(2))").addClass("inactive");
				jQuery("ul.v-arconix-tabs li:nth-child(4) a").removeClass("inactive");
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery(".v-arconix-panes .v-arconix-pane:nth-child(2)").show();
				
				var t = jQuery(this).attr("id");
				jQuery("ul.arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".arconix-panes .arconix-pane").hide();
				jQuery("#pane-"+ t ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition 
				jQuery("ul.arconix-tabs li a").addClass("inactive");         
				jQuery(this).removeClass("inactive");
				jQuery(".arconix-panes .arconix-pane").hide();
				jQuery(t).fadeIn("slow");    
			}
	</script>
	<script>
jQuery("ul.v-arconix-tabs li a:not(:nth-child(2))").addClass("inactive");
jQuery("ul.v-arconix-tabs li:nth-child(4) a").removeClass("inactive");
			jQuery(".v-arconix-panes .v-arconix-pane").hide();
			jQuery(".v-arconix-panes .v-arconix-pane:nth-child(2)").show();
			jQuery("ul.v-arconix-tabs li a").click(function(){
				var v = jQuery(this).attr("id");
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery("#v-pane-"+ v ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");  
				jQuery(this).removeClass("inactive");
				jQuery(".v-arconix-panes .v-arconix-pane").hide();
				jQuery(t).fadeIn("slow");    
			}
</script>
<!------------------------------------------ End for Second Tab ACtive ----------------------------->
<script>
jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()>300)
    {
    //   jQuery('.menu-sticky-bar').show(500);
	jQuery('.menu-sticky-bar').slideDown(500);
    }
    else
    {
		// jQuery('.menu-sticky-bar').hide(500);
		jQuery('.menu-sticky-bar').slideUp();

    }
});
</script>
<script>
jQuery('.drop-down-show-hide').hide();
jQuery('.drop-down-show-hide:first').show();
jQuery('#dropDown').change(function () {
    jQuery('.drop-down-show-hide').hide()    
    jQuery('#' + this.value).show();

});
</script>
<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>



