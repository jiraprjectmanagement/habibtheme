<?php
/*
 Template Name: slider test
*/
get_header(); ?>
<div class="mw_home_tag">
<div class="inner_wraper">
<?php /* ?><?php
			query_posts(array(
				'post_type'      => 'home-slider', // You can add a custom post type if you like
				'posts_per_page' => 1,
				'end_size'=> 1,
				'order' => 'DESC'
			));
			if ( have_posts() ) : while (have_posts()) : the_post(); 
?>			            
            	<a href="<?php the_permalink(); ?>">	
      				<h1><?php the_title(); ?></h1>
      				<span><?php echo get_post_meta($post->ID, 'tagline', true); ?></span>
				</a>  
<?php
			endwhile;
			endif;
			wp_reset_postdata();
?><?php */ ?>	  
 			<?php  if ( have_posts() ) :
			 while (have_posts()) : the_post();
			 	the_content();
			 endwhile; endif; ?>

			</div>
  		</div>

		<div id="hm_ftrwidget" class="full grey">
			<h2>Social Hub</h2>
			<div id="home-widget">
				<?php /*?><div id="hw-1">
					<div class="footer_social">
						<ul>
							<li class="li"><a href="https://www.linkedin.com/company/habib-university" title="LinkedIn" target="_blank">linked in</a></li>
							<li class="tw"><a href="https://twitter.com/habibuniversity" title="Twitter" target="_blank">Twitter</a></li>
							<li class="vm"><a href="http://vimeo.com/habibuniversity" title="Vimeo" target="_blank">viemo</a></li>
							<li class="fb"><a href="https://www.facebook.com/HabibUniversity" title="Facebook" target="_blank">facebook</a></li>
							<li class="tm"><a href="http://instagram.com/habibuniversity" title="Instagram" target="_blank">tumblr</a></li>
						</ul>
						<span class="red">SOCIAL MEDIA HUB</span>
					 </div>
				<?php // if ( ! dynamic_sidebar( 'Fourth Home Widget Area' ) ) : ?>
					<?php // endif; ?>
					<!--<span class="events_more"> <a href="/events"> View All</a> </span>-->
				</div><?php */?>
				<?php /*?>
				<div id="hw-2">
					<?php if ( ! dynamic_sidebar( 'Second Home Widget Area' ) ) : ?>
					<?php endif; ?>
				</div>
				<div id="hw-3">
					<?php if ( ! dynamic_sidebar( 'Third Home Widget Area' ) ) : ?>
					<?php endif; ?>
				</div>
				<?php */?>
			</div>
		</div>
<?php get_footer(); ?>
