<?php
/**
 * Template for displaying Working Paper Series Texonomy
 *
 */

get_header('ycsd'); ?>

		<div id="container" class="huSpeakers">
        	
			<div id="content" role="main">
			
			<?php
                if ( have_posts() )
                    the_post();
            ?>

			<h1 class="page-title">
  				<?php single_cat_title(); ?>
			</h1>
            <!-- <?php #var_dump(get_queried_object()); ?> -->
			<div class="mwSpeakerDesc">
            	<img src="<?php the_field('thumbnail', get_queried_object()); ?>" style="float:left; margin:0 20px 20px 0;" />
				<?php echo term_description( $term_id, $taxonomy ) ?>
                <div style="width: 170px; float: right;">
                    <div class="fb-like" data-href="https://habib.edu.pk/speaker/<?php echo get_queried_object()->slug; ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
                    <div style="padding-top:4px; float:right;">
                    <a href="http://twitter.com/share" class="twitter-share-button" data-url="https://habib.edu.pk/speaker/<?php echo get_queried_object()->slug; ?>" data-text="" data-count="none" data-via="HabibUniversity" >Tweet</a>
                    
                    </div>
                </div>
            </div>
            
            <div id="mwSpeakerEvent">
            	<h2>Sessions/Lectures at Habib University</h2>
<?php
	/*
	 * Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/*
	 * Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archive.php and that will be used instead.
	 */
	// get_template_part( 'loop', 'archive' );
	
	while ( have_posts() ) : the_post();
?>
	
                <article class="ycsdBox">
                	<?php if( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )) { ?>
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"  width="230" height="97" class="top" width="230" height="97" /></a>
                    <?php } ?>    
                        <div class="ycsdInner">
                            <h4 class="ycsdTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <p class="ycsdText"><?php echo string_limit_words(get_the_excerpt(), 15).'...'; ?></p>
                            <a class="ycsdReadMore" href="<?php the_permalink(); ?>">Read More...</a>
                        </div>
                </article>    
<?php
	endwhile;
?>
				</div><!-- mwSpeakerEvent -->
			</div><!-- #content -->
 	        <div id="primary" class="widget-area">
            	
				<?php
					$taxonomy = 'speakers';
					$args = array(
								'exclude'	=> array(get_queried_object()->term_id),
							);
					$tax_terms = get_terms($taxonomy, $args);
				if(tax_terms) {
				?>	
                <h3 class="widget-title side"><a href="/speakers">Speakers</a></h3>
                <ul class="speakerList tb_side">
                <?php 
					foreach ($tax_terms as $tax_term) { ?>
                	<li><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></li>
               	<?php  	
					}
					wp_reset_query();
					
				}
				?>     
                </ul>
            </div><!-- #primary .widget-area -->          
		</div><!-- #container -->

<?php get_footer(); ?>