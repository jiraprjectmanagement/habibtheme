<?php
/*
 Template Name: CPE Inner Page
*/
get_header('cpe'); ?>
     

<div class="container">
    <div class="inner-hero__banner__container">
        <img src="/wp-content/themes/habib/css/images/Group-5-inner-cpe.png"/>
        <div class="banner_inner_main">
            <div class="banner1inner"><p>Center for Pedagogical Excellence</p></div>
            <div class="banner2inner">Covid-19 Response</div>
        </div>
    </div>


   <div id="container_missions" >
			<div id="content" role="main" class="adm_inner"  style="background-color: #fff;">
                    <!--<h1 class="entry-title"><?php echo get_the_title();?></h1>-->
                    <?php
        			if ( have_posts() ) :
        				while (have_posts()) : the_post();
                        	the_content();
                   		endwhile;
        			endif; ?>
			       
            </div>
           
         <div id="primary" class="widget-area cpesidebar" role="complementary">
            <div class="cpe-prv"><a href=""><span>
                <script>
                      //document.write('<a class="prv-btn" href="' + document.referrer + '"><i class="fa fa-arrow-left"></i> Back</a>');
                      document.write('<a class="prv-btn" href="/covid19/cpe-response/"><i class="fa fa-arrow-left"></i> Back</a>');
                </script></span></a>
            </div>
                    
            <?php  //dynamic_sidebar( 'cpe-sidebar' ); ?>
            <?php get_sidebar(); ?>
            
         </div> 
            
            
    </div>
</div>


<?php get_footer('cpe'); ?>
