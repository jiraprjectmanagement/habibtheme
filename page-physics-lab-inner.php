<?php
/**
 * Template Name: Physics Lab Inner
 *
 */

//get_header('small'); ?>
<?php get_header('with-megamenu-live'); ?>

		<div id="container" class="phyLab">
			<div id="content" class="innerLayout">
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<div class="topImg parallax-window" data-parallax="scroll" data-image-src="<?php echo $url; ?>">
					
					<img src="<?php echo $url; ?>" />
					<div class="mbox">
						<h1><?php the_title(); ?></h1>
						<div class="subTitle"><?php the_field('sub_title'); ?></div>
					</div>
				</div>
			<?php	if ( have_posts() ) :
						while (have_posts()) : the_post();
			?>
            				<!--<h1 class="title"><?php the_title(); ?></h1>-->
            				<div class="content">
								<?php the_content(); ?>
            				</div>
            <?php				
						endwhile;
					endif; 
			?>
				<div class="phySidebar">
					<?php dynamic_sidebar( 'physics_lab' ); ?>
				</div>

			</div><!-- #content.fullwidth -->
		</div><!-- #container -->

<?php get_footer('footer-live'); ?>

<?php //get_footer(); ?>
