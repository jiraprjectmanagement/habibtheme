<?php
/*
 Template Name: Home page 
*/
get_header(); ?>

		<div class="mw_home_tag">
    		<div class="inner_wraper">
<?php /* ?><?php
			query_posts(array(
				'post_type'      => 'home-slider', // You can add a custom post type if you like
				'posts_per_page' => 1,
				'end_size'=> 1,
				'order' => 'DESC'
			));
			if ( have_posts() ) : while (have_posts()) : the_post(); 
?>			            
            	<a href="<?php the_permalink(); ?>">	
      				<h1><?php the_title(); ?></h1>
      				<span><?php echo get_post_meta($post->ID, 'tagline', true); ?></span>
				</a>  
<?php
			endwhile;
			endif;
			wp_reset_postdata();
?><?php */ ?>	  
 			<?php  if ( have_posts() ) :
			 while (have_posts()) : the_post();
			 	the_content();
			 endwhile; endif; ?>

			</div>
  		</div>

<?php get_footer(); ?>
