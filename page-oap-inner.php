<?php
/*
 Template Name: OAP Inner 
*/
 // get_header('oge'); ?>
<?php get_header('with-megamenu-live'); ?>

<!-- <div id="header"></div> -->
   <div id="container_missions" > 
   
   	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu mw_oap_menu">
                <?php echo "<h1 class='oap-h1'>".get_the_title()."</h1>"; ?>
                 <div class="breadcrumbs">
                             <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
                   </div>
            
			</div>
		</div>
        
	</div>
    
	<div id="content_stdaffairs" role="main" class="adm_inner" >
  <?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
	       
    </div> 
           
         <div id="primary_stdaffairs" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php  dynamic_sidebar( 'std_affairs_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs1" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_1_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs2" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_2_sidebar' ); ?>
         </div> 
            
            
    </div>

 

<?php //get_footer(); ?>
<?php get_footer('footer-live'); ?>