<?php
/*
 Template Name: Admissions sep 21
*/

// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<div id="container_missions">
   <div id="content" role="main" class="adm_inner">
         <?php
            if ( have_posts() ) :
               while (have_posts()) : the_post();
                  the_content();
                     endwhile;
               endif; 
         ?>
                     <!-- <h1 class="entry-title"><?php echo get_the_title();?></h1> --->
         <div id="primary" class="widget-area" role="complementary">
         <!-- <?php dynamic_sidebar( 'admission_sidebar' ); ?> -->
         </div>
   </div>
</div>

   <script>$("#accordion").accordion({ collapsible: true, active: false });</script>
   <script>
            $('.counter-count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 2000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
         $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
</script>
<script language="javascript">
$(document).ready(function() {
    $("p.tap-steps-down").click(function(){
    $("table.process-step-table").toggle();
    $("p.tap-steps-down i.fa.fa-angle-down").toggle();
    $("p.tap-steps-down i.fa.fa-angle-up").toggle();
  });
  
	if(window.innerWidth <= 991){
	$(".comment").shorten();
	
	$(".comment-small").shorten({showChars: 10});
    }
 });
 </script>
<!-- <script language="javascript">
    $(document).ready(function(){
  $("p.tap-steps-down").click(function(){
    $("table.process-step-table").toggle();
  });
});
</script> -->
<?php get_footer('footer-live'); ?>
