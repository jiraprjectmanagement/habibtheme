<?php
/*
 Template Name: OGE Inner 
*/

// get_header('oge'); ?>

<?php get_header('with-megamenu-live'); ?>


	<?php /*?><?php if(get_field('logo')) { ?>
    <div class="mw_center_logo">
        <a href="<?php the_field( 'logo_link' ); ?>"><img src="<?php the_field( 'logo' ); ?>" /></a>
    </div>
    <?php } ?><?php */?>
	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu">
				<?php wp_nav_menu( array('container_class' => 'ogeMenu', 'theme_location' => 'oge_nav' ) ); ?>
			</div>
		</div>
	</div>
   	<div class="mw_oge_mid innerpage">
<!--
   		<div class="mw_news_events_visit">
   			
		</div>
-->
    	<?php
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		if($url) {
		?>
     	<div class="inner_wraper oge_banner">
     		<img src="<?php echo $url; ?>" />
     		<?php echo "<h1>".get_the_title()."</h1>"; ?>
		</div>
       <?php } ?>
        <div id="contentPart" class="inner_wraper">
        	<div class="content">
        	<?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
			</div>
       		<?php get_sidebar('oge') ?>
        </div>
        
        
    </div>

	

<?php // get_footer('oge'); ?>

<?php get_footer('footer-live'); ?>