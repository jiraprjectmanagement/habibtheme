<?php 
// custom admin panel 

if (get_option('tutorial_theme_options')) {
	$theme_options = get_option('tutorial_theme_options');
} else {
	add_option('tutorial_theme_options', array (
//		'footer_text' => 'all Copywrites are Reserved by multiwebzone.com',
//		'header_text' => 'MULTIWEBZONE'
	));
	$theme_options = get_option('tutorial_theme_options');
}


add_action('admin_menu', 'themeoptions_admin_menu');

function themeoptions_admin_menu() 
{
	// here's where we add our theme options page link to the dashboard sidebar
	add_theme_page("Theme Options", "Theme Options", 'edit_themes', basename(__FILE__), 'themeoptions');
}

 
function themeoptions() { 
 
	global $theme_options;
	$new_values = array (
		'fb_url' => $_POST['fb_url'],
		'tw_url' => $_POST['tw_url'],
		'linkedin_url' => $_POST['linkedin_url'],
		'vimeo_url' => $_POST['vimeo_url'],
		'instagram_url' => $_POST['instagram_url'],
		'google_analytics' => $_POST['google_analytics'],
		'mw_copyright_text' => $_POST['mw_copyright_text']
	);
if ($_POST['Submit']) {
	update_option('tutorial_theme_options', $new_values);
	$theme_options = $new_values;

?>
<div class="updated">
	<p><strong><?php _e('settings saved.', 'menu-test' ); ?></strong></p>
</div>
<?php	
}
?>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/admin-style.css" rel="stylesheet" media="all" />

<div class="wrap"> <a href="http://www.techibits.com" target="_blank" title="Techibits"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/tb-logo.png" class="tb" /></a>
  <h2>Theme Options For
    <h1 class="title"><?php echo bloginfo(); ?></h1>
  </h2>
  <div  class="theme_main">
    <form method="post" action="themes.php?page=theme-options.php">
      <?php // wp_nonce_field('update-options') ?>
      <div class="option_header"> </div>
      <div class="option_menu">
        <div id='tab1'>
          <h3>General Settings</h3>
      	<table>
            
            
            <tr>
              <td><strong>Social links</strong></td>
              <td></td>
            </tr>
            <tr>
              <td>Facebook Link URL</td>
              <td><input type="text" name="fb_url" size="45" value="<?php echo $theme_options['fb_url']; ?>" /></td>
            </tr>
            <tr>
              <td>Twitter Link URL</td>
              <td><input type="text" name="tw_url" size="45" value="<?php echo $theme_options['tw_url']; ?>" /></td>
            </tr>
            <tr>
              <td>Linkedin Link URL</td>
              <td><input  type="text" name="linkedin_url" value="<?php echo $theme_options['linkedin_url']; ?>"></td>
            </tr>
            <tr>
              <td>Vimeo Link URL</td>
              <td><input type="text" name="vimeo_url" size="45" value="<?php echo $theme_options['vimeo_url']; ?>" /></td>
            </tr>
            <tr>
              <td>Instagram Link URL</td>
              <td><input type="text" name="instagram_url" size="45" value="<?php echo $theme_options['instagram_url']; ?>" /></td>
            </tr>
          </table>
        </div>
       
        <div id='tab3'>
          <h3>Footer Settings</h3>
          <table>
            <tr>
              <td>Google Analytics :</td>
              <td><textarea name="google_analytics" cols="40"><?php echo $theme_options['google_analytics']; ?></textarea></td>
            </tr>
            <tr>
              <td>Copyright Text</td>
              <td><textarea name="mw_copyright_text" cols="40"><?php echo $theme_options['mw_copyright_text']; ?></textarea></td>
            </tr>
          </table>
        </div>
        
        <?php settings_fields('ud_options'); ?>

    <?php do_settings_sections('ud'); ?>

        <p>
          <input type="submit" name="Submit" value="<?php esc_attr_e('Save Changes') ?>" class="save-button" />
        </p>
        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="page_options" 
                            value="logo_url,favicon_url,feature_box1,feature_box1_url,feature_box2,top_header,feature_box2_url,feature_box3,feature_box3_url,fb_url,tw_url,link_url,rss_url,speakers_pop,google_analytics,copyright_text" />
        <div class="tb_footer"> <a href="http://www.techibits.com" target="_blank" title="Techibits"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/tb-logo.png" class="tb_footer" /></a></div>
      </div>
      
      
    </form>
  </div>
</div>
<?php  
    }  
    ?>
