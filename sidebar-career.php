<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php
global $post;

if (is_page( 'faculty-careers' ) || '265889' == $post->post_parent){
	?>

<?php if ( is_active_sidebar( 'open-faculty-positions_sidebar' ) ) : ?> 
<div id="primary" class="widget-area ofp" role="complementary">
	 <ul class="tb_side huit_wid career_wid"> 
		 <?php dynamic_sidebar( 'open-faculty-positions_sidebar' ); ?> 
		</ul> 
	</div> 
	<?php endif; ?>

	<?php }else{  ?>

<?php if ( is_active_sidebar( 'career_sidebar' ) ) : ?>
	<div id="primary" class="widget-area" role="complementary">
          <ul class="tb_side huit_wid career_wid">
            <?php dynamic_sidebar( 'career_sidebar' ); ?>
          </ul>
    </div>
<?php else: ?>
            
		<div id="primary" class="widget-area" role="complementary">
<?php
	/*
	 * When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	
	?>
<?php
  if($post->post_parent) {
  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
  $titlenamer = get_the_title($post->post_parent);
  $titlelink = get_the_permalink($post->post_parent);
  }

  else {
  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
  $titlenamer = get_the_title($post->ID);
  $titlelink = get_the_permalink($post->ID);
  }
  if ($children) { ?>

  <h3 class="widget-title side"> <a href="<?php echo $titlelink; ?>"><?php echo $titlenamer; ?></a> </h3>
  <ul class="tb_side">
  	<?php echo $children; ?>
  </ul>

<?php } else { ?>
<?php if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

			<li id="search" class="widget-container widget_search">
				<?php get_search_form(); ?>
			</li>
		<?php endif; // end primary widget area ?>
<?php  } ?>
    
	
			
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'newsibar' ) ) : ?>
		<div id="primary2" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'newsibar' ); ?>
		</div>
<?php endif; 

 endif; 
 
}
 ?>
