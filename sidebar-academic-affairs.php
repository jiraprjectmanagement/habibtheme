<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 */
?>

<div id="primary" class="widget-area wid-poffice" role="complementary">

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'wd-president-office' ) ) : ?>
		<?php dynamic_sidebar( 'wd-president-office' ); ?>
<?php
	endif; 
?>
</div>
