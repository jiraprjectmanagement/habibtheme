<?php
/**
 * Template Name: Admissions Campaign Main
 *
 */

get_header('small'); ?>

<style type="text/css">
.overlay {
    display: none;
  width: 100%;
  background: rgba(0,0,0,.75);
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
}
.head_fall2020{
    text-align: center;
    font-size: 31px;
    color: #5c2568;
    text-shadow: 2px 4px 5px #ccc;

}
.videoBox {
      position: fixed;
    width: 90%;
    left: 50%;
    top: 50%;
    transform: translateY(-50%) translateX(-50%);
}
.para_fall2020{
    text-align: center;
    color: #000000;
    font-size: 20px!important;
    line-height: 25px;
    padding: 19px 0px;

}
.videoBox video {
  width: 100%;
}
.close {
    width: auto;    cursor: pointer;
    height: 36px;    font-size: 16px;
    position: absolute;
    top: 25%;
    right: 2px;
    display: block;
    opacity: .8;
    color: white;
}
.resp-container {
    position: relative;
    overflow: hidden;
    padding-top: 56.25%;
}
.resp-iframe {
        position: absolute;
    top: 30%;
    left: 0;
    width: 100%;
    height: 50%;
    border: 0;
}
.close:hover {
  opacity: 1;
}
@media (min-width: 767px) {
  .videoBox {
    width: 50%;
  }
}
</style>
<div id="container" class="phyLab">
<div class="overlay">
 <!-- <div class="videoBox resp-container id="videobox">
    <a class="close"><i class="fa fa-window-close" aria-hidden="true"></i>CLOSE WINDOW</a> 
<iframe id="vimeo_player" class="resp-iframe" src="https://player.vimeo.com/video/242161685?autoplay=1&muted=1&loop=1&autopause=0&controls=1" width="640" height="360" frameborder=�0� allowfullscreen allow=autoplay></iframe>
</div>-->
</div>
<script src="http://code.jquery.com/jquery.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script>
$(function() {
  // CLOSE AND REMOVE ON ESC
  $(document).on('keyup',function(e) {
    if (e.keyCode == 27) {
      $('.overlay').remove();
    }
  });
  
  // CLOSE AND REMOVE ON CLICK
  $('body').on('click','.overlay, .close', function() {
    $('.overlay').remove();
  });
  
  // SO PLAYING WITH THE VIDEO CONTROLS DOES NOT
  // CLOSE THE POPUP
  $('body').on('click','.videoBox', function(e) {
    e.stopPropagation();
  });
});
</script>
		<div id="stlife_content" class="innerLayout">
				
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					if(!empty($url)) {
						$myurl = 'data-parallax="scroll" data-image-src="'.$url.'"';
					} else {
						$myurl = '';
					}
				?>
			  
				<?php if( have_rows('physics_slider') ): ?>
				
           <div class="vav_wrap01"> 
               <div  class="menu-toggle-button" data-menu-id="demo-menu1">Browse Student Life <i style="visibility: hidden;">-</i><i class="fas fa-chevron-down"></i></div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'stdlifemenu','items_wrap' => '<ul class="menu-list" id="demo-menu1"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'stdlifemenu' ) ); ?>
                    </div><!-- #access -->      
                </div> 
            </div>
            
				<?php else: ?>
				
			
				<?php endif; ?>
			<?php	if ( have_posts() ) :
						while (have_posts()) : the_post();
			?>
            				<!--<h1 class="title"><?php the_title(); ?></h1>-->
            				<div class="content">
								<?php the_content(); ?>
                                
            				</div>
            <?php				
						endwhile;
					endif; 
			?>
			
			</div><!-- #content.fullwidth -->
		</div><!-- #container -->

<?php get_footer(); ?>
