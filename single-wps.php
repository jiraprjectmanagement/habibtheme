<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php
	$cat_term = get_the_terms( $post->ID, 'wps_categories');
	
	$cat_term_tax = $cat_term[0]->taxonomy;
	$cat_term_val = $cat_term[0]->slug;
?>
		<div id="container" class="workingPaperSeries">
        	<div id="wpsSingleBanner">
            	<?php if(get_field('banner')){ ?>
                	<img src="<?php echo the_field('banner'); ?>" />
                <?php } ?>
            </div>
			<div id="content" role="main">
			<div class="huBreadcrumb">
                <a href="/wps"><?php _e('Working Paper Series'); ?></a> > <a href="<?php echo esc_attr(get_term_link($cat_term_val, $cat_term_tax)); ?>"><?php echo $cat_term[0]->name ?></a>
            </div>
			<?php
			/*
			 * Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			// get_template_part( 'loop', 'single' );
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>
                    <div class="entry-meta">
						 <span class="icn_cal"></span><span class="date"><?php echo get_the_date('d/m/Y'); ?></span>
					</div><!-- .entry-meta -->
					<div class="entry-content">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				</div>   
			
			<?php			
			endwhile; 
            endif;
			?>

			</div><!-- #content -->
			<div id="primary" class="widget-area">
            	<div class="wpsUlCat">
<?php
					
					
					$custom_terms = get_terms('wps_categories');
					$args = array(
						'post_type' => 'wps',
						'tax_query' => array(
						
							array(
								'taxonomy' => 'wps_categories',
								'field' => 'slug',
								'terms' => $cat_term_val,
							),
						),
//							   'post__not_in' => $post->ID,
					 );
					 $loop = new WP_Query($args);
					  if($loop->have_posts()) {
?>							                 
                    <h3>Related</h3>
                    <ul>
                    	<?php
							while($loop->have_posts()) : $loop->the_post();
								echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
							endwhile;
						?>
                    </ul>
                    <?php } ?>
                </div>
                
            </div><!-- #primary .widget-area -->
            
		</div><!-- #container -->


<?php get_footer(); ?>
