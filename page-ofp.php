<?php
/*
Template Name: Ofp
 */

get_header(); ?>

        <!-- Owl Stylesheets  -->
        <link rel="stylesheet" href="https://habib.edu.pk/playground/wp-content/themes/playground/css/owl.carousel.min.css?ver=4.8.10">
		<link rel="stylesheet" href="https://habib.edu.pk/playground/wp-content/themes/playground/css/owl.theme.default.min.css?ver=4.8.10">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

         <!-- javascript -->
		<script src="https://hufus.org/owl/assets/vendors/jquery.min.js"></script>
		<script src="https://habib.edu.pk/playground/wp-content/themes/playground/js/owl.carousel.min.js"></script>

		<div class="ofp-container">
			<div class="ofp-container">

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
           
		</div><!-- #container -->

        <script>

jQuery(document).ready(function($) {
    var prev =  $( ".owl-prev").text;
    console.log(prev+" == ");

    $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');		
    var owl = $('.owl-carousel');
    owl.on('initialize.owl.carousel initialized.owl.carousel ' +
        'initialize.owl.carousel initialize.owl.carousel ' +
        'resize.owl.carousel resized.owl.carousel ' +
        'refresh.owl.carousel refreshed.owl.carousel ' +
        'update.owl.carousel updated.owl.carousel ' +
        'drag.owl.carousel dragged.owl.carousel ' +
        'translate.owl.carousel translated.owl.carousel ' +
        'to.owl.carousel changed.owl.carousel',
        function(e) {
        $('.' + e.type)
            .removeClass('secondary')
            .addClass('success');
        window.setTimeout(function() {
            $('.' + e.type)
            .removeClass('success')
            .addClass('secondary');
        }, 500);
        });
    owl.owlCarousel({
        loop: true,
        nav: true,
        // navText: ["<div class='nav-button owl-prev'> </div>", "<div class='nav-button owl-next'> </div>"],
        lazyLoad: false,
        margin: 10,
        autoplay:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        960: {
            items: 3
        },
        1200: {
            items: 3
        }
        }
    });
    });
</script>
<?php get_footer(); ?>
