<?php
/*
 Template Name: academics central
*/

// get_header('hpv1'); ?> 
<?php get_header('with-megamenu-live'); ?>




  
<style>

.cst-p-ul{
 
    list-style-type: none;
    text-align: left;
    margin: 0px;
    padding: 0px;
}

.cst-p-ul-li{

    display: inline-block;
    color: #5c2568;
    padding: 2px;
    background-color: #e9daed;
    margin: 3px;
    text-align: left;
    margin-left: 0px;

}

p ul li{
    color: #5c2568;
    background-color: #e9daed;
    padding: 2px 10px;
    margin: 5px;
    font-weight: 500;
}

.view-all-class{

    text-align: center; 
    font-size: 14pt;
    border-radius: 7px;
    background-color: #5c2568;
    padding: 4px 15px;
    width: 20%;
    
}

</style>
<div class="container space-set">
<div class="container-fluid no-padding">
  <div class="row">
    <div class="col-md-12">
      <img  style="width: 100%;" class="img-responsive" src="https://habib.edu.pk/docs/app-images/Academics-banner-02.jpg" alt="placeholder 960" />
    </div>
  </div>
</div>

<div class="wc_content_lc"> 
    
    <div class="row mb-3" style="color: #fff;">
         .
        </div>

    <div class="row text-center mt-1 mb-2">
        <h1 style="text-transform: uppercase;  font-size: 2rem;  font-weight: bold; color: #5c2568;">ACADEMICS</h1>
    </div>
    
 
    <div class="row"> 

 
<!--     <div id="primary_lc_sidebar" class="widget-area desktoplc" role="complementary">
        <?php  dynamic_sidebar( 'lc_sidebar' ); ?>
     </div>  -->
         
    <div class="col-sm-12">
        <p style="color: #5c2568; text-align:justify;">The strength of Liberal Arts model of education lies in its capacity to produce graduates who possess sharp critical minds, a penchant for creative and unconventional thinking, and essential leadership skills. Developing the full capacities of a truly well-educated person relies both on a depth of study in the students chosen major combined with a real breadth of knowledge gained through a meaningful and sufficiently broad engagement with a range of existing forms of knowledge. An engineer who also knows literature is a better engineer, and a poet who also knows physics is inherently a better poet. Habib University is committed to the Liberal Arts and Sciences model of education because of its demonstrated success in producing graduates who have both the adaptability and confidence to succeed in a world of increasingly rapid change and volatility. Liberal Arts and Sciences graduates arent just surviving in the complex world of the twenty-first century, theyre seizing the initiative to actively shape it! At the heart of a Habib education lies our distinctive Liberal Core curriculum, which embodies our ideal of Yohsin: "The worth of each person lies in their own ful self-cultivation." As our students mature, their experience in the Habib Liberal Core empowers them to both reflect on and change the world they inherit.</p>
        <p style="color: #5c2568; text-align:justify;"> The strength of Habibs Liberal Core is built on its seven Forms of Thought and Action. These seven Forms of Thought, which were inspired by Stanford Universitys Breadth Governance model, but adapted to our own unique regional context, include: historical and social thought, philosophical thought, rhetoric and expression, formal reasoning, quantitative reasoning, scientific method and analysis and creative practice. All students are required to take a determined minimum number of courses under each form of thought and action. </p>
    </div>
   

    </div>

    <h5 style="color: #5c2568; text-align:center;"> To find out more, please click on the circles </h5>

    <!--Circle Start for mobile -->

    <div class="container mblview">  
            <hr style="    border-top: 1px solid rgb(255 255 255);"/>
                <h2 style="text-align: center; text-transform: uppercase;	font-weight: bold;	color: #5c2568;	margin-bottom: 20px;">Transformative  <br />  Intellectual  <br />  Experience </h2>         
                <div class="selector_mbl_lc">
                    <div class="row">
                        <div class="col-sm-6 lft-circle academics-mblview-circle">
                            <a href="#Habib-Libral-Core" data-lity>
                            <div class="selector_circle">
                                <span>
                                    <img src="/docs/app-images/icons/Habib-Libral-Core.png"/>
                                </span>
                                <p>
                                    Habib Liberal <br />Core 
                                </p>
                            </div>
                            </a>
                        </div>
                        <div class="col-sm-6 rgt-circle academics-mblview-circle">
                            <a href="#student-centric" data-lity>
                            <div class="selector_circle">
                                <span>
                                    <img src="/docs/app-images/icons/Student-centricity.png"/>
                                </span>
                                <p>
                                    Student <br /> Centric
                                </p>
                            </div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6 lft-circle academics-mblview-circle">
                        <a href="#faculty" data-lity>
                            <div class="selector_circle">
                                <br> 
                                <span>
                                    <img src="/docs/app-images/icons/World-Class-Faculty.png"/>
                                </span>
                                <p>
                                    Faculty
                                </p>
                            </div>
                            </a>
                        </div>
                        <div class="col-sm-6 rgt-circle academics-mblview-circle">
                            <a href="#contextualized-learning" data-lity>                               
                            <div class="selector_circle">
                                <span>
                                    <img src="/docs/app-images/icons/Contextualized-learning.png"/>
                                </span>
                                <br>
                                <p>
                                    Contextualized <br />Learning
                                </p>
                            </div>
                            </a>
                        </div>
                    </div>
                  
                    
                    <div class="container" style="    margin: 0 auto;    width: 50%;">
                        <div class="row academics-mblview-circle">
                            <a href="#design-thinking" data-lity>
                                <div class="col-sm-12 rgt-circle">
                                <div class="selector_circle">
                                    <br>
                                    <span>
                                        <img src="/docs/app-images/icons/Human centered design-005.png"/>
                                    </span>
                                    <p>
                                        Design<br /> Thinking
                                    </p>
                                </div>
                                </div>
                            </a>
                        </div>
                    
                    </div>
                    
                   <!-- <div class="jumbotron-lc">
                        <a href="/academics/habib-core/course-descriptions/" target="_self"  class="btn_liberalcore">View Course Descriptions</a>
                    </div> -->
                </div>
        </div>


    <!--Circle end for mobile -->
        
    
    <!--Circle Start -->
        <div class="wc_section_academics">
            <div class='selectorac'>
                <ul>
                    <li>
                    <input id='c1' type='checkbox'>
                    <label for='c1'><a href="#Habib-Libral-Core" data-lity><span><img src="/docs/app-images/icons/Habib-Libral-Core.png"/></span><br />Habib Liberal <br />Core </label></a>
                    </li>
                    <li>
                    <input id='c2' type='checkbox'>
                    <label for='c2'><a href="#student-centric" data-lity><span><span><img src="/docs/app-images/icons/Student-centricity.png"/></span><br />Student <br /> Centric </label></a>
                    </li>
                    <li>
                    <input id='c3' type='checkbox'>
                    <label for='c3'><a href="#faculty" data-lity><span><img src="/docs/app-images/icons/World-Class-Faculty.png"/></span><br />Faculty<br /> </label></a>
                    </li>
                    <li>
                    <input id='c4' type='checkbox'>
                    <label for='c4'><a href="#contextualized-learning" data-lity><span><span><img src="/docs/app-images/icons/Contextualized-learning.png"/></span><br />Contextualized <br />Learning</label></a>
                    </li>
                    <li>
                    <input id='c5' type='checkbox'>
                    <label for='c5'><a href="#design-thinking" data-lity><span><span><img src="/docs/app-images/icons/Human centered design-005.png"/></span><br />Design<br /> Thinking</label></a>
                    </li>
                </ul>
                <button disabled="disabled">Transformative <br /> Intellectual <br /> Experience</button>
            </div>
        </div>
    <!-- Circle End-->
    <div class="row text-center mt-1 mb-2">
        <!-- <h1 style="text-transform: uppercase;  font-size: 28px; text-transform: uppercase;  uppercase; margin-top:20px;  margin-bottom:30px;  color: #000;"><strong>ACADEMIC PROGRAMS</strong></h1> -->
        <h2 style="text-align: center; text-transform: uppercase;	font-weight: bold;	color: #5c2568; margin-top:30px;	margin-bottom: 20px;"> ACADEMIC PROGRAMS </h2>
    </div>
    
    <div class="row">
        <div class="col-sm-6" style="padding-right: 35px;"> 
            <h2 class="my-class">Majors</h2>
                <div class="row majors">
                    <li> <a href="https://habib.edu.pk/academics/sse/computer-science/"> <span><img src="https://habib.edu.pk/docs/app-images/uni-logos/CS-icn.jpg"/></span>BS Computer Science  </a> </li>
                    <li> <a href="https://habib.edu.pk/academics/sse/electrical-engineering/"> <span><img src="https://habib.edu.pk/docs/app-images/uni-logos/EE-icn.jpg"/></span>BS Electrical Engineering </a> </li>
                    <li> <a href="https://habib.edu.pk/academics/sse/computer-engineering/"> <span><img src="https://habib.edu.pk/docs/app-images/uni-logos/CE-icn.jpg"/></span>BS Computer Engineering </a> </li>
                    <li> <a href="https://habib.edu.pk/academics/ahss/comm-design/"> <span><img src="https://habib.edu.pk/docs/app-images/uni-logos/CND-icn.jpg"/></span>BA (Honors) Communication & Design </a> </li> 
                    <li> <a href="https://habib.edu.pk/academics/ahss/comparative-humanities/"><span><img src="https://habib.edu.pk/docs/app-images/uni-logos/CLS-icn.jpg"/></span>BA (Honors) Comparative Humanities </a> </li>
                    <li> <a href="https://habib.edu.pk/academics/ahss/social-development/"> <span><img src="https://habib.edu.pk/docs/app-images/uni-logos/SDP-icn.jpg"/></span>BSc (Honors) Social Development & Policy </a> </li>
                </div>
        </div>
        <div class="col-sm-6" style="padding-right: 35px;">
            <h2 class="my-class">Minors</h2>
            <div class="row minors">
                <li>Comparative Literature</li>
                <li>Computer Science</li>
                <li>History</li>
                <li>Mathematics</li>
                <li>Philosophy</li>
                <li>Physics</li>
                <li>Religious Studies</li>
                <li>Social Development and Policy</li>
                <li>South Asian Music</li>
                <li><a href="https://habib.edu.pk/academics/minors/" class="view-all-class">  View all   </a></li>
                   
            </div>
        </div>
    </div>
    
    <div class="row mb-3" style="color: #fff;">
     .
    </div>
    
    <div class="row text-center mt-1 mb-2">
        <h1 style="text-transform: uppercase;  font-size: 2rem;  font-weight: bold; color: #5c2568;">Global Connections</h1>
    </div>
    
    <div class="row">
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center> <img src="https://habib.edu.pk/docs/app-images/uni-logos/berkley.jpg" class="uni-logo img-responsive"/> </center>
        </div>
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center> <img src="https://habib.edu.pk/docs/app-images/uni-logos/harvey-mudd.jpg" class="uni-logo img-responsive"/> </center>
        </div>
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center><img src="https://habib.edu.pk/docs/app-images/uni-logos/michigen.jpg" class="uni-logo img-responsive"/> </center>
        </div>
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center> <img src="https://habib.edu.pk/docs/app-images/uni-logos/pitzer.jpg" class="uni-logo"/> </center>
        </div>
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center><img src="https://habib.edu.pk/docs/app-images/uni-logos/stanford.jpg" class="uni-logo"/> </center>
        </div>
        <div class="col-sm-2 col-6 adjust-uni-logo">
            <center><img src="https://habib.edu.pk/docs/app-images/uni-logos/texas-am.jpg" class="uni-logo"/> </center>
        </div>
    </div>
       
    <div class="row mb-3" style="color: #fff;">
     .
    </div>
</div>
</div>




<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script>var nbOptions = 6;
var angleStart = -360;

// jquery rotate animation
function rotate(li,d) {
    $({d:angleStart}).animate({d:d}, {
        step: function(now) {
            $(li)
               .css({ transform: 'rotate('+now+'deg)' })
               .find('label')
                  .css({ transform: 'rotate('+(-now)+'deg)' });
        }, duration: 0
    });
}

// show / hide the options
function toggleOptions(s) {
    $(s).toggleClass('open');
    var li = $(s).find('li');
    var deg = $(s).hasClass('half') ? 180/(li.length-1) : 360/li.length;
    for(var i=0; i<li.length; i++) {
        var d = $(s).hasClass('half') ? (i*deg)-90 : i*deg;
        $(s).hasClass('open') ? rotate(li[i],d) : rotate(li[i],angleStart);
    }
}

$('.selectorac button').click(function(e) {
    toggleOptions($(this).parent());
});

setTimeout(function() { toggleOptions('.selectorac'); }, 100);//@ sourceURL=pen.js
</script>

            
            <div id="Habib-Libral-Core" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h3 style="text-align: center; color: #5c2568;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Habib Liberal Core</h3>
                <hr />
                <p style="color: #5c2568;"> SEVEN FORMS OF THOUGHT AND ACTION.</p>
                <p> 
                    <ul class="cst-p-ul">
                        <li class="cst-p-ul-li">HISTORICAL AND SOCIAL THOUGHT</li>
                        <li class="cst-p-ul-li">PHILOSOPHICAL THOUGHT</li>
                        <li class="cst-p-ul-li">FORMAL REASONING</li>
                        <li class="cst-p-ul-li">LANGUAGE AND EXPRESSION</li>
                        <li class="cst-p-ul-li">CREATIVE PRACTICE</li>
                        <li class="cst-p-ul-li">QUANTITATIVE REASONING</li>
                        <li class="cst-p-ul-li">NATURAL SCIENTIFIC METHOD AND ANALYSIS</li>
                    </ul>
                </p>
                <p> <a href="https://habib.edu.pk/academics/habib-core/"  class="view-all-class" style="color: #fff;"> Please click to see details </a></p>
            </div>

            <div id="student-centric" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h3 style="text-align: center; color: #5c2568;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Student Centric</h3>
                <hr />
                <p style="color: #5c2568;"> With complete focus on students and their experience, we maintain a 12:1 student-faculty ratio for greater student-faculty interaction; we also have a comprehensive support system for our students.</p>
                <p> 
                    <ul class="cst-p-ul">
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/oap/" style="color:#5c2568;">Office of Academic Performance </a> </li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/oap/ehsas-center/" style="color:#5c2568;">EHSAS Centre </a></li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/oap/writing-center/" style="color:#5c2568;">Writing Centre </a></li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/student-life/" style="color:#5c2568;">Office of Student Life </a></li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/wellness-center/" style="color:#5c2568;">Wellness Center </a></li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/career-services/" style="color:#5c2568;">Office of Career Services </a></li>
                    </ul>
                </p>
            </div>
            <div id="faculty" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h3 style="text-align: center; color: #5c2568;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Faculty</h3>
                <hr />
                <p style="color: #5c2568;">Our world-class faculty comes from some of the institutions from across the globe – University of Pennsylvania, University of Columbia, York University; Georgia Institute of Technology, University of  Cambridge, McGill University, Syracuse University.</p>
                <p> 
                    <ul class="cst-p-ul">
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/academics/sse/faculty/" style="color:#5c2568;">DSSE Faculty </a> </li>
                        <li class="cst-p-ul-li"><a href="https://habib.edu.pk/academics/ahss/faculty/" style="color:#5c2568;">AHSS faculty </a> </li>
                    </ul>
                </p>
                
            </div>

            <div id="design-thinking" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h3 style="text-align: center; color: #5c2568;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Design Thinking</h3>
                <hr />
                <p style="color: #5c2568;">21st century human-centered problem-solving tools.</p>
                
            </div>

            <div id="contextualized-learning" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h3 style="text-align: center; color: #5c2568;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Contextualized Learning</h3>
                <hr />
                <p style="color: #5c2568;">Through regional languages, fieldwork and projects based on addressing local issues</p>
                
            </div>
            
            
            
            <!-- <div class="lity lity-opened lity-inline" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1" aria-hidden="false">
                <div class="lity-wrap" data-lity-close="" role="document">
                    <div class="lity-container">
                        <div class="lity-content">
                            <div id="WireTransfer" class="cst-pop" style="height: 347px; text-align: center; max-height: 418px;">
                                <img src="/images/wire_transfer.png" alt="" width="75px">
                                <h4>Student Centric</h4>
                                <p>test. <br>
                                test<br>
                                test<br>
                                test<br>
                                test<br>
                                test</p>
                            
                            </div>
                        </div>
                        <button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close="">×</button>
                    </div>
                </div>
            </div> -->

            <!-- <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content">
                  
                    <div class="modal-header">
                      <h4 class="modal-title">Modal Heading</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <div class="modal-body">
                      Modal body..
                    </div>
                    
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                    
                  </div>
                </div>
              </div> -->

          

          

<?php // get_footer('academics-center'); ?>
<?php get_footer('footer-live'); ?>