<?php
/*
 Template Name: OAP Main 
*/
// get_header('oge'); ?>

<?php get_header('with-megamenu-live'); ?>

<!-- <div id="header"></div> -->
     <!--container_ad class removed due to ad keyword -->
   <div id="container_missions"> 
   
   	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu mw_oap_menu">
                <?php echo "<h1 class='oap-h1'>".get_the_title()."</h1>"; ?>
                <h3 class="oap-h3">Facilitating students to achieve academic excellence</h3>
                  
             <?php // wp_nav_menu( array('container_class' => 'ogeMenu', 'theme_location' => 'stdlifemenu' ) ); ?>
			</div>
		</div>
        
	</div>
    
	<div id="content_stdaffairs" role="main" class="adm_inner" >
    
    <div class="rev_slider_wrapper" style="width:100%">
   			<?php echo do_shortcode('[rev_slider alias="oap-slider"]'); ?>
		</div>
            
            <?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
	       
    </div> 
           
         <div id="primary_stdaffairs" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php  dynamic_sidebar( 'std_affairs_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs1" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_1_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs2" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'std_affairs_2_sidebar' ); ?>
         </div> 
            
            
    </div>

 

<?php //get_footer(); ?>
<?php get_footer('footer-live'); ?>