<?php
/*
Template Name: Board of Governors
*/

//get_header(); ?>
<?php get_header('with-megamenu-live'); ?>

<div id="container">
  <div id="content" role="main">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if ( is_front_page() ) { ?>
      <h2 class="entry-title">
        <?php the_title(); ?>
      </h2>
      <?php } else { ?>
      <h1 class="entry-title">
        <?php if (get_post_meta($post->ID, 'sub_t', true)) { echo get_post_meta($post->ID, 'sub_t', true); }else{the_title();} ?>
      </h1>
      <?php } ?>
      <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'habib' ), 'after' => '</div>' ) ); ?>
        <?php edit_post_link( __( 'Edit', 'habib' ), '<span class="edit-link">', '</span>' ); ?>
      </div>
      <!-- .entry-content --> 
    </div>
    <!-- #post-## -->
    <?php endwhile; wp_reset_query(); // end of the loop. ?>
    
    <div class="board_members">
      <?php query_posts("post_type=board-governors&posts_per_page=20&orderby=menu_order&order=ASC"); 
				   if (have_posts() ) : while (have_posts() ) : the_post(); { ?>
      <div class="member"> 
      		<a href="<?php the_permalink()?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt="<?php the_title(); ?>" /></a>
        <h3><a href="<?php the_permalink()?>"><?php echo the_title(); ?></a></h3>
        <div class="desig">
        <?php if( get_post_meta($post->ID, 'desig', true)) {echo get_post_meta($post->ID, 'desig', true); } ?>
        </div> </div>
      <?php }  endwhile; endif;  wp_reset_query();?>
    </div>
    <?php //comments_template( '', true ); ?>
  </div>
  <!-- #content -->
  
  <?php get_sidebar(); ?>
</div>
<!-- #container -->

<?php //get_footer(); ?>

<?php get_footer('footer-live'); ?>
