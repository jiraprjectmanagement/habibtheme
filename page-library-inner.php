<?php
/*
 Template Name: Library Inner 
*/

// get_header('library'); ?>

<?php get_header('with-megamenu-live'); ?>

	<?php /*?><?php if(get_field('logo')) { ?>
    <div class="mw_center_logo">
        <a href="<?php the_field( 'logo_link' ); ?>"><img src="<?php the_field( 'logo' ); ?>" /></a>
    </div>
    <?php } ?><?php */?>
	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_library_menu">
				<?php wp_nav_menu( array('container_class' => 'libraryMenu', 'theme_location' => 'library_nav' ) ); ?>
			</div>
		</div>
	</div>
	
	<div id="container" class="mw_library_inner">
		<div id="content" role="main">
		<?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
		?>
			<h1 class="page-title"><?php the_title(); ?></h1>
            <div class="content">
              	<?php 
					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					if($url) {
						echo '<img class="fullWidth" src="'.$url.'" title="'.get_the_title().'" />';
					}
				?>
               	<?php
                	the_content();
				?>
			</div>
          <?php
           		endwhile;
			endif;
		?>
		</div><!-- #content -->
		<div id="primary" class="widget-area" role="complementary">
		  <h3 class="widget-title side"> <a href="/library/">Library</a> </h3>
			<?php wp_nav_menu( array('container_class' => 'libraryInner', 'menu_class' => 'tb_side', 'theme_location' => 'library_nav_two' ) ); ?>
		</div>
		<?php // get_sidebar(); ?>
	</div><!-- #container -->

  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b18f40710b99c7b36d4b5cd/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<?php //get_footer('library'); ?>

<?php get_footer('footer-live'); ?>