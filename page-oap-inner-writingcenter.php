<?php
/*
 Template Name: OAP Inner WritingCenter
*/
get_header('oge'); ?>

   <div id="container_ad" > 
   
   	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu mw_oap_menu">
                <?php echo "<h1 class='oap-h1'>".get_the_title()."</h1>"; ?>
                 <div class="breadcrumbs">
                             <?php if(function_exists('the_breadcrumbs')) the_breadcrumbs(); ?>
                   </div>
                <?php // wp_nav_menu( array('container_class' => 'ogeMenu', 'theme_location' => 'oap_nav' ) ); ?>
			</div>
		</div>
        
	</div>
    
	<div id="content_stdaffairs" role="main" class="adm_inner" >
    <?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
	       
    </div> 
           
         <div id="primary_stdaffairs" class="widget-area" role="complementary">
            <?php  dynamic_sidebar( 'std_affairs_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs1" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'std_affairs_1_sidebar' ); ?>
         </div> 
          <div id="primary_stdaffairs2" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'std_affairs_2_sidebar' ); ?>
         </div> 
            
            
    </div>

 

<?php get_footer(); ?>