<?php
/*
 Template Name: Admissions Inner Page
*/

// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<!-- <div id="header">
     </div> -->
   <div id="container_missions" >
			<div id="content" role="main" class="adm_inner" >
                    <h1 class="entry-title"><?php echo get_the_title();?></h1>
                    <?php
        			if ( have_posts() ) :
        				while (have_posts()) : the_post();
                        	the_content();
                   		endwhile;
        			endif; ?>
			       
            </div>
           
         <div id="primary" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            <?php dynamic_sidebar( 'admission_sidebar' ); ?>
         </div> 
            
            
    </div>

 <script>$("#accordion").accordion({ collapsible: true, active: false });</script>

<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>
