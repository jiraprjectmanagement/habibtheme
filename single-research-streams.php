<?php
/**
 * Template for displaying all single posts
 *
 */

get_header('ycsd'); ?>

		<div id="container">
			<div id="content" role="main">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php if(has_post_thumbnail()){ ?>
                    <div class="featuredimg">
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <img src="<?php echo $url; ?>"  />
                    </div>
                <?php } ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'habib' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

					<div class="entry-utility">
						<?php // twentyten_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-## -->
<?php 	 
		endwhile; // end of the loop. ?>
			</div><!-- #content -->
			<?php get_sidebar('researchStreams'); ?>
		</div><!-- #container -->


<?php get_footer(); ?>
