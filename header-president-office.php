﻿<?php
/**
 * Header template for President Office
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'habib' ), max( $paged, $page ) );
	?></title>
	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/lity.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="http://bkp.habib.edu.pk/wp-content/themes/habib/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<script
  src="https://code.jquery.com/jquery-1.12.4.js"
  integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
  crossorigin="anonymous"></script>
<script>
    
	$( function() {
	//	jQuery( "#tabs" ).tabs();
		
		$('.btn.quickLinks').on('click', function(){
			if($('.btn.quickLinks').hasClass('opened'))
			{
				$(this).removeClass('opened');
				$(this).next('.quickMenu').slideUp();
			} else {
				$(this).addClass('opened');
				$(this).next('.quickMenu').slideDown();
			}
			
		});
	} );
</script>
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script type='text/javascript' src='https://habib.edu.pk/wp-includes/js/jquery/ui/menu.min.js'></script>
<script type='text/javascript' src='https://habib.edu.pk/wp-includes/js/wp-a11y.min.js'></script>
<!--
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
-->
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>

<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>

<div id="fb-root"></div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>
<?php /*?><script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "http://www.habib.edu.pk/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://www.habib.edu.pk/?s={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script><?php */?>

<?php /*?><!-- Twitter Info -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script><?php */?>
<?php /*
<script type="text/javascript">
twttr.conversion.trackPid('l6bm1', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>

<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l6bm1&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l6bm1&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" /></noscript>
<?php */ ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>
<?php if(is_front_page()) { ?>
<!-- bxSlider CSS file -->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<?php } ?>

<?php if(!is_front_page()) { ?>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=538422112954938";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDv2XJALhgJj6-3WlChKdvNX2Co7NQYWhw&sensor=false"></script>
<?php } ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8138926901173174",
    enable_page_level_ads: true
  });
</script>


</head>

<body <?php body_class(); ?>>	
	
<div id="wrapper" class="hfeed">
    <div id="header">
    
        <!-- .inner_header -->	
    	<div class="inner_header poffice">
			<div class="logo">
				<?php /*?><a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>"><?php */?>
				<a href="/office-of-the-president/" title="<?php echo bloginfo(); ?>">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/presidentOffice/op-logo.png" />
				</a>
			</div>
			
			<div class="mstopRight">
				<div class="search">
					<form id="po_searchform" method="get" action="/">
						<input value="" name="s" id="s" placeholder="Search" type="text"> <input type="submit" id="submit" value="" />
					</form>
				</div>
				<div class="quickLinkBox">
					<a class="btn quickLinks" href="#">Quick Links</a>
					<?php if ( is_active_sidebar( 'wd-quick-links' ) ) : ?>
					<div class="quickMenu">
						<div class="quickMenuDiv">
							<?php dynamic_sidebar( 'wd-quick-links' ); ?>
							<a class="quickMenuHome" href="<?php echo home_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a>
						</div>
					</div>
                    
					<?php endif; ?>
				</div>
                
                
			</div>
		</div><!--.inner_head-->
      	
      	<div class="poffice_header_bottom">
      	
        <?php wp_nav_menu( array(  'container_class' => 'po_menu', 'theme_location' => 'president_office_top' ) ); ?>

		</div>
        <!-- .bottom_header -->
      
           <div class="vav_wrap_po">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i>---</i>≡</div>
            <?php wp_nav_menu( array(  'container_class' => 'po_menu', 'theme_location' => 'president_office_top','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
		
	</div><!-- #header -->
	<div id="main" class="president-office">