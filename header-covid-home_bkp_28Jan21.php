<?php
/**
 * Header template for header_covid_home
 */
?><!DOCTYPE html>
<html lang="en">
<html <?php language_attributes(); ?>><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=0.9" />
<meta name="google-site-verification" content="Ve2yybuuPVn3cyescM40ySPHPfUUgUoPFUs6gs351lA" />
<meta name="theme-color" content="#5f2468"/>
<title>Liberal Arts and Sciences University in Pakistan - Habib University</title>
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid_mobile.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
<!--<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css"/>  DEMO CSS REMOVED-->
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css"/>
<link rel="stylesheet" type="text/css" media="all" href="<?php  echo esc_url( get_template_directory_uri() ); ?>/css/responsive.css" />

<!-- CDN Fontawesome-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel='stylesheet' id='megamenu-css'  href='https://habib.edu.pk/wp-content/uploads/maxmegamenu/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='https://habib.edu.pk/wp-includes/css/dashicons.min.css?ver=4.9.10' type='text/css' media='all' />
<!--n2css--><script type='text/javascript' src='https://habib.edu.pk/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://habib.edu.pk/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://habib.edu.pk/wp-json/'/>
<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<!-- bxSlider CSS file -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"/>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lazysizes.min.js" async></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.js" async></script>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet" />
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/awesomplete.css" rel="stylesheet" />
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/awesomplete.js" async></script>              
<script>
    $(document).ready(function(){
        $(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
    
</script> 
<script>
    jQuery(document).ready(function($){
    $.daisyNav();
	});
</script>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>

<style>

.box {
    background: #e2d7bc;
    text-align: center;
    transition: all 0.5s linear;
    display: block;
    z-index: 999999;
    padding: 4px 0px;
}
#box a {
    font-size: 13px;
    border: 2px solid #5c2568;
    border-radius: 6px;
    padding: 1px 6px;
    margin: 0px 6px;
    color: #5c2568;
    font-weight: 700;
}
.hidden {
  display: none;
}

.visuallyhidden {
  opacity: 0;
}

button {
  display: block;
  margin: 0 auto;
  border: none;
}
.searchform {
    display: none;
    position: absolute;
    width: 700px;
    height: 70px;
    line-height: 40px;
    top: 0px;
    right: 0;
    padding: 0 15px;
    cursor: default;
    border-radius: 2px;
    border-style: solid;
    border-width: 1px;
    border-color: #e1e1e1;
    box-shadow: 0 3px 13px 0 rgba(0, 0, 0, 0.2);
    margin-left: -120px;
    z-index: 9999999;
    background-color: #fff;
}

.searchlink.open .searchform {
    display: block;
}

#search {
    display: block;
    position: relative;
}
.searchcst{
    width: 995px;
    float: right;
    text-align: right;
    margin-top: 17px;
    margin-bottom: 10px;
    font-family: 'Open Sans';
}

.sbtn {
    display: block;
    position: relative;
    background: none;
    border: none;
    color: #5c2568;
    font-size: 0.8em;
    font-weight: bold;
    border-radius: 6px;
    right: -14px;
    width: 80px;
    background-color: #f9b516;
    padding: 0px 0;
    margin-bottom: 1px;
}

.sbtn_cross{
    display: block;
    position: absolute;
    background: none;
    border: none;
    color: #5c2568;
    font-size: 0.9em;
    right: 10px;
    top: 14px;
    width: 0px;
    background-color: #fff;
}
.input-group {
    border-bottom: 2px solid #f9b516!important;
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 70%;
    float: right;
}
.search-form-wrapper {
    display: none;
    position: absolute;
    right: 40px;
    padding: 20px 15px;
    margin-top: 0px;
}
.search-form-wrapper.open {
    height: auto;
    background: #ffffff;
    display: block;
    position: absolute;
    padding: 3px 15px;
    top: 34px;
    width: 600px;
}
#wrapper .search{
    width: auto; 
    float: right; 
    text-align: left; 
    margin-top: 17px; 
    margin-bottom: 0px; 
    box-shadow: none;
}
.top_menu {
    width: auto;
    float: right;
    margin-top: 40px;
}
.menu-header{
    float: left;
}
.top_menu ul {
    width: 550px;
    list-style-type: none;
    margin: 0;
}
.cst-input-search{
    float: left;
    width: 330px;
    display: inline-flex;
    margin-right: 36px;
}
.input-group input {
    width: 290px;
    border-bottom: 1px solid #f9b516;
}
#box a {
    border: 2px solid #5c2568;
    border-radius: 6px;
    padding: 1px 12px;
    margin: 6px;
}
#box a i {
    font-size: 22px!important;
    margin: 0;
    padding: 0;
    vertical-align: bottom;
}
@media only screen only screen and (max-width: 736px) and (min-width: 360px){
  .logo a img {
    height: 150px;
  }
  .vav_wrap {
    display: none!important;
  }  
}



</style>
</head>
<body <?php body_class(); ?>>

<div id="box" class="box"><a target="_blank" href="/covid19/">Habib Response - Covid-19 </a>
                            <a target="_blank" href="/covid19/app/"><i class="fa fa-mobile" aria-hidden="true"></i> COVID-19 App</a>
    <!--<button style="background-color: #e2d7bc; float: right;"><i class="fa fa-times" aria-hidden="true"></i></button>-->
</div>

<script>
let box = document.getElementById('box'),
    btn = document.querySelector('button');

btn.addEventListener('click', function () {
  
  if (box.classList.contains('hidden')) {
    box.classList.remove('hidden');
    setTimeout(function () {
      box.classList.remove('visuallyhidden');
    }, 20);
  } else {
    box.classList.add('visuallyhidden');    
    box.addEventListener('transitionend', function(e) {
      box.classList.add('hidden');
    }, {
      capture: false,
      once: true,
      passive: false
    });
  }
  
}, false);
</script>


<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                    <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                    <?php if(is_front_page() || is_page_template('home-page.php')) : ?>
                    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"/> <!--Running Code-->
                    <?php else : ?>
                    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/>
                    <?php endif; ?>
                    </a>
                </div>
              
                <div id="nav-trigger" class="fa fa-bars"></div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                      
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav_inner' ) ); ?>
                <div class="hidden-xs navbar-form navbar-right" style="float: left;">
                    <a href="#search" class="search-form-tigger"  data-toggle="search-form"><i class="fa fa-search scst" aria-hidden="true"></i></a>
                </div>
      
                <div class="search">
        		
                 <div class="search-form-wrapper">
                   <form class="search-form" id="s" name="s" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                      <div class="input-group">
                         <input id="myinput" type="text" name="search" class="awesomplete" data-list="#mylist" placeholder="Search the site"><button class="sbtn">Search<i class="fa fa-search" aria-hidden="true"></i></button></input>
                        
                         
                         <!--<span class="input-group-addon search-close" id="basic-addon2"><i class="fa fa-close" aria-hidden="true"></i></span>-->
                         
                      </div>
                   </form>
                </div>
                    <script>$( document ).ready(function() {
                    $('[data-toggle=search-form]').click(function() {
                        $('.search-form-wrapper').toggleClass('open');
                        $('.search-form-wrapper .search').focus();
                        $('html').toggleClass('search-form-open');
                      });
                      $('[data-toggle=search-form-close]').click(function() {
                        $('.search-form-wrapper').removeClass('open');
                        $('html').removeClass('search-form-open');
                      });
                    $('.search-form-wrapper .search').keypress(function( event ) {
                      if($(this).val() == "Search") $(this).val("");
                    });
                    
                    $('.search-close').click(function(event) {
                      $('.search-form-wrapper').removeClass('open');
                      $('html').removeClass('search-form-open');
                    });
                    });
                    </script> 
                    <!-- -->
                    
                    <!-- -->
                </div>
                </div>
                
            </div><!--.inner_head-->
            
            <!--Mega Menu-->
          
                                    
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU  <i style="display: none; visibility: hidden;">---</i><i class="fa fa-bars"></i></div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                     <?php wp_nav_menu( array( 'theme_location' => 'mega_menu' ) ); ?>
                        <!-- Previous main menu
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?> -->
                    </div><!-- #access -->
                   
                 </div>   
        	</div>
            
            
            
          
        	</div>
	</div><!-- #header -->
            
         
            
          
        	</div>
	</div><!-- #header -->
	<div id="main">      
    
    	
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css"/>


