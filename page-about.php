<?php
/*
 Template Name: About sidebar
*/

// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<style>
	#nav_menu-41 h3{
		display:none;
	}
	</style>

		<div id="container">
			<div id="content" role="main">

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->

			<div id="primary" class="widget-area" role="complementary">
		  <h3 class="widget-title side"> <a href="/about-us/vision-values/">About</a> </h3>
		  <?php wp_nav_menu( array('container_class' => 'libraryInner', 'menu_class' => 'tb_side', 'theme_location' => 'about_sidebar' ) ); ?>
		</div>
			
		</div><!-- #container -->


<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>