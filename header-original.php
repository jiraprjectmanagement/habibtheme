﻿<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package WordPress
 * @subpackage Habib Uni
 * @since Habib Uni 2.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<meta name="google-site-verification" content="Ve2yybuuPVn3cyescM40ySPHPfUUgUoPFUs6gs351lA" />
<title>Liberal Arts &amp; Sciences University, Karachi Pakistan</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/lity.min.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_TRACKING_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'GA_TRACKING_ID');
</script>
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>
<?php /*?><script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "WebSite",
  "url": "https://www.habib.edu.pk/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.habib.edu.pk/?s={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script><?php */?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>
<?php if(is_front_page() || is_page_template('home-page.php')) { ?>
<!-- bxSlider CSS file -->
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<?php } ?>

<?php if(!is_front_page()) { ?>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=538422112954938";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php } ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8138926901173174",
    enable_page_level_ads: true
  });
</script>
</head>
<?php /*?><script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script><?php */?>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                <?php if(is_front_page() || is_page_template('home-page.php')) : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png">
                <?php else : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png">
                <?php endif; ?>
                </a>
                </div>
                <div id="nav-trigger">
                        <div>- <span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></div>
                </div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav' ) ); ?>
                
      
                <div class="search">
                	<?php
						
						//if ( is_user_logged_in() ) {
					?>
                    <?php		
						//}
						
					?>
                                       
                    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div>
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <?php /*?><div class="mw_button applyNow"><a href="//www.habib.edu.pk/orientation-2017/">Sign up for Orientation 2017</a></div><?php */?>
                            <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" id="s" />
                            <input type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search" />
                        </div>
                    </form>
                </div>
                </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i>---</i>≡</div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
	</div><!-- #header -->
	<div id="main">
           
            	
<?php 
	if(!is_tax( 'speakers' ) && get_field('redirection')) {
			$mwlocation = get_field('redirection');
		header("Location: $mwlocation");
	} 
?>       
<?php header("Access-Control-Allow-Origin: https://habib.edu.pk")?>
<?php header("Access-Control-Allow-Methods:POST ")?>
<?php header("Access-Control-Allow-Headers:Content-Type")?>