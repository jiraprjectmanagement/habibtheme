﻿<?php
/**
 * Header template for liberal-core
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title>THE HABIB UNIVERSITY LIBERAL CORE</title>
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" /> 
<!--<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css"/>  DEMO CSS REMOVED-->
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css"/>
<!-- CDN Fontawesome-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<!-- bxSlider CSS file -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"/>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lazysizes.min.js" async></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.js" async></script>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet" />
<meta property="og:url" content="<?php echo get_permalink($post->ID); ?>"/>
<meta property="og:title" content="<?php echo $post->post_title; ?>"/>

<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
                    
<script>
    $(document).ready(function(){
        $(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
    
</script> 
<script>
    jQuery(document).ready(function($){
    $.daisyNav();
	});
</script>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>

<style>

.selector_circle{
        border-radius: 50%;
    color: #fff;
    background: #5c2568;
    width: 160px;
    height: 160px;
    line-height: 16px;
    box-shadow: 0 3px 3px rgba(0, 0, 0, 0.1);
    font-size: 14px;
    padding: 10px;
    text-align: center;
    padding: 20px;
    margin-bottom: 10px;
}

.col-sm-6.rgt-circle, .col-sm-6.lft-circle{
    float: left;
    width: 50%;
}

</style>
</head>

<body <?php body_class(); ?>>
	
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                <?php if(is_front_page()) : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"/>
                <?php else : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"/>
                <?php endif; ?>
                </a>
                </div>
                <div id="nav-trigger" class="fa fa-bars">
                        
                </div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                        <div class="nav-mid"><?php wp_nav_menu( array('theme_location' => 'mid_nav' ) ); ?></div> 
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav_inner' ) ); ?>
                
      
                <div class="search">
                	<div class="search_mid_nav"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'mid_nav' ) ); ?> </div>
                     <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                       
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" id="s"/>
                            <button type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search"/><i class="fa fa-search"></i></button>
                    </form>                    
                </div>
                </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i class="fa fa-bars"></i></div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
	</div><!-- #header -->
	<div id="mainlc" class="oge">
    	
     