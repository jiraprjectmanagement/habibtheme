<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 */
?>

<div id="primary" class="widget-area oap-widgets" role="complementary">

<?php

$my_array = array('a','b','c','d','e');

for ($i=0; $i<2; $i++) {
    $random = array_rand($my_array);  # one random array element number
    $get_it = $my_array[$random];	  # get the letter from the array
	$my_array = array_diff($my_array, [$get_it]);
//	echo $get_it."<br />";
}
// print_r($my_array);

?>

<?php

// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'oapbar' ) ) : ?>
		<?php dynamic_sidebar( 'oapbar' ); ?>
<?php
	endif; 
?>
</div>

