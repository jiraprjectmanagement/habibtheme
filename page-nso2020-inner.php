<?php
/*
 Template Name: NSO 2020 Inner
*/
// get_header(''); ?>

<?php get_header('with-megamenu-live'); ?>

<!-- <div id="header"></div> -->
     <!--container_ad class removed due to ad keyword -->
<div id="container_missions"> 
   
     <div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu1">                
                <?php echo "<text class='entry-para'><i class='fa fa-home' style='text-align:center;'></i><a href='/welcome-class/'>Back To Main</a></textp>";?>
                <?php echo "<h1 class='entry-title'>".'&nbsp;'.get_the_title()."</h1>"; ?>
               </div>
		</div>
	</div>

	<div id="content_stdaffairs" role="main" class="adm_inner" >
 
          <?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?> 
     </div> 
           
     <div id="primary_nso2020" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>       
            <?php  dynamic_sidebar( 'nso2020_sidebar01' ); ?>
     </div> 
</div>

<!-- <script>$("#accordion").accordion({ collapsible: true, active: false });</script> -->

<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>