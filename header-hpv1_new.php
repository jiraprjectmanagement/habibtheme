<?php
/**
 * Header template for homepagev1
 */
?><!DOCTYPE html>
<html lang="en">
<html <?php language_attributes(); ?>><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=0.9" />
<meta name="google-site-verification" content="Ve2yybuuPVn3cyescM40ySPHPfUUgUoPFUs6gs351lA" />
<meta name="theme-color" content="#5f2468"/>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
    <link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style.css" />
    <link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/> 
    <link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid_mobile.css" />
    <link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
    <link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/menu/daisynav.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<!-- CDN Fontawesome-->
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
<!-- bxSlider CSS file -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"/>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lazysizes.min.js" async></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.js" async></script>
<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet" />
<meta property="og:url" content="<?php echo get_permalink($post->ID); ?>"/>
<meta property="og:title" content="<?php echo $post->post_title; ?>"/>

<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
                    
<script>
    $(document).ready(function(){
        $(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
    
</script> 
<script>
    jQuery(document).ready(function($){
    $.daisyNav();
	});
</script>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<style>

    .box {
        background: #e2d7bc;
        text-align: center;
        transition: all 0.5s linear;
        display: block;
        z-index: 999999;
        padding: 8px 0px;
    }
    #box a {
        font-size: 13px;
        border-bottom: 1px solid hsl(289deg 48% 28% / 41%);
        border-radius: 0;
        padding: 5px 12px;
        margin: 6px;
        color: #5c2568;
        font-weight: 700;
        
    }
    
    .hidden {
      display: none;
    }
    
    .visuallyhidden {
      opacity: 0;
    }
    
    button {
      display: block;
      margin: 0 auto;
      border: none;
    }
    .searchform {
        display: none;
        position: absolute;
        width: 700px;
        height: 70px;
        line-height: 40px;
        top: 0px;
        right: 0;
        padding: 0 15px;
        cursor: default;
        border-radius: 2px;
        border-style: solid;
        border-width: 1px;
        border-color: #e1e1e1;
        box-shadow: 0 3px 13px 0 rgba(0, 0, 0, 0.2);
        margin-left: -120px;
        z-index: 9999999;
        background-color: #fff;
    }
    
    .searchlink.open .searchform {
        display: block;
    }
    
    #search {
        display: block;
        position: relative;
    }
    .searchcst{
        width: 995px;
        float: right;
        text-align: right;
        margin-top: 17px;
        margin-bottom: 10px;
        font-family: 'Open Sans';
    }
    
    .sbtn {
        display: block;
        position: relative;
        background: none;
        border: none;
        color: #fff;
        font-size: 0.95em;
        font-weight: bold;
        border-radius: 6px;
        right: -8px;
        width: 101px;
        background-color: #5c2568;
        padding: 3px 0;
        margin-bottom: 1px;
    }
    
    .sbtn_cross{
        display: block;
        position: absolute;
        background: none;
        border: none;
        color: #5c2568;
        font-size: 0.9em;
        right: 10px;
        top: 14px;
        width: 0px;
        background-color: #fff;
    }
    .input-group {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-align: stretch;
        align-items: stretch;
        width: auto;
        float: right;
    }
    .search-form-wrapper {
        display: none;
        position: absolute;
        right: 40px;
        padding: 20px 15px;
        margin-top: 0px;
    }
    .search-form-wrapper.open {
        height: auto;
        background: #fff;
        display: block;
        position: absolute;
        padding: 12px 15px;
        top: 24px;
        width: 600px;
        border-radius: 12px;
            right: 60px;
    }
    .logohide{
        visibility: hidden;
    }
    #wrapper .search{
        width: auto; 
        float: right; 
        text-align: left; 
        margin-top: 17px; 
        margin-bottom: 0px; 
        box-shadow: none;
    }
    .top_menu {
        width: auto;
        float: right;
        margin-top: 40px;
    }
    .menu-header{
        float: left;
    }
    .top_menu ul {
        width: 550px;
        list-style-type: none;
        margin: 0;
    }
    .cst-input-search{
        float: left;
        width: 330px;
        display: inline-flex;
        margin-right: 36px;
    }
    .input-group input {
        width: 462px;
        border-bottom: 1px solid #e2d7bc !important;
        background: transparent !important;
        color: black !important;
    }
    
    #box a i {
        font-size: 22px!important;
        margin: 0;
        padding: 0;
        vertical-align: bottom;
    }
    @media only screen and (max-width: 736px) and (min-width: 360px){
      .logo a img {
        height: 150px;
      }
      #mega-menu-wrap-mega_menu .mega-menu-toggle + #mega-menu-mega_menu { padding: 0px 0px 60px 0px; position: absolute;  width: 100%;   z-index: 9999999;   box-shadow: 2px 5px 62px #5f266b; }
    }
    </style>
</head>
<body <?php body_class(); ?>>

    <div id="box" class="box"><a target="_blank" href="/covid19/">Habib Response - Covid-19 </a><span>|</span><a target="_blank" href="/covid19/app/"><i class="fa fa-mobile" aria-hidden="true"></i> COVID-19 App</a>
        <!--<button style="background-color: #e2d7bc; float: right;"><i class="fa fa-times" aria-hidden="true"></i></button>-->
    </div>

    <script>
        let box = document.getElementById('box'),
            btn = document.querySelector('button');
        
        btn.addEventListener('click', function () {
          
          if (box.classList.contains('hidden')) {
            box.classList.remove('hidden');
            setTimeout(function () {
              box.classList.remove('visuallyhidden');
            }, 20);
          } else {
            box.classList.add('visuallyhidden');    
            box.addEventListener('transitionend', function(e) {
              box.classList.add('hidden');
            }, {
              capture: false,
              once: true,
              passive: false
            });
          }
          
        }, false);
        </script>
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                    <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                    <?php if(is_front_page() || is_page_template('home-page.php')) : ?>
                    	<img class="mbl_logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/> <!--Running Code-->
                        	<img class="web_logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/>
                    <?php else : ?>
                    	<img class="web_logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/>
                        <img class="mbl_logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/> <!--Running Code-->
                    <?php endif; ?>
                    </a>
                </div>
              
                <div id="nav-trigger" class="fa fa-bars"></div>
                <nav id="nav-mobile">
                    <?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                    <!-- <div class="nav-mid"><?php // wp_nav_menu( array('theme_location' => 'mid_nav' ) ); ?></div>   -->
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav_inner' ) ); ?>
                    <div class="hidden-xs navbar-form navbar-right" style="float: left;">
                        <a href="#search" class="search-form-tigger"  data-toggle="search-form"><i class="fa fa-search scst" aria-hidden="true"></i></a>
                    </div>
      
                    <div class="s">
        		
                        <div class="search-form-wrapper">
                            <form class="search-form" id="s" name="s" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <div class="input-group">
                                    <input id="s" type="text" name="s" class="awesomplete" data-list="#mylist" placeholder="Search the site"><button class="sbtn search-close" >Search<i class="fa fa-search" id="foo" aria-hidden="true"></i></button></input>
                                
                                </div>
                            </form>
                        </div>

                        <script>
                            $( document ).ready(function() {
                            
                            
                            var element = document.getElementById("mega-menu-item-263218");
                                element.classList.remove("mega-toggle-on");
                            
                            
                            
                        
                            $('[data-toggle=search-form]').click(function() {
                                $('.search-form-open').removeClass('open');
                                $('.search-form-wrapper').toggleClass('open');
                                $('.mbl_logo').toggleClass('logohide');
                                $('.fa.fa-search.scst').toggleClass('fa-close');
                                $('.search-form-wrapper .s').focus();
                                $('html').toggleClass('search-form-open');
                            });
                            $('[data-toggle=search-form-close]').click(function() {
                                $('.search-form-wrapper').removeClass('open');
                                $('.fa.fa-search.scst').removeClass('fa-close');
                                $('html').removeClass('search-form-open');
                            });
                            $('.search-close').click(function(event) {
                                
                            $('.search-form-wrapper').removeClass('open');
                            $('html').removeClass('search-form-open');
                            });
                            $('.sbtn ').click(function(event) {
                                
                                $('.mbl_logo').removeClass('logohide');
                            });
                            
                            
                            });
                            
                            jQuery(document).ready(function( $ ) {
                                jQuery('.mega-menu-toggle').on('click', function(e) {
                                    jQuery('.max-mega-menu').data('maxmegamenu').hideAllPanels();
                                });
                            });
                            
                            
                        </script> 
                    </div>
            </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
                <div class="res_menues"> <?php wp_nav_menu( array( 'theme_location' => 'mega_menu' ) ); ?>
                </div>
              </div>
            
          
        	</div>
	</div><!-- #header -->
            
         
            
          
        	</div>
	</div><!-- #header -->
	<div id="main">      	
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css"/>

<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/custom-mobile-style.css" />


