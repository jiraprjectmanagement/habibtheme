<?php
/**
 * Footer template for President Office
 *
 */
?>

</div>
<!-- #main -->
<div id="footer" class="po-footer" role="contentinfo">
  <div class="inner_footer">
	<div class="poFtLeft">
		<div class="poflogo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/presidentOffice/op-jlogo.png" /></div>
		<div class="pofmail">
			<ul>
			  <li><a href="http://facebook.com/hupres/" target="_blank">facebook.com/hupres</a></li>
			</ul>
		</div>
	</div>
	<div class="poFtRight">
		<?php wp_nav_menu( array(  'container_class' => 'ftr_menu', 'theme_location' => 'president_office_footer' ) ); ?>
	</div>
  </div>
</div>
<!-- #footer -->
</div>
<!-- #wrapper -->
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js"></script>
<?php
	/*
	 * Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>

</body></html>
