<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 */
?>

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'newsibar' ) ) : ?>
		<?php $categories = wp_get_post_terms($post->ID, 'event-categories', array("fields" => "all")); ?>
        <div id="primary" class="widget-area <?php echo $categories[0]->slug; ?>" role="complementary" style="margin-top:13.8%;">
        	<div class="btcategory">
        		<?php // $key = array_search(idrac, array_column($categories, 'slug')); ?>
				
					<?php 
						if($categories[1]->slug == 'idrac') {
							echo '<a href="/events/categories/'.$categories[1]->slug.'">Go Back: '.$categories[1]->name.'</a>';
						} else {
							echo '<a href="/events/categories/'.$categories[0]->slug.'">Go Back: '.$categories[0]->name.'</a>';
						} 
					?>
           		
            	<?php
					// echo '<a href="/events/categories/'.$categories[0]->slug.'">Go Back: '.$categories[0]->name.'</a>';
				?>
            </div>
            	
				<?php dynamic_sidebar( 'newsibar' ); ?>
		</div>
<?php endif; ?>
<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

	<!-- #secondary .widget-area -->

<?php endif; ?>
