<?php
/*
 Template Name: Arzu Center 
*/
// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/arzu.css">

 <div class="mw_center_logo">
        <?php dynamic_sidebar( 'center_logo' ); ?>
    </div>
  <?php echo do_shortcode('[rev_slider alias="arzu"]');?>
    <div class="mw_arzu_tag">
        <div class="inner_wraper">
        
        <?php  if ( have_posts() ) :
         while (have_posts()) : the_post();
            the_content();
         endwhile; endif; ?>
        </div>
    </div>
    <!-- PAST EVENTS IDRAC | START-->
	 <div class="mwPastEvents">
            	<?php
					$events = EM_Events::get(array('scope'=>'past', 'category'=>'arzucenter', 'limit'=>4));
					if(!empty($events)) {
				?>	
              <!--   <h2>Past Events | <a class="viewall" href="https://habib.edu.pk/arzu-events/">View all</a></h2> -->
                <?php
					}
					foreach( $events as $EM_Event ){ ?>
                    	<?php #var_dump($EM_Event->output("#_EVENTURL")) ?>
						<article class="ycsdBox">
                        	<a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>"><img class="idrac_article_img" src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" width="230" height="97" class="top"></a>
                            <div class="ycsdInner">
                                <h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo $EM_Event->output("#_EVENTNAME"); ?></a></h4>
                                <p class="ycsdText"><?php echo string_limit_words($EM_Event->output("#_EVENTEXCERPT{10}"), 20).'...'; ?></p>
                                <a class="ycsdReadMore" href="<?php echo $EM_Event->output("#_EVENTURL"); ?>">Read More...</a>
                            </div>
                            <div class="ycsdBtn">
                            <!--	<?php if($EM_Event->output('#_ATT{Video url}')) { ?><a class="watchVideo" href="<?php echo $EM_Event->output('#_ATT{Video url}'); ?>" rel="wp-video-lightbox"><i class="fa fa-play-circle-o"></i> Watch Video</a><?php } ?> -->
                                <?php if($EM_Event->output('#_ATT{Poster}')) { ?><a data-lity class="pressRelease" href="<?php echo $EM_Event->output('#_ATT{Poster}'); ?>" target="_blank"> Poster</a><?php } ?>
                                <?php /*?><?php if($EM_Event->output('#_ATT{Press Release}')) { ?><a class="pressRelease" href="<?php echo $EM_Event->output('#_ATT{Press Release}'); ?>" target="_blank"><i class="fa fa-file-text-o"></i> Press Release</a><?php } ?><?php */?>
                            </div>
                        </article>
				<?php
					}
				?>
           <div class="mwPastEvents-btn-main" style=" 
           margin-bottom: 10px !important;
    background-color: #ffffff;
    border: 1px solid #5c2568;
    border-radius: 0px;
    color: #5c2568;
    padding: 12px 10px;
    font-size: 17px;
    font-weight: 500;
    text-align: center;
    width: 150px;
    overflow: hidden;
    margin: 6px auto;
    float: none;"><a style="color: #5c2568; text-decoration: none;" title="" 
           href="https://habib.edu.pk/arzu-events/" target="_blank" rel="noopener noreferrer">View all events</a></div>                     
    </div>
<!-- PAST EVENTS IDRAC | END-->
<!-- SUBSCRIBERS FORM -->
<section id="connected-cmd">
<div class="contact-cmd">
<h2 style="padding-bottom: 30px;
    font-size: 1.4em;
    color: #fff;">Subscribe to our mailing list!</h2>
<script src="https://form.jotform.com/jsform/50831219925961" type="text/javascript"></script>
</div>
<div class="map-cmd"><div style="width: 100%"><iframe width="100%" height="350" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=abib%20University%2C%20Block%2018%2C%20Gulistan-e-Jauhar%20%E2%80%93%20University%20Avenue%2C%20Off%20Shahrah-e-Faisal%2C%20Karachi%20+(Habib%20University)&amp;ie=UTF8&amp;t=&amp;z=17&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="#"></a></iframe></div></div>
</section>
<!-- SUBSCRIBERS FORM -->
<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>

<?php if(wp_is_mobile()){ ?>
<script>
$('.mw_arzu_tag p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});
</script>
<?php } ?>
