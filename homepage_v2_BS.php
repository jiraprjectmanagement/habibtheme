<?php
/*
 Template Name: homepage_v2_BS
*/
get_header('v2_BS'); ?>


<picture>
    <source type="image/webp"  srcset="https://habib.edu.pk/docs/sliders/web0-slider-global-home.webp">
    <source type="image/png" srcset="https://habib.edu.pk/docs/sliders/web0-slider.jpg">
    <img src="/docs/sliders/web0-slider.jpg">
</picture>



<div class="container-xl">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="/docs/sliders/web0-slider.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
         
        </div>
      </div>

      <div class="item">
        <img src="/docs/sliders/virtualtour_slider.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>Launch Virtual Tour</h3>
          <p>While Habib University remains physically closed as a precautionary measure to lower the risk of COVID-19 exposure to our community,
students, faculty and staff have all moved online and prospective students can take a virtual tour and explore the campus safely from home.</p>
        </div>
      </div>

    <div class="item">
        <img src="/docs/sliders/Covid-full-width-2020-3.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          
        </div>
      </div>
      
       <div class="item">
        <img src="/docs/second-slider.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>Making Quality Education Affordable</h3>
          <p>Unmatched Scholarship and Financial Aid program</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
        
    		<div class="container-xl">	
     
              <!--Slider Ended-->
            <div class="homebox">
                <h2>Welcome to Habib University, providing world-class Liberal Arts and Sciences Education in Pakistan.</h2>
                <p>Habib University remains the hub of Liberal Arts and Sciences Education in Pakistan, ever since its inception in 2014. Offering a truly transformative and evolved learning experience, it is an institution that truly redefines higher education in the country and its nearby regions.
                
                The university operates through the fulfilment of its core philosophy, derived from the Arabic word "Yohsin". This term broadly defines the notions of thoughtful self-cultivation, grounded on the five values of Excellence, Beauty, Passion, Respect and Service.
                
                The aim that Habib University wants to achieve through the aforementioned virtues and a comprehensive, interdisciplinary curriculum, is to produce critically-conscious, ambitious and determined graduates who build a name for themselves individually, and positively impact their society.</p>
            
            </div>
            <!--Description Ended -->
            <div class="full home-tabs">	
        		<div class="hmbox "><a href="/academics/ahss/" rel="noopener noreferrer" title="/academics/ahss/"><img src="/wp-content/uploads/2019/10/ahss-thumb.jpg" alt="School of Arts, Humanities &amp; Social Sciences"><span class="indigo">School of Arts, Humanities &amp; Social Sciences</span></a></div>
        	
        		<div class="hmbox "><a href="/academics/sse/" rel="noopener noreferrer" title="/academics/sse/"><img src="/wp-content/uploads/2019/10/dsse-thumb.jpg" alt="Dhanani School of Science &amp; engineering"><span class="indigo">Dhanani School of Science &amp; engineering</span></a></div>
        	
        		<div class="hmbox "><a href="/campus-tour/" rel="noopener noreferrer" title="/campus-tour/"><img src="/wp-content/uploads/2019/10/campus-thumb.jpg" alt="Campus Tour / Campus Spaces"><span class="indigo">Campus Tour / Campus Spaces</span></a></div>
            </div>
            <!--Ratio Stats-->
            <div class="full clearfix">	
            		<div class="stats_giving"><div class="stats"><ul id="stats">	
            		<li><span class="nums"><p>AROUND</p><a target="_blank" rel="noopener noreferrer" href="/admissions/student-finance/">90%</a></span><p>STUDENTS RECEIVE SOME
            FORM OF FINANCIAL SUPPORT</p></li>
            	
            		<li><span class="nums"><p></p><a target="_blank" href="/academics/" rel="noopener noreferrer">6</a></span><p>WORLD CLASS
            DEGREES</p></li>
            	
            		<li><span class="nums"><p></p><a target="_blank" href="/student-affairs/" rel="noopener noreferrer">12:1</a></span><p>STUDENT
            TO FACULTY RATIO</p></li>
            </ul><span class="tag-line">HU BY THE NUMBERS</span> </div><div class="hm_giving" ><a title="Habib University" href="/giving/"><img src="/wp-content/uploads/2019/10/giving-thumb-2.jpg" alt="giving"><span class="yellow">GIVING</span></a></div></div>
            </div>
            <!--Ratio Stats-->
            <!--Latest News API-->
            <div class="homeNewsEvents">
            <h2>LATEST NEWS &amp; UPCOMING EVENTS</h2>
            <div class="hucontent">
            <?php
            //<!--Latest News API-->
            //$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?categories=254");
            //Live news category id: 254
            //http://habib.edu.pk/HU-news/wp-json/wp/v2/posts/?&categories_exclude=255,257&per_page=$mwNum
            $response = wp_remote_get( "http://habib.edu.pk/HU-news/wp-json/wp/v2/posts/?&categories_exclude=255,257&per_page=5");
                if( is_wp_error( $response ) ) {
            		return;
            	}
            	$posts = json_decode( wp_remote_retrieve_body( $response ) );
                $i=0;
            	if( empty( $posts ) ) {
            		return;
            	}
                
                
                	foreach( $posts as $post ) {
                	   // This loop runs only 5 times as shown below
                	    if($i==5) break;
                    ?>
                    		<article id="post-<?php echo $post->id; ?>">
                    		<?php 
                    			$img = $post->acf_field->home_page_thumbnail;
                    			if($img) {
                    		?>
                            	<a target="_blank" href="<?php echo $post->link; ?>"><img src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" /></a>
                            <?php } ?>
                            	<div class="art_content">
                                	<h3><?php echo $post->title->rendered; ?></h3>
                                	<a target="_blank" href="<?php echo $post->link; ?>" rel="noopener noreferrer">Read more on HU News</a>
                            	</div>
                        	</article>

                    <?php $i++; } ?>

            
            </div>
            </div>
                             </div>
                 
 			 <!--Archival Section 4 cols start -->
		<div class="container-fluid">
            <div class="homeNewsEvents">
            <h2>HIGHLIGHTS FROM ARCHIVES</h2>
            </div>
         </div>
         
         <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                    <h2 class="col_heading text-center">GLOBAL</h2>
                        <article id="post-5389">
      
                            <a target="_blank" href="/HU-news/building-global-partnerships-dr-waqar-saleems-faculty-exchange-experience-at-pitzer-college/">
                            </a><div class="tooltip_h"><a target="_blank" href="/HU-news/building-global-partnerships-dr-waqar-saleems-faculty-exchange-experience-at-pitzer-college/">
                            <img class="widget_img" src="/HU-news/wp-content/uploads/2019/08/global-pitzer-thumb.jpg" alt="Building Global Partnerships: Dr. Waqar Saleem's Faculty Exchange Experience at Pitzer College"></a>
                            <span class="tooltiptext">Building Global Partnerships: Dr. Waqar Saleems Faculty Exchange Experience at Pitzer College</span>
                            </div>
                            <div class="art_content">
                                <h3>Building Global Partnerships: Dr. Waqar Saleem's Faculty ...</h3>
                        	</div>
                        </article>
                        <article id="post-5369">
                            </a><div class="tooltip_h"><a target="_blank" href="https://habib.edu.pk/HU-news/study-abroad-program-hu-students-excel-at-stanford-uc-berkeley-and-texas-am/">
                            <img class="widget_img" src="https://habib.edu.pk/HU-news/wp-content/uploads/2019/08/HU-News-SAP.jpg" alt="Study Abroad Program: HU Students Excel at Stanford, UC Berkeley and Texas A&amp;M"></a>
                            <span class="tooltiptext">Study Abroad Program: HU Students Excel at Stanford, UC Berkeley and Texas A&amp;M</span>
                            </div>
                          <div class="art_content">
                            <h3>Study Abroad Program: HU Students Excel at Stanford, UC B...</h3>
                    	   </div>
                	</article>
                    <article id="post-5218">
                          <a target="_blank" href="https://habib.edu.pk/HU-news/renowned-scholar-reza-aslan-speaks-at-habib-universitys-yohsin-lecture/">
                            </a><div class="tooltip_h"><a target="_blank" href="https://habib.edu.pk/HU-news/renowned-scholar-reza-aslan-speaks-at-habib-universitys-yohsin-lecture/">
                            <img class="widget_img" src="https://habib.edu.pk/HU-news/wp-content/uploads/2019/06/reza-thumb.jpg" alt="Renowned Scholar Reza Aslan Speaks at Habib University's Yohsin Lecture"></a>
                            <span class="tooltiptext">Renowned Scholar Reza Aslan Speaks at Habib University�s Yohsin Lecture</span>
                            </div>
                           <div class="art_content">
                                <h3>Renowned Scholar Reza Aslan Speaks at Habib University's ...</h3>
                        	</div>
            	   </article> 
                    </div>
                   
                    <div class="col-sm-3">
                    <h2 class="col_heading text-center">STUDENT ACHIEVEMENTS</h2>
                        <article id="post-5455">
      
                                        		                                                	<a target="_blank" href="https://habib.edu.pk/HU-news/sussex-summer-story/">
                                                    </a><div class="tooltip_h"><a target="_blank" href="https://habib.edu.pk/HU-news/sussex-summer-story/">
                                                    <img class="widget_img" src="https://habib.edu.pk/HU-news/wp-content/uploads/2019/09/omema-thumbnail-1.jpg" alt="Shakespeare, Stonehenge &amp; Brighton Pier: My Sussex Summer Story"></a>
                                                    <span class="tooltiptext">Shakespeare, Stonehenge &amp; Brighton Pier: My Sussex Summer Story</span>
                                                    <!-- -->
                                                    </div>
                                                                                                <div class="art_content">
                                                        <h3>Shakespeare, Stonehenge &amp; Brighton Pier: My Sussex S...</h3>
                                                	</div>
                                            	</article>
<article id="post-5331">
      
                                        		                                                	<a target="_blank" href="https://habib.edu.pk/HU-news/from-utah-to-the-smoky-mountains-my-study-abroad-experience/">
                                                    </a><div class="tooltip_h"><a target="_blank" href="https://habib.edu.pk/HU-news/from-utah-to-the-smoky-mountains-my-study-abroad-experience/">
                                                    <img class="widget_img" src="https://habib.edu.pk/HU-news/wp-content/uploads/2019/07/student-ugrad-thumb.jpg" alt="From Utah to the Smoky Mountains: My UGRAD Study Abroad Experience"></a>
                                                    <span class="tooltiptext">From Utah to the Smoky Mountains: My UGRAD Study Abroad Experience</span>
                                                    <!-- -->
                                                    </div>
                                                                                                <div class="art_content">
                                                        <h3>From Utah to the Smoky Mountains: My UGRAD Study Abroad E...</h3>
                                                	</div>
                                            	</article>
<article id="post-5148">
      
                                        		                                                	<a target="_blank" href="https://habib.edu.pk/HU-news/celebrating-excellence-at-habib-university/">
                                                    </a><div class="tooltip_h"><a target="_blank" href="https://habib.edu.pk/HU-news/celebrating-excellence-at-habib-university/">
                                                    <img class="widget_img" src="https://habib.edu.pk/HU-news/wp-content/uploads/2019/05/1a-1.jpg" alt="Celebrating Excellence at Habib University"></a>
                                                    <span class="tooltiptext">Celebrating Excellence at Habib University</span>
                                                    <!-- -->
                                                    </div>
                                                                                                <div class="art_content">
                                                        <h3>Celebrating Excellence at Habib University</h3>
                                                	</div>
                                            	</article> 
                    </div>
                    <div class="col-sm-3">
                    <h2 class="col_heading text-center">MOHSINEEN</h2>
                        
                    </div>
                    
                     
                    <div class="col-sm-3">
                    <h2 class="col_heading text-center">SHORT VIDEOS</h2>
                    
                    <div class="video-thumbnail">
                    <a href="https://www.facebook.com/HabibUniversity/videos/377535579803160/" target="_blank">
                    <div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-cls-thumb.jpg" />
                    <span class="tooltiptext">Comparative Liberal Studies</span></a></div>
                    </div>
                    <div class="art_content_4">
                    <h3>Comparative Liberal Studies</h3>
                    </div>
                    <!--VideoBOXEnd -->
                    
                    <div class="video-thumbnail">
                    <a href="https://www.facebook.com/HabibUniversity/videos/388720728484499/" target="_blank">
                    <div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-orientation-thumb.jpg" />
                    <span class="tooltiptext">New Student Orientation 2019</span></a></div>
                    </div>
                    <div class="art_content_4">
                    <h3>New Student Orientation 2019</h3>
                    </div>
                    <div class="video-thumbnail">
                    <a href="https://www.facebook.com/HabibUniversity/videos/383614388969562/" target="_blank">
                    <div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-parents-thumb.jpg" />
                    <span class="tooltiptext">Parents Night 2019</span></a></div>
                    </div>
                    <div class="art_content_4">
                    <h3>Parents Night 2019</h3>
                    </div>
                    
                    </div>
                </div>
            </div>
         </div>
         
         
        
        

