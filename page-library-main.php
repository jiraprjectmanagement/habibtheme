<?php
/*
 Template Name: Library Main 
*/
// get_header('library'); ?>

<?php get_header('with-megamenu-live'); ?>


<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tab.min.js"></script>

	<?php /*?><?php if(get_field('logo')) { ?>
    <div class="mw_center_logo">
        <a href="<?php the_field( 'logo_link' ); ?>"><img src="<?php the_field( 'logo' ); ?>" /></a>
    </div>
    <?php } ?><?php */?>
	<div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_library_menu">
				<?php wp_nav_menu( array('container_class' => 'libraryMenu', 'theme_location' => 'library_nav' ) ); ?>
			</div>
		</div>
	</div>
   	<div class="mw_library_mid">
   		<div id="library_lftrth" class="inner_wraper">
   			<div class="library_lft">
   				<div class="ui top attached tabular menu">
				  <a class="item active" data-tab="first">Catalog Search</a>
				  <a class="item" data-tab="second">Google Scholar</a>
				  <a class="item" href="https://habib.edu.pk/library/covid-19-learning-continuity/" target="_self">COVID-19 & Learning Continuity</a>
				</div>
				<div class="ui bottom attached tab segment active" data-tab="first">
				  <form id="catlogSearch" action="https://catalog.habib.edu.pk/cgi-bin/koha/opac-search.pl" method="get" target="_blank">
					  <div>
						  <aside class="csLeft"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_tab.jpg" /></aside>
						  <aside class="csRight">
						  	<input type="text" name="q" class="srhField" value="" placeholder="Search HU Catalog"/>
						  	<input type="submit" name="submit" value="Submit" class="srhBtn"/>
						  </aside>
						  <aside class="csAdv"><a href="https://catalog.habib.edu.pk/cgi-bin/koha/opac-search.pl" target="_blank">Advanced Search</a></aside>
					  </div>
				  </form>
				</div> 
				<div class="ui bottom attached tab segment" data-tab="third">
				  <?php /*?><?php the_field('journal_database'); ?><?php */ ?> 
				  
				  	<div class="mlogos">
						<a href="http://dl.acm.org/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/1.png" /></a>
						<a href="https://ebookcentral.proquest.com/auth/lib/fjpw/login.action?returnURL=https%3A%2F%2Febookcentral.proquest.com%2Flib%2Ffjpw%2Fhome.action" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/2.png" /></a>
						<a href="https://link.springer.com/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/3.png" /></a>
						<a href="https://muse.jhu.edu/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/4.png" /></a>
						<a href="https://www.jstor.org/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/5.png" /></a>
						<a href="http://www.oxfordreference.com/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/6.png" /></a>
						<a href="http://www.tandfonline.com/" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/library/7.png" /></a>
					</div>
				</div>
				
				
				<div class="ui bottom attached tab segment" data-tab="second">
                    <form id="scholarbounce" action="https://scholar.google.com/scholar" method="get" name="q" target="_blank">
					  <div>
						  <aside class="csLeft"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_tabscholarjpg.jpg" /></aside>
						  <aside class="csRight">
						  	<input type="text" name="q" class="srhField" value="" placeholder="Search Google Scholar">
						  	<input type="submit" name="submit" value="Submit" class="srhBtn">
						  </aside>
		
					  </div>
				  </form>
  				
				</div>
				
				<script>
					jQuery('.menu .item').tab();
				</script>
			</div><!-- .library_lft -->
			<div class="library_rth">
				<?php echo do_shortcode(get_field('slider_shortcode')); ?>
			</div>
		</div>
    	
     	<div class="inner_wraper">
     		<div class="library_timing">
				<h3>Library Hours: Monday to Friday 8:00 a.m. to 4:30 p.m. Working Saturday 9:00 a.m. to 3:00 p.m.</h3>
                <!--<span>Mon-Friday 8.00 a.m. to 7.00 p.m.</span>
				<span>Working Saturdays 8.00 a.m. to 5.00 p.m.</span>
                <span>Monday to Saturday 10:00 a.m. to 8:00 p.m.</span>>-->
		  	</div>
		</div>
        <div id="contentPart" class="inner_wraper">
        	<?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
        </div>        
    </div>

	
<!--Start of Tawk.to Script-->
	  <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b18f40710b99c7b36d4b5cd/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<?php // get_footer('library'); ?>

<?php get_footer('footer-live'); ?>