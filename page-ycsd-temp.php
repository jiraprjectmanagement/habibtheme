<?php
/*
 Template Name: YCSD Center 
*/

get_header('ycsd'); ?>

	<?php /*?><?php if(get_field('logo')) { ?>
    <div class="mw_center_logo">
        <a href="<?php the_field( 'logo_link' ); ?>"><img src="<?php the_field( 'logo' ); ?>" /></a>
    </div>
    <?php } ?>
    <div class="mw_ycsd_menu">
    	<?php wp_nav_menu( array('container_class' => 'ycsdMenu', 'theme_location' => 'ycsd_nav' ) ); ?>
    </div><?php */?>		
    <div class="mw_ycsd_slider">
    	<?php echo do_shortcode(get_field('rev_slider')); ?>
    </div>

    <div class="mw_ycsd_mid">
        <div class="inner_wraper">
        <?php
			//$events = EM_Events::get(array('scope'=>'all', 'category'=> 'idrac', 'orderby'=>'event_start_date', 'limit'=>3));
			if(!empty($events)) {
		?>	
        	<h2>Events | <a class="viewall" href="/idrac/events/">View all</a></h2>
        	<div class="mwUpcomingEvents">
            	<?php
					foreach( $events as $key=>$EM_Event  ){ ?>
					<?php 
						$today = date("Ymd");
						$eventDate = $EM_Event->output("#_{Ymd}");
						if($today < $eventDate) {
							$eventClass = "futureEvent";
						} elseif($today > $eventDate) {
							$eventClass = "pastEvent";
						} elseif($today == $eventDate) {
							$eventClass = "todayEvent";
						}
					?>
                        <article class="ycsdBox <?php echo $eventClass; ?>">
                        	<a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>"><img class="idrac_article_img" src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" width="230" height="97" class="top"></a>
                            <div class="ycsdInner">
                            	<?php /*?><h5><i class="fa fa-calendar"></i> <?php echo "Date : ".$EM_Event->output("#_{d-M-Y}"); ?></h5><?php */?>
                                <?php /*?><h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo string_limit_words($EM_Event->output("#_EVENTNAME"), 10); ?></a></h4><?php */?>
                                <h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo $EM_Event->output("#_EVENTNAME"); ?></a></h4>
                                <p class="ycsdText"><?php echo string_limit_words($EM_Event->output("#_EVENTEXCERPT{10}"), 20).'...'; ?></p>
                                <a class="ycsdReadMore" href="<?php echo $EM_Event->output("#_EVENTURL"); ?>">Read More...</a>
                                
                               <!-- <button class="ycsdViewAlbum">View Album</button> -->
                            </div>
                            <div class="ycsdBtn">
                       <?php 
                                   if($EM_Event->output('#_ATT{Album}')) {                                         
                                       ?>
                                      <link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/css/lity.min.css" />
                                      <!-- <a class="ycsdViewAlbum btn-slider"  href="#"><i class="fa fa-image"></i>View Album</a>-->
                                       
                                           <?php 
                                               $shortcode = ''.$EM_Event->output('#_ATT{Album}').''; 
                                               echo do_shortcode( $shortcode );
                                           ?>
                                       
                                       <?php
                                   } 
                               ?>
                            	<?php  if($EM_Event->output('#_ATT{Video url}') && $EM_Event->output('#_ATT{Video url}') != '#') { ?><a class="watchVideo"  href="<?php echo $EM_Event->output('#_ATT{Video url}'); ?>" rel="wp-video-lightbox"><i class="fa fa-play-circle-o"></i> Watch Video</a><?php } ?>
                                <?php if($EM_Event->output('#_ATT{Poster}')) { ?><a data-lity class="pressRelease" href="<?php echo $EM_Event->output('#_ATT{Poster}'); ?>" target="_blank">Poster</a><?php } ?>
                               
                            </div>
                        </article>
				<?php
					}
				?>  
                        <div class="ycsdSideBar">
        	<?php /*?><aside id="yscdCalender">
            <?php
				$events = EM_Events::get(array('scope'=>'month', 'category'=> 'ycsd', 'limit'=>3, ));
				# if(!empty($events)) {
			?>	
                <h2>Calendar</h2>
                <div class="mwCalendar">
                    <h3><?php echo date('F'); ?></h3>
                    <?php
					if($events) {
                        foreach( $events as $EM_Event ){ ?>
                            <article class="ycsdRow">
                                <div class="mwDate"><?php echo $EM_Event->output("#_{j}"); ?><sup><?php echo $EM_Event->output("#_{S}"); ?></sup></div>
                                <h4 class="ycsdTitle"><?php echo $EM_Event->output("#_EVENTLINK"); ?></h4>
                                <?php 
									$eventID = $EM_Event->output("#_EVENTPOSTID");
									echo get_the_term_list( $eventID, 'speakers', '<h4 class="ycsdTitle speaker"><strong>Speakers:</strong> ', ', ', '</h4>' ); 
								?>
                            </article>
                    <?php
                        }
					} else {
						echo '<p style="color: #fff; padding: 15px;">Sorry No Event Found</p>';
					}
                    ?>
                </div>
            </aside><?php */?>
            <aside id="workingPaperSeries">
         <!--   <a href="https://vimeo.com/album/3152429" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video_lecture_videos.jpg" /></a>
            	
<a href="http://10.100.0.157:8080/jspui/handle/123456789/2"  target="_blank"><h2 class="icn_wps">Working Paper Series</h2><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/wps_icon.jpg" />
                </a>
                <p style="padding: 0px 12px 12px 12px; margin: 0; text-align: justify;">Working papers are designed to communicate ongoing research and to share key findings with a broader community. The Working Paper Series constitutes "work in progress." The series aims at assessing theoretical issues and offering critical insights to issues connected to development, social change and specific policies.</p>
           
--> </aside>
            <?php /*?><aside id="fromAhssBlog">
            	<h3>From AHSS Blog</h3>
                <div class="ahssBlogg">
                	<?php echo do_shortcode('[ahss]'); ?>            
                </div>
            </aside><?php */?>
            <aside id="yscdLectureVideos">
				<a href="https://vimeo.com/album/3152429" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video_lecture_videos.jpg" /></a>
            </aside>
        </div>
            </div>
            <?php } ?>
            <!--<div class="mwPastEvents">
            	<?php
					$events = EM_Events::get(array('scope'=>'past', 'category'=>'idrac', 'limit'=>3));
					if(!empty($events)) {
				?>	
                <h2>Past Events | <a class="viewall" href="/events/categories/ycsd/">View all</a></h2>
                <?php
					}
					foreach( $events as $EM_Event ){ ?>
                    	<?php #var_dump($EM_Event->output("#_EVENTURL")) ?>
						<article class="ycsdBox">
                        	<a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>"><img src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" width="230" height="97" class="top"></a>
                            <div class="ycsdInner">
                                <h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo $EM_Event->output("#_EVENTNAME"); ?></a></h4>
                                <p class="ycsdText"><?php echo string_limit_words($EM_Event->output("#_EVENTEXCERPT{10}"), 20).'...'; ?></p>
                                <a class="ycsdReadMore" href="<?php echo $EM_Event->output("#_EVENTURL"); ?>">Read More...</a>
                            </div>
                            <div class="ycsdBtn">
                            	<?php if($EM_Event->output('#_ATT{Video url}')) { ?><a class="watchVideo" href="<?php echo $EM_Event->output('#_ATT{Video url}'); ?>" rel="wp-video-lightbox"><i class="fa fa-play-circle-o"></i> Watch Video</a><?php } ?>
                                <?php /*?><?php if($EM_Event->output('#_ATT{Press Release}')) { ?><a class="pressRelease" href="<?php echo $EM_Event->output('#_ATT{Press Release}'); ?>" target="_blank"><i class="fa fa-file-text-o"></i> Press Release</a><?php } ?><?php */?>
                            </div>
                        </article>
				<?php
					}
				?>
            </div>-->
            <?php 
				$restream = array(
								'post_type' => 'research-streams',
								'posts_per_page' => 3,
								'meta_key' => '',
   								'meta_value' => ''
							);
				query_posts($restream);	
			?>				        				
            <div class="mwResearchStreams">
                <h2>Research Themes | <a class="rsviewall" href="research-themes/">View all</a></h2>
                <div class="mwResearchStreams">
                    <?php 
                        while ( have_posts() ) : the_post(); ?>
                        <article class="ycsdBox">
                        <?php if( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )) { ?>
                            <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"  width="230" height="97" class="top" width="230" height="97" /></a>
                        <?php } ?>    
                        	<?php 
								$icon = get_field('custom_icon');
							?>
							<a href="<?php the_permalink(); ?>"><img src="<?php echo $icon['url']; ?>" alt="<?php echo get_the_title(); ?>" /></a>
                            <div class="ycsdInner">
                            	
                                <h4 class="ycsdTitle"><a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php the_title(); ?></a></h4>
                                <p class="ycsdText"><?php echo string_limit_words(get_the_excerpt(), 20).'...'; ?></p>
                                <a class="ycsdReadMore" href="<?php the_permalink(); ?>">Read More...</a>
                            </div>
                        </article>
               		<?php
						endwhile; 
                        wp_reset_query();
                	?>
                </div>
            </div>
            
			<?php  if ( have_posts() ) :
             while (have_posts()) : the_post();
                // the_content();
             endwhile; endif; ?>
         
        </div>
        

        
    </div>
</div>
	

<?php get_footer(); ?>
