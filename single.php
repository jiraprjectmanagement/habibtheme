<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

// get_header(); ?>

<?php get_header('covid-home'); ?>


		<div id="container">
			<div id="content" role="main">

			<?php
			/*
			 * Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>

			</div><!-- #content -->
			<div id="primary" class="widget-area" role="complementary">
				<ul class="tb_side huit_wid career_wid">
				<?php dynamic_sidebar( 'left_sidebar' ); ?>
				</ul>
			</div>
		</div><!-- #container -->

<?php //get_footer(); ?> 

<?php get_footer('hpv1footercovid'); ?>
