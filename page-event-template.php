<?php
/**
 * Template Name: Event Page Template
 */

// get_header(); ?>
<?php get_header('with-megamenu-live'); ?>

		<div id="container">
			<div id="content" role="main">
			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

			</div><!-- #content -->
            <div id="primary" class="widget-area" role="complementary">
				<?php /*?><h3 class="widget-title side"> <a href="<?php echo $titlelink; ?>"><?php echo $titlenamer; ?></a> </h3><?php */?>
                  <ul class="tb_side event_wid">
                    <?php dynamic_sidebar( 'event_sidebar' ); ?>
                  </ul>
          	</div>
		</div><!-- #container -->


<?php //get_footer(); ?>

<?php get_footer('footer-live'); ?>

