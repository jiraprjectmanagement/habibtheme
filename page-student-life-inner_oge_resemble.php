<?php
/**
 * Template Name: Student Life Inner_OGE_Resemble
 *
 */
get_header('oge'); ?>


<div id="container" class="phyLab">
			
			<div id="content" class="innerLayout">
				
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					if(!empty($url)) {
						$myurl = 'data-parallax="scroll" data-image-src="'.$url.'"';
					} else {
						$myurl = '';
					}
				?>
				
				<?php if( have_rows('physics_slider') ): ?>
				<div class="topImg" style="margin-bottom: 25px;">
					<div id="bxslider">
						<ul class="bxslider">
					  		<?php while( have_rows('physics_slider') ): the_row(); 

								// vars
								$image = get_sub_field('image');
								$text = get_sub_field('text');

								?>
								<li>
									<img src="<?php echo $image; ?>" />
									<?php if(!empty($text)) { ?>
										<div class="mbox"><h1><?php echo $text; ?></h1></div>
									<?php } ?>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>
				<?php else: ?>
				
				<div class="topImg parallax-window" <?php echo $myurl; ?>>
					<?php if($url) { ?>
					<img src="<?php echo $url; ?>" />
					<?php } ?>
					<div class="mbox">
						<h1><?php the_title(); ?></h1>
						<div class="subTitle"><?php the_field('sub_title'); ?></div>
					</div>
				</div>
				<?php endif; ?>
			<?php	if ( have_posts() ) :
						while (have_posts()) : the_post();
			?>
            				<!--<h1 class="title"><?php the_title(); ?></h1>-->
            				<div class="content">
								<?php the_content(); ?>
            				</div>
            <?php				
						endwhile;
					endif; 
			?>
				<div class="phySidebar">
					<?php dynamic_sidebar( 'student-life' ); ?>
				</div>

			</div><!-- #content.fullwidth -->
		</div><!-- #container -->
<?php get_footer(); ?>