<?php
/**
 * Template for displaying the footer
 *
 */
?>

     
</div>
<!-- #main -->
</div>
<!--.inner_wrap-->
<div id="footer" role="contentinfo">
  <div class="inner_footer">
    <div class="main_footer">
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'First Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Second Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Third Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Fourth Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Fifth Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Sixth Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
    </div>
    <?php
	/*
	 * A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	//get_sidebar( 'footer' );
?>
  </div>
  
  <div id="site-info">
    <div class="inner_wraper">
      <div class="footer_social"> <span>Stay Connected</span>
        <ul>

          <a href="https://www.linkedin.com/company/habib-university" title="LinkedIn" target="_blank"><li class="fa fa-linkedin"></li></a>
          
          <a href="https://twitter.com/habibuniversity" title="Twitter" target="_blank"><li class="fa fa-twitter"></li></a>
                    
          <a href="https://www.facebook.com/HabibUniversity" title="Facebook" target="_blank"><li class="fa fa-facebook"></li></a>
          
          <a href="https://instagram.com/habibuniversity" title="Instagram" target="_blank"><li class="fa fa-instagram"></li></a>
          
          <a href="https://www.youtube.com/user/HabibUni" title="YouTube" target="_blank"><li class="fa fa-youtube"></li></a>
          
        </ul>
      </div>
       <div class="footer_right">
        <p><a href="/contact-us/" style="color:#fff !important;">Contact Us </a>| Phone: +92 21 1110 42242 (HABIB)</br>
          	&copy; Habib University - All Rights Reserved | <a href="/privacy-statement/" target="_blank" style="color:#fff !important;">Privacy Statement</a></p>
      </div>
    </div>
    <!-- #site-info --> 
    
  </div>
</div>
<!-- #footer -->
</div>
<!-- #wrapper -->
<?php
	/*
	 * Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>

</body></html>
<?php if(is_front_page() || is_page_template('home-page.php')) { ?>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.bxslider.min.js"></script>
<script>



	jQuery('.bxslidertwo').bxSlider({
	  auto: true
	});
	
	jQuery(document).ready(function($){
    //az-letters
    $("az-letters").prepend("<b>Filter by :</b>. ");
	});
</script>
<?php } ?>
<?php if ( is_page_template('page-physics-lab-home.php') ||  is_page_template('page-student-life-main.php') ) { ?>
<?php if( have_rows('physics_slider') ): ?>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.bxslider.min.js"></script>
<script>
	
	jQuery(document).ready(function(){
	  jQuery('.bxslider').bxSlider({
		  auto: true
	  });
	});
</script>
<?php endif; ?>
<?php } ?>
<?php /*?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/filterizr/jquery.filterizr.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/controls.js"></script>

<!-- Kick off Filterizr -->
<script type="text/javascript">
	jQuery(function() {
		//Initialize filterizr with default options
		jQuery('.filtr-container').filterizr({
			filter: 1,
		});
	});
</script>
<?php */?>



