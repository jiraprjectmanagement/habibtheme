<?php
/*
 Template Name: career-staff
*/

?>
<?php get_header('with-megamenu-live'); ?>

<style>
	.jobs_buttons {
    display: block!important;
}
.addtoany_shortcode{
    display: block!important;
}
.a2a_kit.a2a_kit_size_32.addtoany_list{
    display: block!important;
}
	</style>

		<div id="container">
			<div id="content" role="main">

			<div class="arconix-tabs-horizontal">
				<ul class="arconix-tabs">
					<li data-arconix-icon=" " data-arconix-color=" " class="arconix-tab tab-title1">
						<a id="tab-title1" class="" href="#">Administrative Job</a></li>
					<li data-arconix-icon=" " data-arconix-color=" " class="arconix-tab tab-title2">
						<a id="tab-title2" class="inactive" href="#">Academic Support</a>
					</li>
					<li data-arconix-icon=" " data-arconix-color=" " class="arconix-tab tab-title3">
						<a id="tab-title3" class="inactive" href="#">Academic Support</a>
					</li>
				</ul>
				<div class="arconix-panes">
					<div id="pane-tab-title1" class="arconix-pane pane-title1" style="display: block;">	
						<?php 

$args = array(
	'post_type'   => 'staff-jobs',
	'post_status' => 'publish',
	'tax_query'   => array(
		array(
			'taxonomy' => 'staff-jobs-categories',
			'field'    => 'slug',
			'terms'    => 'administrative'
		)
	)
   );
  $testimonials = new WP_Query( $args );
  if( $testimonials->have_posts() ) :
  
		while( $testimonials->have_posts() ) :
		  $testimonials->the_post();
		  
		  echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" alt="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</a></h3>';
		  the_excerpt(); 
		endwhile;
		wp_reset_postdata();
  
  else : 
   endif;


						?>
					</div>
					<div id="pane-tab-title2" class="arconix-pane pane-title2" style="display: none;">

						<!-- <div class="arconix-tabs-vertical" style="display: block!important;">
							<ul class="v-arconix-tabs">
								<li data-arconix-icon=" " data-arconix-color=" " class="v-arconix-tab tab-title1">
									<a id="tab-title1" class="" href="#">Administrative Job</a></li>
								<li data-arconix-icon=" " data-arconix-color=" " class="v-arconix-tab tab-title2">
									<a id="tab-title2" class="inactive" href="#">Academic Support</a>
								</li>
							</ul>
							<div class="v-arconix-panes" style="display: block!important;">
								<div id="v-pane-tab-title1" class="v-arconix-pane pane-title1" style="display: block;"> one	</div>
								<div id="v-pane-tab-title2" class="v-arconix-pane pane-title2" style="display: block;">	two</div>
							</div>
						</div> -->

<?php 
$args = array(
  'post_type'   => 'staff-jobs',
  'post_status' => 'publish',
  'tax_query'   => array(
  	array(
  		'taxonomy' => 'staff-jobs-categories',
  		'field'    => 'slug',
  		'terms'    => 'academic-support'
  	)
  )
 );
$testimonials = new WP_Query( $args );
if( $testimonials->have_posts() ) :
      while( $testimonials->have_posts() ) :
        $testimonials->the_post();
		echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" alt="' . esc_attr( get_the_title() ) . '">' . get_the_title() . '</a></h3>';
		the_excerpt(); 
      endwhile;
      wp_reset_postdata();
else : 
 endif;
?>
					</div>
					<div id="pane-tab-title3" class="arconix-pane pane-title3" style="display: none;">
						<ul class="arconix-pane-ul" >
							<li><a data-lity href="https://habib.edu.pk/wp-content/uploads/2021/10/Job-Description-Coordinator-for-Office-of-Associate-Dean.pdf" target="_blank" rel="noopener">Call for Applications - Coordinator, Associate Dean Teaching and Learning</a></li>
							<li><a data-lity href="https://habib.edu.pk/wp-content/uploads/2021/09/JD-Assistant-Manager-Research_OOR.pdf" target="_blank" rel="noopener">Call for Applications - Assistant Manager, Research</a></li>
							<li><a data-lity href="https://habib.edu.pk/wp-content/uploads/2021/03/20210301_JD_RA_ComputerScience.pdf" target="_blank" rel="noopener">Call for Applications - Research Assistant (Computer Science Program)</a></li>
							<li><a data-lity href="/wp-content/uploads/2021/05/Call-for-Applications-Coordinator-Honors-Program.pdf" target="_blank" rel="noopener">Call for Applications - Coordinator, Honors Program</a></li>
							<li><a data-lity href="/wp-content/uploads/2021/10/Call-for-Applications-Senior-Manager-HU-ESP-ver-2.pdf" target="_blank" rel="noopener">Call for Applications - Senior Manager, Habib University Exceptional Scholars’ Program (HU ESP) </a></li>
							<li><a data-lity href="https://habib.edu.pk/wp-content/uploads/2021/08/JD-Assistant-Manager-Academic-Recruitment-003.pdf" target="_blank" rel="noopener">Call for Applications - Assistant Manager, Academic Recruitment</a></li>
						</ul>
					</div>				

				</div>
			</div>


			<script>jQuery("ul.arconix-tabs li a:not(:first)").addClass("inactive");
			jQuery(".arconix-panes .arconix-pane").hide();
			jQuery(".arconix-panes .arconix-pane:first").show();
			jQuery("ul.arconix-tabs li a").click(function(){
				//var t = jQuery(this).attr("href");
				var t = jQuery(this).attr("id");
				jQuery("ul.arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".arconix-panes .arconix-pane").hide();
				//jQuery(t).fadeIn("slow");
				jQuery("#pane-"+ t ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition 
				jQuery("ul.arconix-tabs li a").addClass("inactive");         
				jQuery(this).removeClass("inactive");
				jQuery(".arconix-panes .arconix-pane").hide();
				jQuery(t).fadeIn("slow");    
			}</script>

<script>jQuery("ul.v-arconix-tabs li a:not(:first)").addClass("inactive");
			jQuery(".v-arconix-panes div").hide();
			jQuery(".v-arconix-panes div:first").show();
			jQuery("ul.v-arconix-tabs li a").click(function(){
				//var t = jQuery(this).attr("href");
				var t = jQuery(this).attr("id");
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");        
				jQuery(this).removeClass('inactive');
				jQuery(".v-arconix-panes div").hide();
				//jQuery(t).fadeIn("slow");
				jQuery("#v-pane-"+ t ).fadeIn("slow")
				return false;
			});
			if(jQuery(this).hasClass("inactive")){ //this is the start of our condition 
				jQuery("ul.v-arconix-tabs li a").addClass("inactive");         
				jQuery(this).removeClass("inactive");
				jQuery(".v-arconix-panes div").hide();
				jQuery(t).fadeIn("slow");    
			}</script>

			<?php
			/*
			 * Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */



	
			?>

			</div><!-- #content -->

			<div id="primary" class="widget-area career-staff" role="complementary">
				<?php get_sidebar('career'); ?>
		  <!-- <h3 class="widget-title side"> <a href="/about-us/vision-values/">About</a> </h3> -->
		  <!-- <?php wp_nav_menu( array('container_class' => 'libraryInner', 'menu_class' => 'tb_side', 'theme_location' => 'about_sidebar' ) ); ?> -->
		</div>
			
		</div><!-- #container -->


<?php get_footer('footer-live'); ?>

