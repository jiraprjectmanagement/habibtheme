<?php
/*
 Template Name: liberal core main
*/

//get_header('liberal-core'); ?>

<?php get_header('with-megamenu-live'); ?>


<div class="container">


    <!-- <div class="wc_content_lc"> -->
        
        <!-- <div class="row mb-3" style="color: #fff;">
            .
            </div> -->

        <!-- <div class="row text-center mt-1 mb-2">
            <h1 class="heading-h1">The Habib University Liberal Core</h1>
        </div> -->
        
    
        <div class=""> 

    
        <!-- <div id="primary_lc_sidebar" class="widget-area desktoplc" role="complementary"> -->
        <div id="primary" class="widget-area" role="complementary">

        <h3 class="widget-title side"> <a href="https://habib.edu.pk/academics/">Academics</a> </h3>

            <?php  dynamic_sidebar( 'lc_sidebar' ); ?>
        </div> 
            
        <!-- <div class="col-sm-9"> --> <div id="content" role="main">
            <h1 class="entry-title">Habib Core</h1>

        <h2 class="heading-h1">The Habib University Liberal Core</h2>
            <!-- <p style="color: #5c2568;">The strength of Liberal Arts model of education lies in its capacity to produce graduates who possess sharp critical minds, a penchant for creative and unconventional thinking, and essential leadership skills. Developing the full capacities of a truly well-educated person relies both on a depth of study in the students chosen major combined with a real breadth of knowledge gained through a meaningful and sufficiently broad engagement with a range of existing forms of knowledge. An engineer who also knows literature is a better engineer, and a poet who also knows physics is inherently a better poet. Habib University is committed to the Liberal Arts and Sciences model of education because of its demonstrated success in producing graduates who have both the adaptability and confidence to succeed in a world of increasingly rapid change and volatility. Liberal Arts and Sciences graduates arent just surviving in the complex world of the twenty-first century, theyre seizing the initiative to actively shape it! At the heart of a Habib education lies our distinctive Liberal Core curriculum, which embodies our ideal of Yohsin: "The worth of each person lies in their own ful self-cultivation." As our students mature, their experience in the Habib Liberal Core empowers them to both reflect on and change the world they inherit.</p> -->
            <p style="color: #5c2568; line-height: 2;"> <br> The strength of Habib's Liberal Core is built on its seven Forms of Thought and Action. These seven Forms of Thought, which were inspired by Stanford University's Breadth Governance model, but adapted to our own unique regional context, include: historical and social thought, philosophical thought, rhetoric and expression, formal reasoning, quantitative reasoning, scientific method and analysis and creative practice. All students are required to take a determined minimum number of courses under each form of thought and action. </p>
            <br><br>
            <h5 style="color: #5c2568;"> To find out more, please click on the circles </h5>
        </div>
    

        </div>

        
        
    <!-- </div> -->
</div>

<div class="wc_section_liberal" id="order2">
    <!-- <div class="bgimg_liberalcore"> <h5 style="color: #5c2568; text-align:center; padding-top:18px;"> To find out more, please click on the circles </h5> </div> -->
    <div class='selectorlc'>
    <ul>
        <li>
        <input id='c1' type='checkbox'>
        <label for='c1'><a href="#historical-and-social-thoughts" data-lity><span><img src="/docs/app-images/creative-practice.png"/></span><br />Historical & Social Thought <br />(2 courses)</label></a>
        </li>
        <li>
        <input id='c2' type='checkbox'>
        <label for='c2'><a href="#philosophical-thoughts" data-lity><span><span><img src="/docs/app-images/creative-practice.png"/></span><br />Philosophical Thought <br />(2 courses)</label></a>
        </li>
        <li>
        <input id='c3' type='checkbox'>
        <label for='c3'><a href="#language-and-expression" data-lity><span><img src="/docs/app-images/Philosophical-thought.png"/></span><br />Language & Expression <br />(2 courses)</label></a>
        </li>
        <li>
        <input id='c4' type='checkbox'>
        <label for='c4'><a href="#formal-reasoning" data-lity><span><span><img src="/docs/app-images/language-expression.png"/></span><br />Formal <br />Reasoning <br />(1 course)</label></a>
        </li>
        <li>
        <input id='c5' type='checkbox'>
        <label for='c5'><a href="#quantitative-reasoning" data-lity><span><span><img src="/docs/app-images/Quantitive-reasoning.png"/></span><br />Quantitative Reasoning <br />(1 course)</label></a>
        </li>
        <li>
        <input id='c6' type='checkbox'>
        <label for='c6'><a href="#natural-scientific-method-and-analysis" data-lity><span><span><img src="/docs/app-images/Natural-Scientific.png"/></span><br />Natural Scientific Method & Analysis <br /> (1 course)</label></a>
        </li>
        <li>
        <input id='c7' type='checkbox'>
        <label for='c7'><a href="#creative-practice" data-lity><span><span><img src="/docs/app-images/Formaal-reasoning.png"/></span><br />Creative <br /> Practice <br />(1 course)</label></a>
        </li>
    </ul>
    <button disabled="disabled">Seven Forms of Thought / Action</button>
    </div>


    
    
    
</div>

<div class="container mblview">  
    <hr style="    border-top: 1px solid rgb(255 255 255);"/>
        <h1 style="text-align: center; text-transform: uppercase;	font-weight: bold;	color: #5c2568;	margin-bottom: 20px;">Seven Forms of <br /> Thought / Action</h1>         
        <div class="selector_mbl_lc">
            <div class="row">
                <div class="col-sm-6 lft-circle">
                    <a href="#historical-and-social-thoughts" data-lity>
                    <div class="selector_circle">
                        <span>
                            <img src="/docs/app-images/creative-practice.png"/>
                        </span>
                        <p>
                            Historical & Social Thought <br />(2 courses)
                        </p>
                    </div>
                    </a>
                </div>
                <div class="col-sm-6 rgt-circle">
                    <a href="#philosophical-thoughts" data-lity>
                    <div class="selector_circle">
                        <span>
                            <img src="/docs/app-images/Philosophical-thought.png"/>
                        </span>
                        <p>
                            Philosophical Thought<br />(2 courses)
                        </p>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 lft-circle">
                <a href="#language-and-expression" data-lity>
                    <div class="selector_circle">
                        <span>
                            <img src="/docs/app-images/language-expression.png"/>
                        </span>
                        <p>
                            Language & Expression<br />(2 courses)
                        </p>
                    </div>
                    </a>
                </div>
                <div class="col-sm-6 rgt-circle">
                    <a href="#formal-reasoning" data-lity>
                    <div class="selector_circle">
                        <span>
                            <img src="/docs/app-images/Formaal-reasoning.png"/>
                        </span>
                        <p>
                            Formal <br /> Reasoning<br />(1 course)
                        </p>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6 lft-circle">
                    <a href="#quantitative-reasoning" data-lity>
                        <div class="selector_circle">
                            <span>
                                <img src="/docs/app-images/Quantitive-reasoning.png"/>
                            </span>
                            <p>
                                Quantitative Reasoning<br />(1 course)
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 rgt-circle">
                    <a href="#natural-scientific-method-and-analysis" data-lity>
                    <div class="selector_circle">
                        <span>
                            <img src="/docs/app-images/Natural-Scientific.png"/>
                        </span>
                        <p>
                            Natural Scientific Method & Analysis <br /> (1 course)
                        </p>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="container" style="    margin: 0 auto;    width: 50%;">
                <div class="row">
                    <a href="#creative-practice" data-lity>
                        <div class="col-sm-12 rgt-circle">
                        <div class="selector_circle">
                            <span>
                                <img src="/docs/app-images/creative-practice.png"/>
                            </span>
                            <p>
                                Creative Practice<br />(1 course)
                            </p>
                        </div>
                        </div>
                    </a>
                </div>
            
            </div>
            
            <!-- <div class="jumbotron-lc">
                <a href="/academics/habib-core/course-descriptions/" target="_self"  class="btn_liberalcore">View Course Descriptions</a>
            </div> -->
        </div>
</div>



<div class="jumbotron-lc">
    <a href="/academics/habib-core/course-descriptions/" target="_self"  class="btn_liberalcore">View Course Descriptions</a>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script><script>var nbOptions = 8;
var angleStart = -360;

// jquery rotate animation
function rotate(li,d) {
    $({d:angleStart}).animate({d:d}, {
        step: function(now) {
            $(li)
               .css({ transform: 'rotate('+now+'deg)' })
               .find('label')
                  .css({ transform: 'rotate('+(-now)+'deg)' });
        }, duration: 0
    });
}

// show / hide the options
function toggleOptions(s) {
    $(s).toggleClass('open');
    var li = $(s).find('li');
    var deg = $(s).hasClass('half') ? 180/(li.length-1) : 360/li.length;
    for(var i=0; i<li.length; i++) {
        var d = $(s).hasClass('half') ? (i*deg)-90 : i*deg;
        $(s).hasClass('open') ? rotate(li[i],d) : rotate(li[i],angleStart);
    }
}

$('.selectorlc button').click(function(e) {
    toggleOptions($(this).parent());
});

setTimeout(function() { toggleOptions('.selectorlc'); }, 100);//@ sourceURL=pen.js
</script>


            <div id="philosophical-thoughts" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/Philosophical-thought-purple.png" alt="" height="40px" width="auto"/>Philosophical Thought<br /> (2 courses)</h1>
                <hr />
				<p>The study of philosophy has traditionally been at the heart of all Liberal Core curricula. Philosophical thought serves to enhance the reflective powers of the student, which is essential to concept-generation and innovation in all fields. Furthermore, an understanding of the philosophical depth of a tradition is crucial to a shared sense of inheritance. Habib faculty also widely share an interest in philosophy/theory. All students will be required to take a minimum of two courses in Philosophical Thought.</p>
            </div>
            
            <div id="historical-and-social-thoughts" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/Layer2-purple.png" alt="" height="40px" width="auto"/>Historical & Social Thought <br />(2 courses)</h1>
                <hr />
				<p>The extraordinary significance of historical and social knowledge in modern times arises from the unprecedented pace of change in modernity, as well as the growing complexity of modern societies. Across the disciplines, Habib University's faculty also demonstrate a remarkably coherent historical approach to both social scientific and humanistic knowledge. All students will be required to take a minimum of two courses in Historical & Social Thought.</p>
            </div>
            
            <div id="language-and-expression" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/language-expression-purple.png" alt="" height="40px" width="auto"/>Language & Expression <br />(2 courses)</h1>
                <hr />
				<p>The development of linguistic and expressive abilities is widely recognised to be a key benefit of a Liberal Arts education, and language and literature have traditionally been as central to liberal core curricula as philosophy. Communicative power is one key to achieving success in all fields and disciplines. All students will be required to take a minimum of two courses under this rubric.</p>
            </div>
            
            <div id="formal-reasoning" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/Formaal-reasoning-purple.png" alt="" height="40px" width="auto"/>Formal Reasoning <br />(1 course)</h1>
                <hr />
				<p>Deductive thinking is crucial across fields and disciplines in both Science and Engineering, as well as in Arts, Humanities, and Social Sciences, and a deductive reasoning requirement is standard in higher and liberal education. Such a requirement also reflects the strength of our Science and Engineering faculty at Habib University. All students will be required to take a minimum of one course in Formal Reasoning.</p>
            </div>
            
            <div id="quantitative-reasoning" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/Quantitive-reasoning-purple.png" alt="" height="40px" width="auto"/>Quantitative Reasoning <br />(1 course)</h1>
                <hr />
				<p>Numbers and quantities are an essential part of modern civilization and its forms of knowledge. Quantitative reasoning is the ability to interpret and contextualize large amounts of data, and it is an essential skill in virtually all professions. All students will be required to take a minimum of one course in Quantitative Reasoning.</p>
            </div>
            
            <div id="natural-scientific-method-and-analysis" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/Natural-Scientific-purple.png" alt="" height="40px" width="auto"/>Natural Scientific Method & Analysis <br />(1 course)</h1>
                <hr />
				<p>The development of scientific method and analysis is a fundamental feature of modernity and its forms of knowledge. A natural science requirement is standard in higher educational and liberal institutions. To ensure the scientific literacy of all our graduates, students will be required to take a minimum of one course in Natural Scientific Method & Analysis.</p>
            </div>
            
            <div id="creative-practice" class="cst-pop lity-hide" style="height: 347px; text-align: center; max-height: 706px;">
				<h1 style="text-align: center;"><img style=" vertical-align:middle;" src="/docs/app-images/creative-practice-purple.png" alt="" height="40px" width="auto"/>Creative Practice <br />(1 course)</h1>
                <hr />
				<p>Creativity is increasingly recognised as an important indicator of success, and it is often a required feature of the best higher educational curricula. Given the nature of our programs and faculty in both AHSS and SSE, we have an excellent opportunity to make creative practice a distinctive feature of the Habib experience. All students will be required to take at least one course under this rubric.</p>
            </div>
            
          

<?php // get_footer('liberal-core'); ?>

<?php get_footer('footer-live'); ?>