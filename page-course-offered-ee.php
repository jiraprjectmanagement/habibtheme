<?php
/*
Template Name: Course Offered EE
*/

get_header(); ?>

		<div id="container" class="courseCatlog">
			<div id="content" role="main">
            
            	<h1 class="entry-title">
					<?php the_title(); ?>
                </h1>
                <p style="margin-bottom:15px;">Filter this list of Courses by selecting the <strong>Drop-Down</strong> on any of the <strong>Field(s)</strong> given below and using the <strong>Search Button</strong>.</p>
                <p style="margin-bottom:15px;">Alternatively, you can enter the <strong>Course Code</strong> and/or <strong>Keyword/Phrase</strong> either independently or in combination with the <strong>Drop-Down Field(s)</strong>.</p>
                <p style="margin-bottom:15px;">Use the <strong>Refresh Button</strong> for each new <strong>Search</strong>.</p>
                <p><strong>Disclaimer:</strong> The catalogue is not to be considered as a binding contract between Habib University and students, parents, or guardians of students, nor other interested parties. Habib University reserves the right at any time, without advance notice, to change any part, portion or provision of the catalogue; no vested rights shall run or be created by the catalogue, including the right to notice of any modification, novation, amendment, supplementation, or other change of any term, provision or content of the catalogue; such right of the University to enact changes, etc., especially shall include but not be limited to... <a rel="wp-video-lightbox" href="#disclaim">Read more</a></p>
                <div style="display:none; height:320px; width:300px;" id="disclaim">
                	<div class="disclaim_lightbox">
                    	<ul>
                        	<li>Withdrawal or cancellation of classes, courses and programs;</li>
                            <li>Changes in fee schedules;</li>
                            <li>Changes in the academic calendar;</li>
                            <li>Changes in admission and registration requirements;</li>
                            <li>Changes in the regulations and requirements governing instruction in and graduation from the University;</li>
                            <li>Changes of instructors;</li>
                            <li>Changes of rules and regulations governing the students and student body organizations;</li>
                            <li>Changes of on-campus facilities, programs and costs for room and/or board of students;</li>
                            <li>Changes of extra-curricular student activities, programs and offerings; and</li>
                            <li>Changes of any other regulation affecting students, their parents/guardians or other interested parties. The official version of the Habib University Course</li>
                        </ul>
                    </div>
               	</div>
                <div class="entry-content">
                	<?php // the_content(); ?>
                </div>
<?php
	$prefix = $_POST['prefix'];
	$codeName = $_POST['codeNumber'];
	$courseType = $_POST['courseType'];
	$cdelective = $_POST['cdelective'];
	$courseSchool = 430;

	$keywords = $_POST['keywords'];	
?>                
                <div class="searchFilter">
                	<h2><?php echo get_field('course_filter_title', 'option'); ?></h2>
                    <div class="searchForm">
                      <form action="https://habib.edu.pk/office-of-registrar/course-description/" target="_blank" method="post" name="coursefilter">
                      <?php //$fields = get_field_objects('subject_area');
							// var_dump( $fields );  ?>
                    	<?php echo get_field('course_filter_description', 'option'); ?>

<?php 

// $group_ID = 10993;
/*
$group_ID = 10993;
$fields = array();
$fields = acf_get_field_groups($group_ID);

if( $fields )
{
echo '<div class="col-sm-6">';
// echo '<h3>Rates:</h3>'; //------I want to remove this line if field is empty, but it keeps appearing
    echo '<ul class="list-unstyled">';
  foreach( $fields as $field_name => $field )
  {
    $value = get_field( $field['name'] );
    if ($field['choices']){
      $map = array(
       'yes' => '<i class="icon-ok"></i>'       
       );
      $value = $map[ $value ];
    } else {
    }
    if( $value && $value != 'no') {
      echo '<li>' . $field['label'] . '&nbsp;' . $value . '</li>';
  }
}
  echo '</ul>';
echo '</div>';
}
*/
?>
<?php
	/*
	$fields = array();
    $groups =  acf_get_field_groups(array('post_type' => 'course_catlog'));
    foreach($groups as $group){
		$fields[$group['title']] = $group;
        $f = acf_get_fields($group['ID']);
        foreach($f as $i){
			print_r($fields[$group['title']]);
//			echo "<b>first</b><pre>";
			$fields[$group['title']]['fields'][$i['name']] = $i;
			echo "<b>second</b> <pre>";
			print_r($fields[$group['title']]['fields'][$i['name']]);
			echo "</pre>";
            $fields[$group['title']]['fields'][$i['name']]['value'] = get_field($i['key']);
			echo "</pre> <b>third</b> <pre>";
			print_r($fields[$group['title']]['fields'][$i['name']]['value']);
			echo "</pre>";
       	}    
	}
	
	*/
	
	
	$fields = array();
    $groups =  acf_get_field_groups(array('post_type' => 'course_catlog'));
    foreach($groups as $group){
		$fields[$group['title']] = $group;
        $f = acf_get_fields($group['ID']);
        foreach($f as $i){
			$fields[$group['title']]['fields'][$i['name']] = $i;
			$fields[$group['title']]['fields'][$i['name']]['value'] = get_field($i['key']);
       	}    
	}
	$mwsubjects = $fields['Catalog Filter']['fields']['subject_area']['choices'];
	$mwscoursetypes = $fields['Catalog Filter']['fields']['course_type']['choices'];
	$mwcdelective = $fields['Catalog Filter']['fields']['electives']['choices'];
	$mwcourseschools = $fields['Catalog Filter']['fields']['course_school']['choices'];
	
?>
                        <legend style="width:100%; overflow:hidden;">
                        	<span class="prefix"><select name="prefix">
                            	<option value="">Prefix</option>
                                <?php
									foreach($mwsubjects as $key => $value) {
									?><option <?php echo ($prefix == $key ? "selected" : ''); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option><?php
									}
								?>
                            	<?php /*?><option <?php echo ($prefix == "AFR" ? "selected" : ''); ?> value="AFR">AFR</option>
                                <option <?php echo ($prefix == "ANT" ? "selected" : ''); ?> value="ANT">ANT</option>
                                <option <?php echo ($prefix == "ARB" ? "selected" : ''); ?> value="ARB">ARB</option>
                                <option <?php echo ($prefix == "ART" ? "selected" : ''); ?> value="ART">ART</option>
                                <option <?php echo ($prefix == "BIO" ? "selected" : ''); ?> value="BIO">BIO</option>
                                <option <?php echo ($prefix == "CHE" ? "selected" : ''); ?> value="CHE">CHE</option><?php */?>
                            </select></span>
                            <span class="codeNumber"><input type="text" value="<?php echo $codeName ?>" name="codeNumber" placeholder="Code" /></span>
                            <span class="courseType"><select name="courseType">
                            	<option value="">Course Type</option>
                                <?php
									foreach($mwscoursetypes as $key => $value) {
									?><option <?php echo ($courseType == $key ? "selected" : ''); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option><?php
									}
								?>
                                <?php /*?><option <?php echo ($courseType == "africana-studies" ? "selected" : ''); ?> value="africana-studies">Africana Studies</option>
                                <option <?php echo ($courseType == "anthropology" ? "selected" : ''); ?> value="anthropology">Anthropology</option>
                                <option <?php echo ($courseType == "art-history" ? "selected" : ''); ?> value="art-history">Art History</option>
                                <option <?php echo ($courseType == "elective" ? "selected" : ''); ?> value="art-history">Elective</option>
                                <option <?php echo ($courseType == "liberal-core" ? "selected" : ''); ?> value="art-history">Liberal Core</option><?php */?>
                            </select></span>
                            <?php /*?><span class="cdelective"><select name="cdelective">
                            	<option value="">-- Elective --</option>
                                <?php
									foreach($mwcdelective as $key => $value) {
									?><option <?php echo ($cdelective == $key ? "selected" : ''); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option><?php
									}
								?>
                            </select></span><?php */?>
                            
                            <span class="courseSchool"><select name="courseSchool">
                            	<option value="">Program</option>
                                <?php
									foreach($mwcourseschools as $key => $value) {
									?><option <?php echo ($courseSchool == $key ? "selected" : ''); ?> value="<?php echo $key; ?>"><?php echo $value; ?></option><?php
									}
								?>
                                <?php /*?><option <?php echo ($courseSchool == "sse" ? "selected" : ''); ?> value="sse">School of Science &amp; Engineering</option>
                                <option <?php echo ($courseSchool == "ahss" ? "selected" : ''); ?> value="ahss">School of Arts, Humanities &amp; Social Sciences</option><?php */?>
                            </select></span>
                            <span class="keywords"><input type="text" value="<?php echo $keywords ?>" name="keywords" placeholder="Keyword / Phrase" /></span>
                            <?php /*?><input type="submit" name="submit" class="submitBtn" value="Submit" /><?php */?>
                            <button type="submit" name="submit" class="submitBtn"><i class="fa fa-search" title="Click to begain the search"></i></button>
                            <?php /*?><button type="reset" name="reset" class="resetBtn" onClick="window.location.href('<?php the_permalink(); ?>')"><i class="fa fa-undo"></i></button><?php */?>
                            <a class="resetBtn" href="<?php the_permalink(); ?>"><i class="fa fa-undo" title="Reset"></i></a>
                        </legend>
					  </form>
                    </div><!-- .searchForm -->
                </div><!-- .searchFilter -->
<?php

$args = array(
			'post_type' => 'course_catlog',
			'posts_per_page' => -1, 
			'meta_key'		=>	'subject_area',
			'sort_custom' 	=>	true,
			'meta_query' 	=>	array(
									array(
										'key' => 'subject_area',
										'value' => $prefix,
										'compare' => 'LIKE'
									),
									
									array(
										'key' => 'course_code',
										'value' => $codeName,
										'compare' => 'LIKE'
									),
									array(
										'key' => 'course_school',
										'value' => 430,
										'compare' => 'LIKE'
									)
								)
			);
query_posts($args);
	if ( have_posts() )
		the_post();
?>       
            <ul>
<?php
		rewind_posts();
	
	while ( have_posts() ) : the_post();
?>
            <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            	<?php 
					$mwPrefix = get_field('subject_area');
				?>
                <a href="javascript:void(0)" data-id="<?php the_ID(); ?>"><?php echo $mwPrefix['label']." ".get_field('course_code')." - ".get_the_title(); ?></a>
                <div class="cc_desc" style="display:none;"></div>
            </li>    
<?php endwhile;
wp_reset_query(); ?>
			</ul>	           
			</div><!-- #content -->   
            <?php  get_sidebar(); ?>
		</div><!-- #container -->

<?php get_footer(); ?>

<script>
jQuery('.course_catlog a').on('click', function(e) {
	e.preventDefault();
	var cthis = $(this);
	if(cthis.hasClass('selected')) {
		cthis.parent('li').removeClass('open');
		cthis.next('.cc_desc').slideUp();
		cthis.removeClass('selected');
		
	} else {
		
		cthis.addClass('selected');
		var postId = cthis.data('id'),
			mwDoc = cthis.next('.cc_desc');

		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		jQuery.ajax({
			type:"POST",
			url:ajaxurl,
			data: ({
				action : 'ajaxify',
				post_id: postId
			}),
			beforeSend: function() {
//				alert('loading');
				cthis.append('<div id="mwload">Loading</div>');
			},
			success:function(data){
				cthis.find('#mwload').remove();
				
				mwDoc.html(data);
				cthis.parent('li').addClass('open');
				mwDoc.slideDown();
//				console.log(data);
				more();
			},
			fail: function() {
				console.log('query failed');
			}
		});
	}
//	jQuery(this).next('.wps_abstractHide').toggle('slow');
});
function more() {
	jQuery('.morebtn').on('click', function(e){
		e.preventDefault();
		jQuery(this).prevAll('.show-more').slideDown();
		jQuery(this).fadeOut().next('.lessbtn').fadeIn();
	});
	jQuery('.lessbtn').on('click', function(e){
		e.preventDefault();
		jQuery(this).prevAll('.show-more').slideUp();
		jQuery(this).fadeOut().prev('.morebtn').fadeIn();
	});
}

</script>