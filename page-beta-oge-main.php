<?php
/*
 Template Name: Beat OGE Main 
*/

get_header('oge'); ?>
	<?php /*?><?php if(get_field('logo')) { ?>
    <div class="mw_center_logo">
        <a href="<?php the_field( 'logo_link' ); ?>"><img src="<?php the_field( 'logo' ); ?>" /></a>
    </div>
    <?php } ?><?php */?>
	 
 
    
   	<div class="mw_oge_mid">
   		<!--<div class="mwSlider">
   			<?php //echo do_shortcode('[rev_slider alias="oge_slider"]'); ?>
		</div>-->

<!--1349*395-->
<div class="featured-oge">
<?php the_post_thumbnail(); ?>
<div class="oge-centered">Our Mission</div>
<!--
<div class="phy-nav-bar">	
                    <ul>
                    <li><a href="/student-life">About <br/>Student Life</a></li>
                    <li><a href="/student-life/life-events/">Life <br/> Events</a></li>
                    <li><a href="/student-life/student-leadership-and-mentoring-program/">Student Leadership <br/>and Mentoring Program</a></li>
                    <li><a href="/metacurricular/">Meta Curricular<br /> Transcript</a></li>
                    <li><a href="/student-life/senior-year-experience/">Senior Year<br/> Experience</a></li>
                    <li><a href="/student-life/student-organizations-and-clubs/">Clubs and <br/> Organizations</a></li>
                    <li><a href="https://habib.edu.pk/student-life/student-government/">Student<br/> Government</a></li>
                    <li><a href="/student-life/sense-of-community/">Sense of<br/> Community</a></li>
                    <li><a href="/student-life/student-resources/">Student<br/>Resources</a></li>
                    <li><a href="/student-life/week-of-welcome/">Week of <br/>Welcome</a></li>
                    </ul>
                  </div>
--> 
</div>

                  
        <div id="contentPart" class="inner_wraper">
              
        	<?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
            
          </div>
        <div class="featured">
						<a href="<?php $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); echo $url[0]; ?>" title="<?php the_title_attribute(); ?>">
						
						<?php if ((!in_the_loop() && Bunyad::posts()->meta('layout_style') == 'full') OR Bunyad::core()->get_sidebar() == 'none'): // largest images - no sidebar? ?>
						
							<?php the_post_thumbnail('main-full', array('title' => strip_tags(get_the_title()))); ?>
						
						<?php else: ?>
							
							<?php the_post_thumbnail('main-slider', array('title' => strip_tags(get_the_title()))); ?>
							
						<?php endif; ?>
						
						</a>
					</div>
    </div>

   

<?php get_footer('oge'); ?>
