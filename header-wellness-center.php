﻿<?php
/**
 * Header template for Wellness Center
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/wellness.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.waypoints.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/parallax.min.js"></script><script>
<!-- SmartSlider need ajax google APIs -->
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>

<script>
	jQuery( function() {
		// jQuery( "#tabs" ).tabs();
		
		jQuery('.btn.quickLinks').on('click', function(){
			if(jQuery('.btn.quickLinks').hasClass('opened'))
			{
				jQuery(this).removeClass('opened');
				jQuery(this).next('.quickMenu').slideUp();
			} else {
				jQuery(this).addClass('opened');
				jQuery(this).next('.quickMenu').slideDown();
			}
			
		});
	} );
</script>
  
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=538422112954938";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</head>

<body <?php body_class(); ?>>	
	
<div id="wrapper" class="hfeed">
    <div id="header">
    
        <!-- .inner_header -->	
    	<div class="inner_header poffice">
			<div class="logo">
				<?php /*?><a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>"><?php */?>
				<a href="/wellness-center/" title="<?php echo bloginfo(); ?>">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/wellnessCenter/wellness-logo.png" />
				</a>
			</div>
			
			<div class="mstopRight">
				<div class="search">
					<form id="po_searchform_wc" method="get" action="/">
						<input value="" name="s" id="s" placeholder="Search" type="text"> <input type="submit" id="submit" value="" />
					</form>
				</div>
				<div class="quickLinkBox">
					<a class="btn quickLinks" href="#">Quick Links</a>
					<?php if ( is_active_sidebar( 'wd-quick-links' ) ) : ?>
					<div class="quickMenu">
						<div class="quickMenuDiv">
							<?php dynamic_sidebar( 'wd-quick-links' ); ?>
							<a class="quickMenuHome" href="<?php echo home_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div><!--.inner_head-->
      	
      	<div class="poffice_header_bottom_wellness">
      		<?php wp_nav_menu( array(  'container_class' => 'po_menu', 'theme_location' => 'wellness_center_top' ) ); ?>
		</div><!-- .bottom_header -->
		
	</div><!-- #header -->
	<div id="main" class="wellness-center">
    
     