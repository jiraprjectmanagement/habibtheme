<?php
/**
 * Template for displaying the footer
 *
 */
?>
</div>
<!-- #main -->
</div>
<!--.inner_wrap-->
<div id="footer" role="contentinfo">
  <div class="inner_footer">
    <div class="main_footer">
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'First Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Second Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Third Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Fourth Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
      <div class="footer_col">
        <?php if ( ! dynamic_sidebar( 'Fifth Footer Widget Area' ) ) : ?>
        <?php endif; ?>
      </div>
    </div>
    <?php
	/*
	 * A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	//get_sidebar( 'footer' );
?>
  </div>
  

  <div id="site-info">
    <div class="inner_wraper">
      <div class="footer_social"> <span>Stay Connected</span>
        <ul>
          <li class="li"><a href="https://www.linkedin.com/company/habib-university" title="LinkedIn" target="_blank">linked in</a></li>
          
          <li class="tw"><a href="https://twitter.com/habibuniversity" title="Twitter" target="_blank">Twitter</a></li>
          
          <li class="vm"><a href="https://vimeo.com/habibuniversity" title="Vimeo" target="_blank">viemo</a></li>
          
          <li class="fb"><a href="https://www.facebook.com/HabibUniversity" title="Facebook" target="_blank">facebook</a></li>
          
          <li class="tm"><a href="https://instagram.com/habibuniversity" title="Instagram" target="_blank">instagram</a></li>
          
          <li class="moblieApp"><a href="/hu-mobile/" title="HU Mobile App" target="_blank">HU Mobile App</a></li>
        </ul>
      </div>

      <div class="footer_right">
        <p><a href="/contact-us/" style="color:#fff !important;">Contact Us </a>| Phone: +92 21 1110 42242 (HABIB)</br>
          © Habib University - All Rights Reserved | <a href="/privacy-statement/" target="_blank" style="color:#fff !important;">Privacy Statement</a></p>
      </div>
    </div>
    <!-- #site-info --> 
    
  </div>
</div>
<!-- #footer -->
</div>
<!-- #wrapper -->
<?php
	/*
	 * Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>

<?php /*?><script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script><?php */?>
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script>
	var $j = jQuery;
    $j(document).ready(function(){
		"use strict";
        //$j(".vav_wrap #nav-mobile").html($j("#nav-main").html());
        $j(".vav_wrap #nav-trigger span").click(function(){
            if ($j(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $j(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $j(this).removeClass("open");
            } else {
                $j(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $j(this).addClass("open");
            }
        });
		
		$j(".inner_header #nav-trigger").click(function(){
            if ($j(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $j(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $j(this).removeClass("open");
            } else {
                $j(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $j(this).addClass("open");
            }
        });
    });
</script>
<?php /*?><script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script><?php */?>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>


<?php if(!is_front_page()){ ?>
<?php /*?><script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script><?php */?>
<?php } ?>
<?php /*?><script type="text/javascript">
   function calcHeight()
   {
    //find the height of the internal page
    var the_height= document.getElementById('the_iframe').contentWindow.document.body.scrollHeight;

    //change the height of the iframe
    document.getElementById('the_iframe').height=the_height;
   }
</script><?php */?>
<?php if(!is_front_page()) { ?>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/script.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/cbpFWTabs.js"></script>
<script>
	(function() {

		[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
			new CBPFWTabs( el );
		});

	})();
	
</script>
<?php } ?>
</body></html>
<?php if(is_front_page()) { ?>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.bxslider.min.js"></script>
<?php } ?>
<?php if(!is_front_page()) { ?>
<script type="text/javascript">
	jQuery( document ).ready(function() {
		jQuery("h3.activeClass a").attr("aria-expanded", "true").addClass("active");
		jQuery("div.activeClass").attr("aria-hidden", "false").show();
		
//		jQuery(".mwUpcomingEvents a.pressRelease[href=#]").siblings('a.watchVideo').css('width', '100%');
//		jQuery(".mwUpcomingEvents a.watchVideo[href=#]").siblings('a.pressRelease').css('width', '100%');
		jQuery(".singleEvent a.btn[href=#], a.watchVideo[href=#], a.pressRelease[href=#]").hide();
		
		jQuery(".mwUpcomingEvents a.watchVideo").css('width', '100%');
		jQuery(".mwUpcomingEvents a.pressRelease").hide();
		
		jQuery(".tb_side .page-item-12535 a, .tb_side .page-item-12537 a").attr('target', '_blank');
		
		
		var $j = jQuery;
		setTimeout(function() {
			$j('.ff-stream.ff-layout-grid .ff-header').append('<div class="sdescription"></div>');
			$j('.ff-type-all').on('click', function() {
				
				$j('.sdescription').html( "<p> </p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-type-twitter').on('click', function() {
				
				$j('.sdescription').html( "<p>We LIVE tweet (#HULive) all our major seminars and symposia on our <a href=\"https://twitter.com/habibuniversity\" target=\"_blank\">Twitter channel</a> to encourage virtual attendance and interaction for those that cannot physically attend.</p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-type-instagram').on('click', function() {
				
				$j('.sdescription').html( "<p>We love showcasing our visual stories and connecting with fellow aesthetic-conscience members on <a href=\"https://www.instagram.com/habibuniversity/\" target=\"_blank\">Instagram</a>. This is generally a place where you can connect with us on an informal platform!</p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-type-facebook').on('click', function() {
				
				$j('.sdescription').html( "<p>Habib University’s <a href=\"https://www.facebook.com/HabibUniversity\" target=\"_blank\">Facebook page</a> is your one-stop portal to know everything about us – we update it in real-time about upcoming public events, HU News, Admissions & Recruitment cycles and more.</p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-type-vimeo').on('click', function() {
				
				$j('.sdescription').html( "<p>All our public events and symposia are recorded and uploaded to our <a href=\"https://vimeo.com/habibuniversity\" target=\"_blank\">Vimeo channel</a> and our specific Academic/Center(s) webpages.</p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-type-linkedin').on('click', function() {
				
				$j('.sdescription').html( "<p> </p>" );
			});
			$j('.ff-type-youtube').on('click', function() {
				
				$j('.sdescription').html( "<p><a href=\"https://www.youtube.com/channel/UCCWBA2qQjHLcFciqv3wKAAw\" target=\"_blank\">YouTube</a> is another way for us to connect with our global audience. Highlight of events and what we are up to are put up here but the full-length versions of our videos are always available on our Vimeo channel.</p>" );
				$j('.ff-loadmore-wrapper').show();
			});
			$j('.ff-filter[data-filter="posts"]').on('click', function() {
				
				$j('.sdescription').html("<p style='text-align: center;'>Want to keep up with the spontaneous side of our personality? Or just follow our Snapchat Ambassador? You know what to do.</p><p style='text-align: center;'><img src='https://habib.edu.pk/wp-content/uploads/2016/11/snapcode.png' width='120' height='120' /></p><p style='text-align:center;'><em>Hint: Use your Snapchat app to scan the above ‘Snapcode’. Launch the app on your phone, direct the camera to the HU Snapcode and touch the phone screen for 3 to 4 seconds. The App will then prompt you to add HabibUniversity (HU)_snaps to your Friend list. Proceed to enjoy our Snap-antics. </em></p>");
				$j('.ff-loadmore-wrapper').hide();
			});
			
			
			$j('.ff-filter.ff-type-posts').insertAfter($j('.ff-filter.ff-type-vimeo'));
			$j('.ff-filter.ff-type-posts').addClass('fa fa-snapchat-ghost').removeClass('ff-type-posts');
//			$j('.ff-filter.ff-type-twitter').insertAfter('.ff-filter.ff-type-facebook');
//			$j('.ff-filter.ff-type-instagram').insertAfter('.ff-filter.ff-type-twitter');
//			$j('.ff-filter.ff-type-vimeo').insertAfter('.ff-filter.ff-type-instagram');
//			$j('.ff-filter.ff-type-posts').insertAfter('.ff-filter.ff-type-vimeo');
//			$j('.ff-filter.ff-type-vimeo').insertAfter('.ff-filter.ff-type-posts');
			
			$j('.ff-item.ff-posts').remove();
			
			var grid = jQuery('.ff-stream-wrapper').data('shuffle');
    		if (grid) grid.update(); /* layout calculation on the fly */
			
		}, 3000);
	});
	
	
</script>
<?php } ?>
<?php /*?>
<!-- Faculty Page Tabs --> 
<script src="<?php echo get_stylesheet_directory_uri(); ?>/filterizr/jquery.filterizr.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/controls.js"></script>

<!-- Kick off Filterizr -->
<script type="text/javascript">
	jQuery(function() {
		//Initialize filterizr with default options
		jQuery('.filtr-container').filterizr({
			filter: 1,
		});
	});
</script>
<?php */?>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tab.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Place this script as near to the end of your BODY as possible. -->
<!-- Place this script as near to the end of your BODY as possible. -->
<script type="text/javascript">
  (function() {
    var x = document.createElement("script"); x.type = "text/javascript"; x.async = true;
    x.src = (document.location.protocol === "https:" ? "https://" : "http://") + "sg.libraryh3lp.com/js/libraryh3lp.js?353";
    var y = document.getElementsByTagName("script")[0]; y.parentNode.insertBefore(x, y);
  })();
</script>
<!-- Place this script as near to the end of your BODY as possible. -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".chat-closed").on("click",function(e){
        $(".chat-header,.chat-content,.needs-js").removeClass("hide");
        $(this).addClass("hide");
    });

    $(".chat-header").on("click",function(e){
        $(".chat-header,.chat-content,.needs-js").addClass("hide");
        $(".chat-closed").removeClass("hide");
    });
    
  
});
</script>
