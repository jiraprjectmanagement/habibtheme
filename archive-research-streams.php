<?php
/**
 * Template for displaying Research Stream Main Grid
 *
 *
 * @package WordPress
 * @subpackage Habib Uni
 * @since Habib Uni 2.0
 */

get_header('ycsd'); ?>

		<div id="container">
			<div id="content" role="main">

<?php
	/*
	 * Queue the first post, that way we know
	 * what date we're dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		the_post();
?>

			<h1 class="entry-title">
				<?php _e( 'Research Themes', 'habib' ); ?>
			</h1>
            <div id="researchStreams">
<?php
	rewind_posts();
				while ( have_posts() ) : the_post(); ?>
                    <article class="ycsdBox">
                    <?php if( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )) { ?>
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"  width="230" height="97" class="top" width="230" height="97" /></a>
                    <?php } ?>
                    	<?php 
						$icon = get_field('custom_icon');
						if($icon ) {
						?>
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $icon['url']; ?>" alt="<?php echo get_the_title(); ?>" /></a>    
                        <?php } ?>
                        <div class="ycsdInner">
                            <h4 class="ycsdTitle"><a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php the_title(); ?></a></h4>
                            <p class="ycsdText"><?php echo string_limit_words(get_the_excerpt(), 20).'...'; ?></p>
                            <a class="ycsdReadMore" href="<?php the_permalink(); ?>">Read More...</a>
                        </div>
                    </article>
			<?php endwhile; ?>
            	</div>

			</div><!-- #content -->
			
			<?php // get_sidebar(); ?>
            
            <div role="complementary" class="widget-area" id="primary">
            	<h3 class="widget-title side"><a href="/idrac/">Back to IDRAC</a></h3>
                <ul class="xoxo">
					<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
                </ul>
            </div>
		
        </div><!-- #container -->

<?php get_footer(); ?>
