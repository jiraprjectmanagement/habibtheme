<?php
/**
 * Sidebar template containing the primary and secondary widget areas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div id="primary" class="widget-area" role="complementary">

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'ogebar' ) ) : ?>
		<?php dynamic_sidebar( 'ogebar' ); ?>
<?php
	endif; 
?>
</div>
