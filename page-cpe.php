<?php
/*
 Template Name: CPE Pedagogy
*/
get_header('cpe'); ?>


<div class="container">
    <div class="inner-hero__banner__container">
        <img src="/wp-content/uploads/2020/10/cpe-banner.jpg"/>
        <div class="cpebanner1">Center for Pedagogical Excellence</div>
        <div class="cpebanner2">Covid-19 Response</div>
    </div>
    
    <div>
        
    </div>
    <div class="inner-hero__banner__container_inner">
       <p class="cpe_response_timeline">
       COVID 19 Resources</p>
    </div>
</div>

<div class="tabscpe" id="tabscpeid" style="display: none;">
  <a class="w3-bar-item w3-button current" data-tab="tab-1" onclick="openCity('October')" id="myLink"><i class="fa fa-calendar" aria-hidden="true"></i>Oct</a>
  <a class="w3-bar-item w3-button" data-tab="tab-2" onclick="openCity('September')"><i class="fa fa-calendar" aria-hidden="true"></i>Sept</a>
  <a class="w3-bar-item w3-button" data-tab="tab-3" onclick="openCity('August')"><i class="fa fa-calendar" aria-hidden="true"></i>Aug</a>
  <a class="w3-bar-item w3-button" data-tab="tab-4" onclick="openCity('July')" id="myLink"><i class="fa fa-calendar" aria-hidden="true"></i>Jul</a>
  <a class="w3-bar-item w3-button" data-tab="tab-5" onclick="openCity('June')"><i class="fa fa-calendar" aria-hidden="true"></i>Jun</a>
  <a class="w3-bar-item w3-button" data-tab="tab-6" onclick="openCity('May')"><i class="fa fa-calendar" aria-hidden="true"></i>May</a>
  <a class="w3-bar-item w3-button" data-tab="tab-7" onclick="openCity('April')"><i class="fa fa-calendar" aria-hidden="true"></i>Apr</a>
  <a class="w3-bar-item w3-button" data-tab="tab-8" onclick="openCity('March')"><i class="fa fa-calendar" aria-hidden="true"></i>Mar</a> 
</div>

<div id="October" class="w3-container tab-content city w3-animate-right" style="display: none;">>
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Faculty retreat for course evaluation</li>
            <li class="tab-bullets">Launch CPE 110</li>
            <li class="tab-bullets">Launch Tech in Practice</li>
            <li class="tab-bullets">Launch Tuning Protocol</li>
            <li class="tab-bullets">Launch course clinic</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-1.png"/>
    </div>
</div>

<div id="September" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Design thinking for engagement policies, culture, etc.</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group2.png"/>
    </div>
</div>

<div id="August" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Training on Canvas, Zoom, Panopto, addition to course proposal form</li>
            <li class="tab-bullets">SEL introduction</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-2.png"/>
    </div>
</div>

<div id="July" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Roll out of Panopto, Zoom and Canvas, introduction to CPE-101</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-3.png"/>
    </div>
</div>

<div id="June" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Evaluation of dedicated teaching platforms</li>
            <li class="tab-bullets">Selection of Canvas, Zoom, Panopto, development of teaching guides & internal teaching resources</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-4.png"/>
    </div>
</div>

<div id="May" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Faculty training, Norms of online learning, Culture, overcoming limitations</li>
            <li class="tab-bullets">Course reviews for a support perspective</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-5.png"/>
    </div>
</div>

<div id="April" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Formation of Online Learning Task force</li>
            <li class="tab-bullets">Policies on exams, grades</li>
            <li class="tab-bullets">Surveys to understand challenges</li>
            <li class="tab-bullets">Offer special courses from Edx, Coursera, LinkedIn to the community</li>
            <li class="tab-bullets">Formation of community lead support for online learning</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-6.png"/>
    </div>
</div>

<div id="March" class="w3-container tab-content city w3-animate-right" style="display:none">
  <div class="cpe-lft">
        <ul>
            <li class="tab-bullets">Evaluation of tools for emergency synchronous teaching</li>
            <li class="tab-bullets">Intro of Microsoft Teams</li>
            <li class="tab-bullets">Faculty training in teams and setting up courses</li>
        </ul>
    </div>
    <div class="cpe-rgt">
        <img src="/wp-content/themes/habib/css/images/Group-1.png"/>
    </div>
</div>

<div id="container" class="cpeban">

<p>Teaching in the digital space requires a completely new sets of skills and methodologies. These new requirements coupled with the constraints faced due to COVID-19, demand a completely new approach to teaching. At CPE, we have been working tirelessly to develop new and effective strategies and techniques to equip you with the critical skills required to teach in this new reality. We have also introduced many new digital platforms that not only make this transition easier but also have a profound impact on the overall learning experience.</p>
      <!--Box 1 Start-->
            <a href="/covid19/cpe-response/first-steps/">
                <div class="cpe-container-bx" style="background-color: #c92e2e;">
                    <img src="/wp-content/themes/habib/css/images/04-online book.png"/>
                    <div class="cpe-container-bx-v-middle">
                      <p>Developing a Course for Online Teaching: First Steps</p>
                    </div>
                </div>
            </a>
        <!--Box 1 End-->  
        <!--Box 1 Start-->
        <a href="/covid19/cpe-response/pedagogy/">
            <div class="cpe-container-bx" style="background-color: #f9b515;">
                <img src="/wp-content/themes/habib/css/images/14-test.png"/>
                <div class="cpe-container-bx-v-middle">
                  <p>Developing a Course for Online Teaching: Pedagogy</p>
                </div>
            </div>
        </a>
        <!--Box 1 End-->  
        <!--Box 1 Start-->
        <a href="/covid19/cpe-response/best-practices/">
            <div class="cpe-container-bx" style="background-color: #276ed8;">
                <img src="/wp-content/themes/habib/css/images/23-book.png"/>
                <div class="cpe-container-bx-v-middle">
                  <p>Best Practices</p>
                </div>
            </div>
        </a>
        <!--Box 1 End-->  
        <!--Box 1 Start-->
        <a href="/covid19/cpe-response/technology/learning-management-system-canvas/">
            <div class="cpe-container-bx" style="background-color: #62276f;">
                <img src="/wp-content/themes/habib/css/images/20-tablet.png"/>
                <div class="cpe-container-bx-v-middle">
                  <p>Technology Platforms & Support</p>
                </div>
            </div>
        </a>
        <!--Box 1 End-->  
</div>
<!-- #container -->


<script>
function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
    x[i].classList.remove("current");
  }
  document.getElementById(cityName).style.display = "block";
  //document.getElementById(cityName).classList.add("current");
  //document.getElementById("tabscpeid").className = "MyClass";
}
</script>

<script>

$(document).ready(function(){
  $('.tabscpe a').click(function(){
    var tab_id = $(this).attr('data-tab');
    var hash = window.location.hash;
    if (hash) {
        $('.tabscpe a, .tab-content').removeClass('current');
    }    
    
    
    $('.tabscpe a').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })
})

</script>


<?php get_footer('cpe'); ?>
