<?php
/*
 Template Name: Wellness Center Main
*/

get_header('wellness-center'); ?>

    <div class="container">
        <div class="wc_content">
			<?php	
			if ( have_posts() ) :
            	while (have_posts()) : the_post();
					the_content();
            	endwhile;
			endif; ?>
         
        </div>
        
        <?php /*?><div class="sideBar">
           <?php get_sidebar('wellness-center'); ?>
        </div><?php */?>
        
    </div>
	

<?php //get_footer(); ?>

<?php get_footer('footer-live'); ?>
