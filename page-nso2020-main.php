<?php
/*
 Template Name: NSO 2020 Main
*/
// get_header(''); ?>

<?php get_header('with-megamenu-live'); ?>

<div id="header">
     </div>
     <!--container_ad class removed due to ad keyword -->
   <div id="container_missions"> 
   
   
   <div class="mw_header_top">
   		<div class="inner_wraper">
   			<div class="mw_oge_menu1">               
                <?php echo "<h1>".'&nbsp;'.get_the_title()."</h1>"; ?>
            </div>
		</div>
        
	</div>
    
   
    
	<div id="content_stdaffairs" role="main" class="adm_inner" >
    
    <div class="rev_slider_wrapper" style="width:100%">
   			<!-- <?php //echo do_shortcode('[smartslider3 slider=4]'); ?> -->
               
		</div>
            
            <?php
			if ( have_posts() ) :
				while (have_posts()) : the_post();
                	the_content();
           		endwhile;
			endif; ?>
	       
    </div> 
           
         <div id="primary_nso2020" class="widget-area" role="complementary" /*style="margin-left:36px;"*/>
            
            <?php  dynamic_sidebar( 'nso2020_sidebar01' ); ?>
         </div> 
    </div>

 

<?php // get_footer(); ?>

<?php get_footer('footer-live'); ?>