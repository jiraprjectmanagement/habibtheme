<?php
/*
 Template Name: homepage-live
*/

// header-with-megamenu-live.php

get_header('with-megamenu-live'); ?>

		<div class="mw_home_tag">


    		<div class="inner_wraper">	

<style>

div#main {
    border-top: 0px solid #d6c396!important;
} 
rs-sbg-px, rs-sbg-wrap {
    top: -1px!important; 
}
</style>


            
             <?php add_revslider('home-page-test-video'); ?> 




<!-- <div id="marcomhome" class="owl-carousel owl-theme one-col mainpage <?php echo $class; ?>">
    <div class="mainpage" data-interval="1000">
        <a  href="https://eapplication.habib.edu.pk/" target="_blank"><img alt="hu-slider" data-src="/docs/sliders/Webslider-june21.jpg" class="lazyload"  /></a>
    </div> 
    <div class="mainpage" data-interval="1000">
        <a  href="https://habib.edu.pk/HU-news/habib-university-devises-holistic-academic-plan-in-response-to-caies-decision/" target="_blank"><img alt="hu-slider" data-src="/docs/sliders/CAIE-Response-02.jpg" class="lazyload"  /></a>
     </div> 
	<div class="mainpage" data-interval="1000">
        <a  href="https://eapplication.habib.edu.pk" target="_blank"><img alt="hu-slider" data-src="/docs/sliders/May-21-Web-banner.jpg" class="lazyload"  /></a>
    </div> 
	<div class="mainpage" data-interval="2000">
        <a  href="https://habib.edu.pk/admissions/student-finance/" target=""><img alt="hu-slider" data-src="/docs/sliders/scholarship-banner.jpg" class="lazyload"  /></a>
    </div>  
    <div class="mainpage" data-interval="2000">
        <a  href="https://habib.edu.pk/academics/sse/electrical-engineering/" target=""><img alt="hu-slider" data-src="/docs/sliders/ee-banner-1.jpg" class="lazyload"  /></a>
    </div>  
    <div class="mainpage" data-interval="2000">
        <a href="https://habib.edu.pk/academics/ahss/comm-design/" target=""><img alt="hu-slider" data-src="/docs/sliders/cnd-banner-1.jpg" class="lazyload"  /></a>
    </div>       
</div>  -->
              

              <!--Slider Ended-->
            <div class="homebox">
                <h2 style="text-align: center;  color: #5b2867;">BRINGING WORLD-CLASS LIBERAL ARTS EDUCATION TO PAKISTAN</h2>
                <p style="text-align: center;">Curate your own educational experience at Habib University under the guidance of leading faculty from around the world. At Habib University we aim to nurture the future of students by shaping them into critically conscious and determined graduates who positively impact society.</p>
            
            <div class="homeNewsEvents2">
            <div class="hucontent">
                    <article id="post-6661">
                        	<a href="/academics/sse/"><img alt="hu-article" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb1-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb1-o.jpg"></a>
                            	<div class="art_content4">
                            	   <h3>Dhanani School of Science and Engineering</h3>
                            	</div>
                    </article>
                    <article id="post-6661">
                        	<a href="/academics/ahss/"><img alt="hu-article" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb2-1-oo.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb2-1-oo.jpg"></a>
                            	<div class="art_content4">
                            	   <h3>School of Arts, Humanities & Social Sciences</h3>
                            	</div>
                    </article>
                    <article id="post-6661">
                        	<a href="/campus-tour/"><img alt="hu-article" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/campus-tour-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/campus-tour-o.jpg"></a>
                            	<div class="art_content4">
                            	   <h3>CAMPUS TOUR</h3>
                            	</div>
                    </article>
				<article id="post-6661">
                        	<a href="/giving/"><img alt="hu-article" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb-giving-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/school-thumb-giving-o.jpg"></a>
                            	<div class="art_content4">
                            	   <h3>GIVING</h3>
                            	</div>
                    </article>
                    
                </div>
                </div>         
                     
            </div>       

            <div class="container-hubynumbers">
                <h2>HU BY THE NUMBERS</h2>
                <div class="container-fluid justifyflex">
                  <a class="hubynumb_box lazyload before" href="/admissions/student-finance/">
                  <p style="font-size: 52px; font-weight: 600; display: inline;" class="counter-count">85</p>
                  <p style="display: inline;    font-size: 50px;  font-weight: 500;">%</p>
                    <p>Students Receive Financial Support</p>
                  </a>
                  <a class="hubynumb_box lazyload" id="hubynumb_box2" href="/academics/">
                  <p style="display: inline;font-size: 52px; font-weight: 600;  margin: 0px;" class="counter-count">6</p>
                    <p>World Class Degrees</p>
                  </a>
                  <a class="hubynumb_box lazyload" id="hubynumb_box3" href="/admissions/">
                  <p style="font-size: 52px; font-weight: 600; display: inline;" class="counter-count">12</p>
                  <p style="display: inline;    font-size: 50px;  font-weight: 500;">:1</p>
                    <p>Student to Faculty Ratio</p>
                  </a>
                </div>
            </div>
             
            </div>
           
            <!--Ratio Stats-->
            <!--Latest News API Start-->
            <div class="homeNewsEvents3">
            <h2 >LATEST NEWS &amp; UPCOMING EVENTS</h2>
            <div class="hucontent noradius">
            <?php
            
            $response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts/?&categories_exclude=255,257&per_page=5");
            // var_dump(wp_remote_retrieve_body( $response ));
            
                if( is_wp_error( $response ) ) {
            		return;
            	}
            	$posts = json_decode( wp_remote_retrieve_body( $response ) );
                $i=0;
            	if( empty( $posts ) ) {
            		return;
            	}
                	foreach( $posts as $post ) {
                	   // This loop runs only 5 times as shown below
                	    if($i==5) break;
                    ?>
                    		<article id="post-<?php echo $post->id; ?>">
                    		<?php 
                    			$img = $post->acf_field->home_page_thumbnail;
                    			if($img) {
                    		?>
                            	<a target="_blank" href="<?php echo $post->link; ?>"><img alt="hu-post" data-src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" class="lazyload"/></a>
                            <?php } ?>
                            	<div class="art_content2">
                                	<h3><?php echo $post->title->rendered; ?></h3>
                                	<a target="_blank" href="<?php echo $post->link; ?>">Read more</a>
                            	</div>
                        	</article>

                    <?php $i++; } ?>
                    
                    <!-- <article id="post-<?php echo $post->id; ?>">
                    		<?php 
                    			$img = $post->acf_field->home_page_thumbnail;
                    			if($img) {
                    		?>
                            	<a target="_blank" href="/sse-public-lecture/"><img alt="hu-post" data-src="/sse-public-lecture/thumbnail-DSSE-Lec.jpg" class="lazyload"/></a>
                            <?php } ?>
                            	<div class="    ">
                                	<h3>Online Symposium on Electric Vehicles: Challenges and Promises</h3>
                                	<a target="_blank" href="/sse-public-lecture/">Read more</a>
                            	</div>
                    </article> -->
                            
            </div>
            </div>
            <!--Latest News API End-->
            <!--Highlights From Archive Start -->
            <div class="homeNewsEvents">
            <h2 class="highlights-archives" style="color: #5b2867;padding-top: 13px;">HIGHLIGHTS FROM ARCHIVES</h2>
                <div class="hucontent">
                    <article id="post-6661" class="video-thumbnail1">
                        	<a target="_blank" data-lity href="https://www.youtube.com/watch?v=5IMSbJsyW-M"><img alt="hu-Highlights" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/video-1-OCS-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/video-1-OCS-o.jpg"></a>
                            	<div class="art_content3">
                            	   <h3>The Habib Edge: 360<span>&#176;</span> of Excellence</h3>
                            	</div>
                    </article>
                    <article id="post-6661" class="video-thumbnail1">
                        	<a target="_blank" data-lity href="https://www.youtube.com/watch?v=OoltJRfzQ-k"><img alt="hu-Highlights" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/350x250-01-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/350x250-01-o.jpg"></a>
                            	<div class="art_content3">
                            	   <h3>Corona Sey Agahi- Episode 19</h3>
                            	</div>
                    </article>
                    <article id="post-6661" class="video-thumbnail1">
                        	<a target="_blank" data-lity href="https://www.youtube.com/watch?v=vwvjih38V8E"><img alt="hu-Highlights" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/video-3-Chris-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/video-3-Chris-o.jpg"></a>
                            	<div class="art_content3">
                            	   <h3>New Student Orientation: Class of 2024</h3>
                            	</div>
                    </article>
                    <article id="post-6661" class="video-thumbnail1">
                        	<a target="_blank" data-lity href="https://www.youtube.com/watch?v=Ks9Hm1GR03M"><img alt="hu-Highlights" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/video-4-wasif-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/video-4-wasif-o.jpg"></a>
                            	<div class="art_content3">
                            	   <h3>Habib University - Transforming Higher Education</h3>
                            	</div>
                    </article>
                    <article id="post-6661" class="video-thumbnail1">
                        	<a target="_blank" data-lity href="https://www.youtube.com/watch?v=jIw5h6MX8Fw"><img alt="hu-Highlights" data-src="https://habib.edu.pk/wp-content/uploads/2021/09/video-conv-2020-o.jpg" class="lazyloaded" src="https://habib.edu.pk/wp-content/uploads/2021/09/video-conv-2020-o.jpg"></a>
                            	<div class="art_content3">
                            	   <h3>Convocation 2020 Highlights</h3>
                            	</div>
                    </article>
                </div>            
            </div>
            <!--Highlights From Archive End -->
	
 			 <!--Counter Start-->
            <script>
            
                    $('.counter-count').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 12000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            
            </script>
             <!--Counter End -->
			</div>
            
            
  		</div>
        <script>
            jQuery(document).ready(function() {
            	jQuery('#marcomhome').owlCarousel({
            	   startPosition:0,
            		loop: true,
                    rewind: true,
            		margin: 0,
                    autoplaySpeed:400, 
            		autoplay: false,
            		autoplayHoverPause: false,
            		dots: false,
                    video: true,
                    nav:true,
                    navigation : true,
            		responsiveClass: false,
            		responsive:{
                           0:{
                               items:1,
                               nav:true,
                               loop:false
                           }
            		}
              	});	
            });	
            
        </script>

<script>
    document.getElementById('vid').play();
</script>

 <?php //get_footer('hpv1footer'); ?> 


<?php // get_footer('hpv1footercovid'); ?>

<?php get_footer('footer-live'); ?>