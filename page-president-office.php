<?php
/*
 Template Name: President Office Main
*/

get_header('president-office'); ?>
<style>
    /* President Office Contact
    ------------------------------------------------------------- */
    
    .president_contact  {
    	padding: 5px;
    	display: flex;
    	flex-wrap: wrap;
    }
    .president_contact div {
        width: 31%;
        margin-bottom: 20px;
    	margin-right: 2%;
    }
    .president_contact div:nth-child(3n + 3) {
    	margin-right: 0;
    }
    
    div#images-tab-0 {
        border-top: 0px !important;
        background-color: #fff; 
    }
    div#images-tab-1 {
        border-top: 0px !important;
        background-color: #fff; 
    }
    /*
    .president_contact div:hover {
    	box-shadow:
                    1px 1px #ededed,
                    2px 2px #ededed,
                    3px 3px 8px #ededed;
            -webkit-transition: width 2s;  
        transition: width 4s;
    }
    */
    .president_contact img {
    	padding-bottom: 10px;
    	width: 95%;
    }
    .president_contact h3 {
    	font-size: 18px;
    	color:#5c2568;
        margin-bottom: 5px;
    }
    /*.president_contact a {
    	padding: 5px;
    	background-color: #5c2568;
    	color: #fff !important;
    	text-align: center;
    	width: 92%;
    	margin-top: 10px;
    	display: inherit;
    	text-decoration: none;
    }*/
    .president_contact div:nth-child(odd) {
    	clear: LEFT;
    }
    .president_contact div:nth-child(even) {
    	/*margin-right: 0;*/
    }
    .president_contact p {
    	margin-bottom: 0px !important;
    	line-height: 22px
    }
    fg-listItem{
    	margin-right: 100px !important;
    }
    .presvideo_thumb {
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
    }
    .presvideo_thumb > div {
        background: #F5F5F5;
        color: #2C3E50;
        margin-bottom: 20px;
        border: 1px solid #cdcdcd;
        width: 31%;
        margin-right: 3%;
    }
    .presvideo_thumb > div:nth-child(3n+3) {
    	margin-right: 0;
    }
    .presvideo_thumb a {
    	
    }
    .presvideo_thumb img {
    	margin-bottom: 5px;
    	width: 100%;
    }
    .presvideo_thumb h4 {
    	line-height: 18px;
    	padding: 0px 20px 10px;
    	font-size: 14px;
    }
    .hd_media_tab{
        background-color: #d6ccce;
        padding: 10px;
        text-align: center;
        font-weight: 600;
        color: #5c2568;
    }
    
    .po_content #tabs.ui-tabs .ui-tabs-nav li {
    	background-color: transparent;
    	border: 0px solid #5c2568;
    	border-radius: 3px;
    	
        display: flex;
        height: 30px;
        align-items: center;
    	margin-right: 5px;
    }
    .po_content #tabs.ui-tabs .ui-tabs-nav li a {
    	color: #5c2568;
        font-size: 16px;
    }
    .po_content #tabs.ui-tabs .ui-tabs-nav li.ui-tabs-active {
    	background-color: none;
    	border-color: none;
    }
    
    .gov_forum_box
    {   clear: both;
        float: left;
        width: 100%;
        display: -webkit-flex;
        display: flex;
        -webkit-align-items: initial;
        align-items: initial;
        -webkit-justify-content: flex-start;
        justify-content: flex-start;
        -webkit-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-flex-flow: row wrap;
        flex-flow: row wrap;}
    
    .gov_forum_div{    
    	width: 20%;
        padding: 15px;
        background-color: #f5f5f5;
        margin-left: 1%;
        margin-bottom: 15px;
    	transition: 0.5s;
    	cursor: pointer;
    	border-bottom: 2px solid #fff;
      -webkit-transition:0.5s; height: 30px; background-image: url(http://bkp.habib.edu.pk/wp-content/uploads/2018/01/contract.png); background-repeat: no-repeat; background-position: 12px 18px; background-size: 20px; float: left;}
    
    .gov_forum_div:hover{ border-bottom: 2px solid #5c2568; color: #5c2568; font-weight: bold;}
    
    
    .gov_forum_div a {
    	width: 100%;
        display: block;
    	color: #5c2568; text-decoration: none; border-bottom: none !important; line-height: 28px; padding-left: 32px;
    }
    
    .poGrid {
        clear: both;
        float: left;
        width: 100%;
        display: -webkit-flex;
        display: flex;
        -webkit-align-items: initial;
        align-items: initial;
        -webkit-justify-content: flex-start;
        justify-content: flex-start;
        -webkit-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-flex-flow: row wrap;
        flex-flow: row wrap;
    }
    
    .poGrid .pogridItem:nth-child(1), .poGrid .pogridItem:nth-child(2) {
        background-color: #fff; border: solid 1px #ccc;
    }
    
    .poGrid .pogridItem:nth-child(odd) {
        clear: left;
        margin-left: 0;
        margin-right: 1%;
    }
    
    .poGrid .pogridItem {
        width: 44%;
        padding: 2%;
        background-color: #f5f5f5;
    	border: solid 1px #ccc;
        margin-left: 2.1%;
        margin-bottom: 15px;
    	float: left;
    }
    .pogridItem h2{font-size: 18px;}
    .pogridItem h3 {font-size: 15px;}
    
    .po_content h2 {padding-bottom: 10px;}
    
    .em-calendar thead {
    	background:#5c2568; color: #fff;
    }
    .em-calendar thead a { color: #fff !important;
    }
    
    td.eventful a {
    	font-weight: bold; color: #5c2568 !important; padding:2px !important;
    } 
    td.eventful a:hover {
    	background-color: #5c2568; color: #fff !important;
    }
    
    /* President Office - Start
    ------------------------- */
    .p_int_img
    {
            padding-left:20px;
            height: 250px;
            width: auto;
            float: right;
    }
    .inner_header.poffice {
    	max-width: 1300px;
    	width: 97%;
    }
    .inner_header.poffice .logo {
        width: 369px;
        float: left;
        margin: 17px auto 0;
        position: relative;
        left: auto;
    }
    .inner_header.poffice .mstopRight {
    	float: right;
    	width: 400px;
    	margin-top: 20px;
    }
    #wrapper .mstopRight .search {
        float: left;
        width: 240px;
        margin-top: 19px;
        margin-bottom: 0;
    }
    .mstopRight #po_searchform {
    	
    }
    #wrapper #po_searchform input[type="text"] {
        width: 170px;
        float: left;
        border: 0 none;
        border-bottom: 1px solid #5c2568;
        box-shadow: none;
    	transition: all .2s linear;
    	margin-left: 20px;
    }
    #wrapper #po_searchform input[type="text"]:focus {
    	width: 190px;
    	margin-left: 0;
    }
    #wrapper #po_searchform input[type="submit"] {
    	background: url('<?=get_template_directory_uri()?>/images/presidentOffice/icon-search.jpg') 0 0 no-repeat;
    	width: 27px;
    	height: 27px;
    	border: 0;
    	cursor: pointer;
    	padding: 0;
    }
    
    .mstopRight .quickLinks {
        width: 100px;
        line-height: 40px;
        float: right;
        background-color: #5c2568;
        border-radius: 0;
        padding-left: 35px;
        color: #e7e3e8 !important;
        position: relative;
        overflow:hidden;
        padding:0px 0px 0px 35px;
    }
    .mstopRight .quickLinks:before {
    	position: absolute;
        content: '';
    	bottom: 10px;
        left: 10px;
        border-bottom: 10px solid #e7e3e8;
        border-right: 10px solid transparent;	
    	transition: all .3s linear;
        overflow:hidden;
    }
    .mstopRight .quickLinks.opened:before {
        bottom: initial;
        left: initial;
    	top: 10px;
        right: 10px;
        border: 0;
    	border-top: 10px solid #e7e3e8;
        border-left: 10px solid transparent;
        overflow:hidden;
    }
    .quickLinkBox {
    	position: relative;
    	float: right;
        overflow:hidden;
    }
    .quickMenu {
        border: 1px solid #ccc;
        position: fixed;
        top: 20%;
        width: 650px;
        right: 0;
        padding: 20px;
        background-color: #fff;
        display:none;
    }
    .quickMenuDiv {
    	display: flex;
    	flex-wrap: wrap;
        align-items: flex-start;
    	position: relative;
    }
    .quickMenuDiv > div {
        width: 100%;
    }
    .quickMenuDiv > div:nth-child(3) {
    	margin-right: 0;
    }
    .quickMenuDiv .widget-title {
        font-size: 15px;
        border-bottom: 1px solid #5c2568;
        padding-bottom: 7px;
        color: #111;
        font-family: Open Sans;
        margin-bottom: 5px;
    }
    .quickMenuDiv ul.menu,
    .quickMenuDiv ul.menu > li > ul.sub-menu {
    	list-style: none;
    	margin: 0;
    }
    .quickMenuDiv ul.menu {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        height: 400px;
    }
    .quickMenuDiv ul.menu > li {
    	width: 23.5%;
    	margin-right: 1%;
        margin-bottom: 10px;
    }
    .quickMenuDiv ul.menu > li:nth-child(1) {
    	order: 1;
    }
    .quickMenuDiv ul.menu > li:nth-child(2) {
    	order: 2;
    }
    .quickMenuDiv ul.menu > li:nth-child(3) {
    	order: 3;
    }
    .quickMenuDiv ul.menu > li:nth-child(4) {
    	order: 4;
    }
    .quickMenuDiv ul.menu > li:nth-child(5) {
    	order: 4;
    }
    .quickMenuDiv ul.menu > li:nth-child(6) {
    	order: 1;
    }
    .quickMenuDiv ul.menu > li:nth-child(7) {
    	order: 2;
    }
    .quickMenuDiv ul.menu > li:nth-child(8) {
    	order: 3;
    }
    .quickMenuDiv ul.menu > li > a,
    .quickMenuDiv ul.menu > li > ul.sub-menu > li > a {
        text-decoration: none;
        color: #111;
        line-height: 21px;
    }
    .quickMenuDiv ul.menu > li > a {
    	font-size: 15px;
    	border-bottom: 1px solid #5c2568;
        padding-bottom: 7px;
        margin-bottom: 5px;
    	display: block;
    }
    .quickMenuDiv ul.menu > li > ul.sub-menu ul {
    	display: none;
    }
    .quickMenuDiv ul.menu > li > ul.sub-menu > li {
    	
    }
    .quickMenuDiv ul.menu > li > ul.sub-menu > li > a {
    	font-size: 11px;
    }
    .quickMenuDiv .quickMenuHome {
    	position: absolute;
    	bottom: 0;
        right: 0;
        font-size: 30px;
        color: #743399;
    }
    
    .poffice_header_bottom {
    	width: 100%;
    	min-height: 42px;
    	background-color: #5c2568;
    	margin-bottom: 20px;
    	border-bottom: 2px solid #e1b346;
    }
    .poffice_header_bottom .po_menu {
    	max-width: 1300px;
    	margin: auto;
    	width: 95%;
    }
    .poffice_header_bottom .po_menu > .menu {
    	margin: 0;
        list-style: none;
        float: left;
    }
    .poffice_header_bottom .po_menu > .menu > li {
    	float: left;
    	padding: 12px;
    	padding-right: 15px;
    }
    .poffice_header_bottom .po_menu > .menu > li:first-child {
    	padding-left: 0;
    }
    .poffice_header_bottom .po_menu > .menu > li a {
    	color: #fff;
    	text-decoration: none;
        font-size: 13px;
    }
    
    .poffice_header_bottom .po_menu > .menu:hover .sub-menu {
      visibility: visible; /* shows sub-menu */
      opacity: 1;
      z-index: 1;
      transform: translateY(0%);
      transition-delay: 0s, 0s, 0.3s; /* this removes the transition delay so the menu will be visible while the other styles transition */
    }
    
    .poffice_header_bottom .po_menu > .menu .sub-menu {
    	display: none;
    	list-style: none;
    	margin: 0;
    	padding-top: 13px;
    	position: absolute;
    	width: 210px;
    	transform: translateY(-2em);
      z-index: -1;
      transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s, z-index 0s linear 0.01s;
    }
    .poffice_header_bottom .po_menu > .menu li:hover > .sub-menu {
    	display: block;
    }
    .poffice_header_bottom .po_menu > .menu .sub-menu li {
    	background-color: #5c2568;
    	display: block;
    	border-bottom: 1px solid #551f61; padding:3px;
    }
    
    .poffice_header_bottom .po_menu > .menu .sub-menu li:hover {
    	background-color:#632a6f;
    }
    .poffice_header_bottom .po_menu > .menu .sub-menu li a {
    	display: block;
    	padding: 5px 10px;
    }
    
    .poffice_header_bottom_wellness {
    	width: 100%;
    	min-height: 42px;
    	background-color: #5c2568;
    	margin-bottom: 20px;
    	border-bottom: 2px solid #e1b346;
    }
    .wc_img img {
        object-fit: cover;
        height: 122px;
    }
    .wc_left_content {
        margin: 0 auto;
        width: auto;
    }
    .fg-panel .fg-theme-white, .fg-panel .fg-theme-white:visited {
        zoom: 1.2 !important;
        color: #ffffff !important;
        background: #5c2568 !important;
    }
    .phySidebar.wellnessSidebar h1 {
        font-family: initial;
        font-size: 24px;
        color: #5d2468;
        }
    .fg-album-thumbnail-title {
        text-transform: capitalize !important;
    }
    li.tp-revslider-slidesli.active-revslide {
        height: 70% !important;
    }
    .poffice_header_bottom_wellness .po_menu {
    	max-width: 1300px;
    	margin: auto;
    	width: 95%;
    }
    .poffice_header_bottom_wellness .po_menu > .menu {
    	margin: 0;
        list-style: none;
        float: left;
    }
    .poffice_header_bottom_wellness .po_menu > .menu > li {
    	float: left;
    	padding: 12px;
    	padding-right: 15px;
    }
    .poffice_header_bottom_wellness .po_menu > .menu > li:first-child {
    	padding-left: 0;
    }
    .poffice_header_bottom_wellness .po_menu > .menu > li a {
    	color: #fff;
    	text-decoration: none;
        font-size: 13px;
    }
    
    .poffice_header_bottom_wellness .po_menu > .menu:hover .sub-menu {
      visibility: visible; /* shows sub-menu */
      opacity: 1;
      z-index: 1;
      transform: translateY(0%);
      transition-delay: 0s, 0s, 0.3s; /* this removes the transition delay so the menu will be visible while the other styles transition */
    }
    
    .poffice_header_bottom_wellness .po_menu > .menu .sub-menu {
    	display: none;
    	list-style: none;
    	margin: 0;
    	padding-top: 13px;
    	position: absolute;
    	width: 210px;
    	transform: translateY(-2em);
      z-index: -1;
      transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s, z-index 0s linear 0.01s;
    }
    .poffice_header_bottom_wellness .po_menu > .menu li:hover > .sub-menu {
    	display: block;
    }
    .poffice_header_bottom_wellness _wellness _wellness .po_menu > .menu .sub-menu li {
    	background-color: #5c2568;
    	display: block;
    	border-bottom: 1px solid #551f61; padding:3px;
    }
    
    .poffice_header_bottom_wellness .po_menu > .menu .sub-menu li:hover {
    	background-color:#632a6f;
    }
    .poffice_header_bottom_wellness .po_menu > .menu .sub-menu li a {
    	display: block;
    	padding: 5px 10px;
    }
    .fg-thumbHolder.radykal-clearfix {
            display: inline-flex;
    }
    img.fg-thumb {
        object-fit: cover;
    }
    .fg-thumbnail-container { 
    }
    .wc_sectionFour table {
        opacity: 0.9;
    }
    .president-office .container {
    	width: 95%;
    	max-width: 1270px;
    	margin: auto;
    	border: 1px solid #e4e0e6;
    	padding: 20px 14px;
    	overflow: hidden;
    }
    .po_content #one h4 {
    	margin-bottom: 20px; color:#5c2568;
    	position: relative;
    	text-align: center;
    }
    .po_content #one h4:before {
    	content: '';
    	position: absolute;
    	height: 1px;
    	width: 100%;
    	background-color: #e4e4e4;
    	left: 0;
    	top: 0; bottom: 0;
    	margin: auto;
    	z-index: -1;
    }
    .po_content #one h4 span {
    	background-color: #fff;
    	display: inline-block;
    	padding: 2px 5px;
    }
    .president-office .container .po_content {
    	max-width: 860px;
    	max-width: 900px;
    	width: calc(100% - 330px);
        float: left;
        font-size: 14px;
        font-weight: normal;
        line-height: 24px;
    }
    .president-office .container .po_content a {
    	color: #666;
    	text-decoration: none;
    }
    .president-office .container .po_content a:hover {
    	color: #5c2568;
        border-bottom: 1px;
    }
    .rtbs_menu .current {
        border-bottom: 1px solid #5c2568 !important;
    }
    .president-office .container .sideBar {
    	float: right;
    	width: 326px;
    	position: relative;
    	padding-bottom: 45px;
    }
    
    .president-office .container #primary {
        margin-top: 15px;
        width: 100%;
        background-color: #f9f9f9;
        padding: 15px;
        max-width: 296px;
    	font-size: 14px;
    }
    .president-office .container #primary h3 {
    	font-size: 22px;
        color: #5c2568;
        margin-bottom: 15px;
        line-height: 25px;
    	font-weight: bold;
    }
    .president-office .container #primary article {
    	width: 100%;
    }
    .president-office .container #primary article img {
        object-fit: initial;
    }
    .president-office .container #primary article > a {
    	float: left;
        overflow: hidden;
        width: 100%;
        height: 156px;
        margin-bottom: 10px;
    }
    .icn-explain,
    .icn-clock {
    	background: url(<?=get_template_directory_uri()?>/images/presidentOffice/icon-i.png) 0 0 no-repeat;
        width: 31px;
        height: 31px;
        float: left;
        margin-right: 10px;
        margin-top: -2px;
    }
    .icn-clock {
    	background: url('<?=get_template_directory_uri()?>/images/presidentOffice/icon-clock.png') 0 0 no-repeat;
    	margin-top: 5px;
    }
    .president-office .container #primary dl.dl-poffice {
    	padding-left: 40px;
    	font-size: 14px;
    }
    .president-office .container #primary dl.dl-poffice dt {
        float: left;
        width: 55%;
    }
    .president-office .container #primary dl.dl-poffice dd {
    	margin-bottom: 12px;
    }
    .president-office .container #primary .poffice-fra {
        margin-left: 35px;
        line-height: 28px;
    	font-weight: 600;
    //  text-shadow: 1px 0px 0px #5c2568;
    	margin-bottom: 25px;
    }
    .president-office .container #primary .poffice-fra i {
    	background: url('<?=get_template_directory_uri()?>/images/presidentOffice/icon-badge.png') 0 0 no-repeat;
    	width: 69px;
        height: 93px;
        float: left;
        margin-right: 10px;
    }
    
    .president-office .container h1 {
    	font-size: 26px;
        color: #5c2568;
        text-transform: uppercase;
        font-weight: bold;
        margin-bottom: 10px;
        //letter-spacing: 1px;
    }
    .president-office .container h1.cat-title {
    	
    }
    
    p.contact_af {
        margin-bottom: 20px;
    }
    
    .gov_forum_minutes {display: flex; margin-top: 20px;}
    .gov_forum_minutes ul li {list-style: none; padding-bottom: 25px;}
    .gov_forum_minutes a {background-image: url('http://bkp.habib.edu.pk/docs/pdf-icon.png'); background-repeat: no-repeat; background-position: left; padding: 10px 40px;
        color: #5c2568 !important;}
    .calendarlink {background-image: url('http://bkp.habib.edu.pk/docs/pdf-icon.png'); background-repeat: no-repeat; background-position: left; padding: 10px 40px; text-decoration: none; border-bottom: none !important;
        color: #5c2568 !important;}
    .gov_forum_table {margin-bottom: 20px;}
    
    .gov_forum_btn {
        display: none;
        background-image: url('http://bkp.habib.edu.pk/docs/mcalendar.png'); 
                    background-color: #f3f3f3; 
                    color:#5c2568; background-repeat: 
                    no-repeat; background-position:
                     8px 6px; padding: 10px 7px 10px 40px; 
                     text-decoration: none; margin: 0 auto; 
                     border:solid 1px #f3f3f3; width: 250px;
                      font-size: 16px; box-shadow: 
                      1px 3px 6px #ccc;    width: 80%;
                     }
    .meeting_btn{
        display: none;
        width: 299px;
        display: block;
        overflow: hidden;
        margin: 0 auto;
    	border-bottom: 0px !important;
    }
    .gov_forum_btn:hover {border:solid 1px #5c2568;}
    
    .gov_forum_mcalendar {display: flex; margin-top: 20px;}
    .gov_forum_mcalendar ul li {list-style: none; padding-bottom: 25px;}
    .gov_forum_mcalendar a {background-image: url('http://bkp.habib.edu.pk/docs/mcalendar.png'); background-repeat: no-repeat; background-position: left; padding: 10px 40px;
        color: #5c2568 !important;}
    
    
    .featuredimg {
    	
    }
    .fullwidthImg img,
    .featuredimg img {
    	width: 100%;
    }
    .single .president-office .container h1 {
    	float: left;
    	width: calc(100% - 180px);
    }
    .po_content .entry-meta {
    	display: block;
    }
    .single .po_content .entry-meta {
    	float: right;
    }
    
    .pr_vidBox a {
    	color: #5c2568;
    	font-size: 12px;
    }
    
    #tabs * {
    	outline: none;
    }
    #tabs.leadership {
    	
    }
    #tabs.leadership > ul.ui-tabs-nav {
        list-style: none;
        margin: 0 0 30px;
    	display: flex;
    	justify-content: center;
    	background-color: #f3f3f3;
    	padding: 18px;
    	border-radius: 2px;
    }
    #tabs.leadership > ul.ui-tabs-nav li {
    	margin: 0 20px;	
    	position: relative;
    }
    #tabs.leadership > ul.ui-tabs-nav li:after,
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active:after {
    	-webkit-transition: all .4s linear;
    	-o-transition: all .4s linear;
    	transition: all .4s linear;
    	content: '';
        position: absolute;
        left: 0;
        right: 0;
        bottom: -15px;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 0px solid #f3f3f3;
        width: 0;
        height: 0;
        margin: auto;
    }
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active:after {
        bottom: -27px;
        border-top: 10px solid #f3f3f3;
    }
    #tabs.leadership > ul.ui-tabs-nav li a {
    	color: #5c2568;
    	text-decoration: none;
    	font-size: 14px;
    }
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active a {
    	border-bottom: 1px solid #5c2568;
    }
    /*
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active a:after,
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active a:before {
        position: absolute;
        content: '';
        left: 0;
        right: 0;
        margin: auto;
        bottom: -7px;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #f3f3f3;
        width: 0;
    }
    */
    #tabs.leadership > ul.ui-tabs-nav li.ui-tabs-active a:after {
    	
    }
    #tabs.leadership > .ui-tabs-panel {
    	display: none;
    }
    #tabs.leadership > .ui-tabs-panel {
    	
    }
    .poBogItem_h
        {
          width: 185px;  
        }    
    .po_bog_h {
        width: auto;
        float: left;
        margin-left: 30px;
        margin-bottom: 20px;
    }
    .poBogItem_h img {
        width: 100%;
        height: 190px;
        object-fit: cover;
        border: 2px solid #5c2568;
    }
    .poBogItem_h p {
        color: #666;
        line-height: 18px;
        font-size: 13px;
    }
    .poBogItem_h a {
        text-decoration: none;
        color: #5c2568 !important;
    }
    .poBogItem_h h3 {
        margin-bottom: 0px;
    }
    .bogList {
    	display: flex;
    	flex-wrap: wrap;
    	align-items: flex-start;
    }
    .bogList .po_bog {
    	width: 25%;
    	margin-bottom: 20px;
    }
    .poBogItem {
    	width: 190px;
    	color: #5c2568;
    	margin: auto
    }
    .poBogItem img {
    	width: 100%;
        height:190px;
        object-fit: cover;
    	border: 1px solid #5c2568;
    }
    .poBogItem h3 {
    	margin-bottom:0px !important;
    }
    .poBogItem a {
    	text-decoration: none;
    	color: #5c2568 !important;
    }
    .poBogItem p {
    	line-height: 18px;
        font-size: 13px;
    }
    
    .president-office .container .po_content > .category-president-blog {
    	padding-left: 0;
    	clear: both;
    	margin-bottom: 20px;
    	padding-bottom: 20px;
    	overflow: hidden;
    	border-bottom: 1px dotted #e4e4e4;
    }
    .president-office .container .po_content > .category-president-blog .po_left {
    	width: 300px;
    	float: left;
    	margin-right: 20px;
    	overflow: hidden;
    }
    .president-office .container .po_content > .category-president-blog .po_left img {
    	height: 200px;
    	float: left;
    }
    .president-office .container .po_content > .category-president-blog .po_right {
    	width: calc(100% - 350px);
    	float: left;
    }
    .president-office .container .po_content > .category-president-blog .po_right .entry-title a {
    	color: #5c2568;
    	border: none;
    }
    .president-office .container .po_content > .category-president-blog .po_right .entry-summary a {
    	
    }
    .president-office .container .po_content > .category-president-blog .po_right .entry-meta a,
    .single .po_content .entry-meta a,
    .single .po_content .addtoany_list a,
    .president-office .container .po_content .tabs a {
    	border: none
    }
    
    #footer.po-footer {
    	border-top: 5px solid #5c2568;
    	margin-top: 50px;
    	padding-top: 10px;
    }
    #footer.po-footer .inner_footer {
    	max-width: 1300px;
    	position: relative;
    	overflow: hidden;
    	min-height: 160px;
    }
    #footer.po-footer .inner_footer:before {
    	content: '';
        width: 1px;
        background-color: #b6b6b6;
        height: 150px;
        display: block;
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
        transform: rotate(30deg) translate(-60px, 35px);
    }
    #footer.po-footer .poFtLeft,
    #footer.po-footer .poFtRight {
    	width: 50%;
        float: left;
        padding-top: 30px;
    }
    #footer.po-footer .poFtRight {
    	
    }
    #footer.po-footer .poFtLeft .poflogo {
    	float: left;
    	filter: grayscale(100%);
    	transition: all .5s linear;
    }
    #footer.po-footer .poFtLeft .poflogo:hover {
    	filter: grayscale(0);
    }
    #footer.po-footer .poFtLeft .pofmail {
    	float: left;
        padding-top: 15px;
    }
    #footer.po-footer .poFtLeft .pofmail li {
    	list-style: none;
    	padding: 10px 0 5px 40px;
    }
    #footer.po-footer .poFtLeft .pofmail li:first-child {
    	background: url('<?=get_template_directory_uri()?>/images/presidentOffice/icn-mail.jpg') left center no-repeat;
    }
    #footer.po-footer .poFtLeft .pofmail li:last-child {
    	background: url('<?=get_template_directory_uri()?>/images/presidentOffice/icn-fbook.jpg') left center no-repeat;
    }
    #footer.po-footer .poFtLeft .pofmail li a {
    	color: #5c2568;
    	text-decoration: none;
    	font-size: 16px;
    }
    
    .poFtRight .ftr_menu > .menu {
    	margin: 0;
        list-style: none;
        display: flex;
        flex-wrap: wrap;
        width: 85%;
        float: left;
    }
    .poFtRight .ftr_menu > .menu > li {
    	margin-bottom: 10px; float: left;
    }
    .poFtRight .ftr_menu > .menu > li:nth-child(2n+1) {
        width: 30%;
    }
    .poFtRight .ftr_menu > .menu > li:nth-child(2n+2) {
        width: 50%;
    }
    .poFtRight .ftr_menu > .menu > li:nth-child(2n+1) {
        width: 35%;
    }
    .poFtRight .ftr_menu > .menu > li:nth-child(2n+2) {
    	width: 50%;
    }
    
    .poFtRight .ftr_menu > .menu > li a {
    	color: #5c2568;
    	font-size: 14px;
    	text-decoration: none;
    }
    
    
    .president-office .fg-panel .fg-album-thumbnail:nth-child(3n + 3) {
        margin-right: 0 !important;
    }
    
    /* President Office - End
    ------------------------- */
    
    /* Office Of the President - Start
    ------------------------- */
    
    /* 
      ##Device = Desktops
      ##Screen = 1281px to higher resolution desktops
    */
    
    @media (min-width: 1281px) {
                  
                          
                      .vav_wrap {
                       
                    }
                    
                .vav_wrap_po
                        {
                            display:none;
                        }
                  
                }
    
    /* 
      ##Device = Laptops, Desktops
      ##Screen = B/w 1025px to 1280px
    */
    
    @media (min-width: 1025px) and (max-width: 1280px) {
      
        .vav_wrap_po
                {
                    display:none;
                }
      .img_center {
            margin-bottom: 0px;
            margin-top: 12px;
            width: 240px;
            height: 50px;
        }
      
    }
    
    /*   ##Device = Tablets, Ipads (portrait)   ##Screen = B/w 768px to 1024px */
    
    
    /*   ##Device = Tablets, Ipads (landscape)   ##Screen = B/w 768px to 1024px */
    
    @media only screen and (min-device-width : 768px)  and (max-device-width : 1024px)  and (orientation : landscape)
    {
        .poffice_header_bottom .po_menu
         {
           overflow: hidden;
         } 
         .inner_header.poffice .mstopRight
         {
            width:375px;
         }
         .logo img {
            margin: 4%;
         }
         .inner_header.poffice .mstopRight {
                width: 240px;
         }
         .inner_header.poffice .logo
         {
            margin: 1%;
         }
         .vav_wrap_po {
            display: none;
        }
        .poffice_header_bottom
        {
            margin-top:10px;
        }
        .left_btn {
        float: left;
        margin-left: 0px !important;
        }
        .poBogItem {
        width: 96%;
        }
        .presvideo_thumb > div
        {
            width:30%;
        }
        .poFtRight .ftr_menu > .menu > li:nth-child(2n+1)
        {
            width: 50%;
        }
         .gov_forum_box
        {
        width:112%;
        }
        .inner_header.poffice .mstopRight
        {
            margin-top:0px;
        }
        .img_center {
            margin-bottom: 0px;
            margin-top: 12px;
            width: 240px;
            height: 50px;
        }
    }
    @media only screen and (min-device-width : 768px)  and (max-device-width : 1024px)  and (orientation : portrait)
    {
        .poffice_header_bottom 
         {
            display: none !important;
         }
         .inner_header.poffice .mstopRight
         {
            width:375px;
         } 
         .img_center {
            margin-bottom: 0px;
            margin-top: 12px;
            width: 240px;
            height: 50px;
         }
          .inner_header.poffice .mstopRight {
                width: 240px;
         }
         .inner_header.poffice .logo
         {
            margin: 4%;
         }
         .president-office .container .po_content > .category-president-blog .po_right {
            width: 80%;
            float: left;
        }
        .rtbs .rtbs_menu li a.active {
        position: relative;
        color: #5c2568 !important;
        border-bottom: none !important;
        }
        .rtbs>.rtbs_content
            {
                color: #e1b346 !important;
                
            }
        .rtbs .rtbs_menu li a.active {
            position: relative;
            color: #5c2568 !important;
            border-bottom: none !important;
            }
            .rtbs_full .rtbs_menu li.mobile_toggle
            {
                color:#5c2568 !important;
            }
            .rtbs_full .rtbs_menu ul li
            {
                display: block;
            }
            .president-office .container .po_content a {
                border-bottom: none;
            }
            .presvideo_thumb > div
            {
                  width: 46%;
            }
            .presvideo_thumb h4
            {
                padding-right:4px;
                padding-left: 4px;
            }
            .poBogItem {
                width: 100%;
            }
            .gov_forum_div {
                width: 40%;
            }
            .poGrid .pogridItem:nth-child(odd)
            {
                width:100%
            }
             .poGrid .pogridItem:nth-child(even)
            {
                width:100%
            }
            .poBogItem img {
                 width: 95%; 
                }
            .poGrid .pogridItem
            {
                margin-left:0%;
            }
           
            img.alignleft, img.alignright, img.aligncenter {
                margin-bottom: 32px;
                float: left;
                padding-right: 10px;
            }
            .po_content a {
                color: #48294c !important;
            }
            #tabs.ui-tabs .ui-tabs-nav li
            {
                margin-top:5px;
            }
            div#tabs-4 li {
                text-align: left;
            }
            .right_btn {
              float: left;
                 margin-left: 18px;
            }
            .left_btn {
                margin-left: 18px; 
            }
            .president-office .container h1 {
                 font-size: 20px;
            }
            .poFtRight .ftr_menu > .menu > li:nth-child(2n+1)
            {
                width: 50%;
            }
            #wrapper .mstopRight .search {
                width: 229px;
                margin-top: 0px;
                margin-bottom: 16px;
            }
            #wrapper .mstopRight .search{
                border:none;
                box-shadow:none;
            }
            
         
    }
    
    /* ##Device = Low Resolution Tablets, Mobiles (Landscape)  ##Screen = B/w 481px to 767px */
    
    @media (min-width: 481px) and (max-width: 767px) {
      
         .poffice_header_bottom 
         {
            display: none !important;
         }
        .gov_forum_div 
         {
        width: 26%;
        }
        
      
    }
    
    /* 
      ##Device = Most of the Smartphones Mobiles (Portrait)
      ##Screen = B/w 320px to 479px
    */
    
    @media (min-width: 320px) and (max-width: 480px) {
      
      
            .p_int_img
            {
                    float: none;
                    padding-left: 0px;
            }
    
            .po_content img {
                    width: 350px;
                    object-fit: cover;
                }
            
            .president-office.container.sideBar
            {
                width: 100%;
                position: relative;
                padding-bottom: 45px;
            }
            
            .president-office .container .po_content {
                float: left;
                width: 92%;
                font-size: 14px;
                font-weight: normal;
                line-height: 24px;
            }
            
            #footer.po-footer .inner_footer:before {
                visibility: hidden;
            }
            
            #footer.po-footer .poFtLeft, #footer.po-footer .poFtRight {
                width: 100%;
                float: left;
                padding-top: 30px;
            }
            
            .poFtRight .ftr_menu > .menu{
                width: 50%;
                display: inline;    
                float: none;
                
            }
            
            .poFtRight .ftr_menu > .menu > li
            {
                 margin-bottom: 10px;   
            }
            
            .poFtRight .ftr_menu > .menu > li {
            margin-bottom: 10px;
            margin-left: 30%;
            }
    
            .inner_footer {
                flex-direction: column;
                display: flex;
            }
            
            .poFtLeft{
            order:2;
             margin-left: 30px;
            }
            
            .poftright{
            order:1;
            }
            
            #wrapper .mstopRight .search {
            float: none;
            width: 100%;
            margin-bottom: 0;
            }
          
          /* 
          Header Section, Menus, Logo -- Section Start 
            */
             .inner_header
            {
                min-height: 20px;
            }
            
            .inner_header.poffice .logo {
             margin: 50px auto 0;
             position: relative;
             left: auto;
             width: 300px;
             float: none;
            }
            .quickLinkBox
            {
            display:none;
            }
            
            .inner_header.poffice .mstopRight{
             margin-top:45px;   
            }
            
            .president-office .container .sideBar:after{
                    content: '';
                    position: absolute;
                    background: url(<?=get_template_directory_uri()?>/images/presidentOffice/arrow_bottom.jpg) 0 0 no-repeat;
                    width: 100%;
                    height: 55px;
                    bottom: 15px;
            }
            .president-office .container .sideBar{
                float: none;
                position: relative;
                padding-bottom: 45px;
                overflow:hidden;
                margin-left: 4%;
            }
            .poffice_header_bottom{
                overflow: hidden;
            }
             .poffice_header_bottom_wellness{
                overflow: hidden;
            }
            
            .poffice_header_bottom{
                display: none;
            }
               
          .menu-toggle-button{
            display:block;
            width: 100%;
            }
            
            #wrapper #po_searchform input[type="text"]:focus{
             width: 90%;
            }
            
            .poffice_header_bottom .po_menu > .menu > li a
            {
                font-size: 12px;
            }
            .poffice_header_bottom_wellness .po_menu > .menu > li a
            {
                font-size: 12px;
            }
            .poffice_header_bottom .po_menu > .menu .sub-menu li
            {
                background-color: #360641;
                display: block;
                border-bottom: 1px solid #551f61;
                padding: 0px;
                margin-top: -7%;
                margin-left: 3%;
                width: 38.333%;
            }
            .poffice_header_bottom_wellness .po_menu > .menu .sub-menu li
            {
                background-color: #360641;
                display: block;
                border-bottom: 1px solid #551f61;
                padding: 0px;
                margin-top: -7%;
                margin-left: 3%;
                width: 38.333%;
            }
            #menu-item-14373
            {
                padding-left: 0px;
            }
            .poffice_header_bottom .po_menu{
               
            }
           .poffice_header_bottom {
               // display: none;
            }
            
            /* 
          Header Section, Menus, Logo -- Section End 
            */
            
            
            
            /* 
          HBKP president-profile/ page -- Section Start 
            */
           
           
           ul#menu-president-office-top {
            margin: 0 auto;
            padding-left: 20px;
            }
            
            .po_content h1{
            text-align: center;
            }
            
            .bogList {
             display: block;
             padding-left: 14%;
            }
            
            
            /* 
          HBKP president-profile/ page -- Section End 
            */
            
             /* 
          HBKP governance-forums/ -- Section Start 
            */
            .gov_forum_div
          {
            width: 85%;
            margin-bottom: 0px;
            margin-left: 2%;
            margin-right: 15%;
          }
          
            .gov_forum_box {
                width:100%;
                overflow: hidden;
                float: none;
                margin-left: 6%;
            }
            
            .president-office .container h1 {
                font-size: 18px;
                text-align: left;
            }
            p.contact_af {
                margin-left: 6%;
            }
             p.contact_af_2 {
                margin-left: 6%;
            }
    
               /* 
          HBKP governance-forums/ -- Section End 
            */
            
             /* 
          HBKP Media/ -- Section Start 
            */
            .presvideo_thumb > div
            {
            width: 100%;
            }
            .media_img_div
            {
             margin-left: 8%;
            }       
           
            /* 
          HBKP Media/ -- Section End 
            */
            
             /* 
          HBKP BLOG/ -- Section Start 
            */
            
            .president-office .container .po_content > .category-president-blog .po_right
            {
                margin-left: 5%;
                width:100%;
                margin-top: 5%;
            }
            .president-office .container .po_content > .category-president-blog .po_left
            {
                margin-left: 6%;
            }
            
             /* 
          HBKP BLOG/ -- Section Start 
            */
            
            
             /* HBKP ADC/ -- Section Start */
            .poGrid .pogridItem:nth-child(odd)
            {
                width:100%;
                margin-left: 8px;
            }
             .poGrid .pogridItem:nth-child(even)
            {
                width:100%;
            }
            .poGrid
            {
                margin-left: -5px;
            }
            .left_btn {
                float: none;
                margin-left: 0px;
            }
            .right_btn {
                float: none;
                margin-left: 0px;
            }
              /* HBKP ADC/ -- Section End */
    
             /* HBKP meeting-and-minutes/ -- Section Start */
            .huTable tr td {
                font-size: 15px;
                padding: 0px;
                text-align: center;
            }
             /* HBKP meeting-and-minutes/ -- Section End */
            
             p.g_f_para {
                margin-bottom: 10px;
            }
             /* HBKP leadership-/ -- Section Start */
             .alignright, img.alignright {
                margin-left: 14%;
                display: block;
                float:none;
            }
              /* HBKP leadership-/ -- Section End */
              
             
              /* HBKP OGE-Home-/ HasanAli-- Section Start */
             
          .owl-carousel.owl-loaded.owl-drag {
                width: 76%;
                padding: 0px;
            }
            
            .owl-image {
                width: 25%;
            }
            
            .ogeSection .testimonial .owl-item blockquote
            {
                width: 29%;
            }
            
            .mw_oge_mid #contentPart
            {
                margin-left: 7.4%;
            }
           .custom_spot_map aside {
                left: 135px;
            }
            .custom_spot_map img {
                height: 170px;
            }
            .custom_map_box
            {
                width:88%;
                padding: 22px 44px 5px 10px;
            }
            .oge_cal
            {
                float: none;
                height: 170px;
                padding: 25px 17px;
            }
            .custom_spot_map .cspot {
                padding-left: 4%;
            }
            .owl-image {
                width: 100% !important;
            }
            .ogeSection .testimonial .owl-item blockquote {
                width: 82%  !important;
                display: block  !important;
                overflow: hidden;
                text-align: center;
                padding-top: 8px;
            }
            .owl-nav {
                margin-top: -20px;
                width: 100%;
            }
            .ogeSection .testimonial {
                padding-left: 24px;
            }
            .owl-carousel.owl-drag .owl-item .item {
                display: block  !important;
                width: 55%  !important;
            }
          
             img.alignleft, img.alignright, img.aligncenter {
                 margin-bottom: 0px; 
            }
            .alignleft, img.alignleft {
                display: block;
                float: none;
            }
            .page-id-7546 .alignleft, img.alignleft {
                padding-bottom: 20px !important;
            }
            .alignleft, img.alignleft {
                
            }    
                     
          
             
            #main.oge .mw_header_top {
                display: none;
            }
            .vav_wrap_oge {
              //  display: none;
            }
            
            .oge_banner h1 
            {
                color: #fff;
                background-color: #b01d1d;
                padding-bottom: 15px;
                padding-top: 15px;
            }
            .oge_banner img {
                display: none; 
            }
           
            .mw_oge_mid.innerpage #contentPart
            {
                width: 75%;
                padding-left: 8px;
                display: block;
                
            }
            .oge_banner
            {
                min-height: 80px;
            }
            nav#nav-mobile {
                margin-top: 20px;
            }
           
           .mw_oge_mid #primary
           {
            margin: 0 auto;
            padding: 0px;
           }
             /* HBKP OGE-Home-/ HasanAli-- Section End */
             
            /* HBKP SSE-/ -- Section Start */
            
            .rightSide {
                width: 100%;
            }
            .rightSide #primary
            {
                float:left;
            }
            #container.temTwo #content
            {
                margin: 0 auto;
                width: 100%;
            }
            .leftSide {
                float: left;
                width: 100%;
                max-width: 840px;
            }
            .leftSide #tabs.ui-tabs .ui-tabs-panel {
                padding-left: 10%;
                padding-left: 2%;
                padding-right: 20%;
            }
            #container.temTwo #content
            {
                padding: 0px;
            }
            #content .leftSide .designationInfo
            {
                padding-left: 6px;
            }
            .leftSide #tabs.ui-widget.ui-widget-content
            {
                width: 130%;
                display: block;
                overflow: hidden;
            }
            .leftSide #tabs > ul > li {
                width: 75%;
            }
            #tabs.ui-tabs .ui-tabs-nav li.plus {
                display: none;
            }
            .facultyImg img {
                margin-left: 20%;
            }
            .facultyImg {
                display: block;
            }
            #content .leftSide .designationInfo {
            margin-left: 0px;
            padding-left: 0px;
            border-left: none;
            }
            /* HBKP SSE-/ -- Section End */
            .rtbs_full .rtbs_menu ul li {
                display: none;
                padding-left: 0px;
                background: #5c2568;
            }
            .rtbs_full .rtbs_menu li.mobile_toggle
            {
                background: #5c2568;
            }
            .president-office .container .po_content a
            {
                border-bottom:none;
            }
            div#images-tab-0 {
                border-top: 2px solid !important;
            }
            .presvideo_thumb {
                display: block;
            }
            .fg-panel .fg-thumbail-selection {
                margin-left: 5%; 
            }
            
            /* Wellness-center - start-------------MobileResp.------------ */
            .wc_mleft {
                   width: 85%;
                }
                .wc_mright {
                    float: none;
                    width: 85%;
                    margin-bottom: 0px;
                }
                .wc_list ul li {
                    width: 100%;
                    float: right;
                    text-align: center;
                }
                .poffice_header_bottom .po_menu > .menu
                {
                    padding-left: 4%;
                }
                .wc_list {
                    width: 100%;
                }
                .page-template-page-wellness-center-inner .wc_left_content
                {
                    width:95%;
                }
                .page-template-page-wellness-center-inner .wc_left_content p {
                   // width: 86%;
                    padding-left: 0px;
                }
                .phySidebar.wellnessSidebar
                {
                    display:block;
                    width:100%;
                }
                .para_wc_01
                {
                    padding-left: 62px !important;
                }
                .wc_sectionTwo {
                    display: none;
                }
                .page-template-page-wellness-center-inner .wc_left_content {
                    padding: 5%;
                }
                .phySidebar.wellnessSidebar
                {
                    transform:none;
                    max-width: 90%;
                }
                .topBg h1
                {
                font-size:22px;
                }
                .mw_wce_mid .inner_wraper {
                    width: 100%;
                }
                .poffice_header_bottom_wellness
                {
                    display:block;
                }
                .icon-boxes .mbox {
                    width: 80%;
                    height: 50px;
                }
     /* Wellness-center - End----------MobileResp.--------------- */
      /* http://bkp.habib.edu.pk/academics/sse/faculty/ - Start----------MobileResp.---------------11jun 2018 | Hassan */           
                .facultyBox {
                    margin: 0 auto;
                    width: 280px;
                }
      /* http://bkp.habib.edu.pk/academics/sse/faculty/ - End----------MobileResp.--------------- */          
    }

</style>
    <div class="container">
        <div class="po_content">
			<?php	
			if ( have_posts() ) :
            	while (have_posts()) : the_post();
					the_content();
            	endwhile;
			endif; ?>
         
        </div>
        
        <div class="sideBar">
           <?php get_sidebar('president-office'); ?>
        </div>
        
    </div>
	

<?php get_footer('president-office'); ?>
