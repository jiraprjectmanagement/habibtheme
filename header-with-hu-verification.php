﻿<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>


<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });

		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');

</script>

</head>


<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>

<body <?php body_class(); ?>>
<?php /* if(is_front_page()) { ?>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/backstretch/jquery.backstretch.min.js"></script>
	<script>
$(function () {
var items = [
<?php

			  query_posts(array(
			 'post_type'      => 'home-slider', // You can add a custom post type if you like
			 'posts_per_page' => 400,
			 'end_size'=> 1,
			 'order' => 'DESC'
			  ));
			 if ( have_posts() ) :
			 while (have_posts()) : the_post(); 
//$rgb = hex2rgba('#'.get_post_meta($post->ID, 'color', true));
$rgba = hex2rgba('#'.get_post_meta(get_the_ID(), 'color', true), 0.9);
			 
			 echo "{img: '".wp_get_attachment_url( get_post_thumbnail_id($post->ID))."', caption: '".get_the_title()."', color: '".get_post_meta($post->ID, 'color', true)."', textcolor: '".get_post_meta($post->ID, 'textcolor', true)."', tagline: '".get_post_meta($post->ID, 'tagline', true)."', links: '".get_post_meta($post->ID, 'link', true)."', },";
			 ?>
			
          <?php endwhile; endif; wp_reset_postdata(); ?>
]
	
            var options = {fade: 500, duration: 5500};

            var images = $.map(items, function(i) { return i.img; });

            // Start Backstretch, and save a reference to it.
            var slideshow = $.backstretch(images,options);

            $(window).on("backstretch.show", function(e, instance) {
                var newCaption = items[instance.index].caption;
				 var newTagline = items[instance.index].tagline;
				  var newColor = items[instance.index].color;
				 	var textcolor = items[instance.index].textcolor;
					var links = items[instance.index].links;
    function hex2rgb(hex, opacity) {
    var rgb = hex.replace('#', '').match(/(.{2})/g);
     
    var i = 3;
    while (i--) {
    rgb[i] = parseInt(rgb[i], 16);
    }
     
    if (typeof opacity == 'undefined') {
    return 'rgb(' + rgb.join(', ') + ')';
    }
     
    return 'rgba(' + rgb.join(', ') + ', ' + opacity + ')';
    };				 
				 $(".home .home_tag").css('background-color', hex2rgb('#'+newColor, 0.9));
				 $(".home_tag a").attr('href',links);
				 
                $(".home_tag h1").text( newCaption ); $(".home_tag h1").css('color','#'+textcolor);
				$(".home_tag span").text( newTagline ); $(".home_tag span").css( 'color', '#'+textcolor );
            });
});	
   
    </script>
<?php }

	else */ if (is_page())  { ?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/backstretch/jquery.backstretch.min.js"></script>
	<script>
        $.backstretch([
		
			 
          "<?php echo  wp_get_attachment_url( get_post_thumbnail_id($post->ID));?>",
		  
        ],  {
            fade: 750,
            duration: 4000,
			transition :'fade',
			transition_speed:500, 
			navigation : 1,
			fit_portrait: 0,
			fit_landscape0:0,
			slide_captions: 1,
			slide_counter :1
        });
 
    	$.backstretch("<?php //echo $url; ?>");
    </script>

	<?php } else { ?>
    	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/backstretch/jquery.backstretch.min.js"></script>
	<script>
        $.backstretch([
		
			 
          "<?php echo  get_post_meta($post->ID, 'customimage', true);?>",
		  
        ],  {
            fade: 750,
            duration: 4000,
			transition :'fade',
			transition_speed:500, 
			navigation : 1,
			fit_portrait: 0,
			fit_landscape0:0,
			slide_captions: 1,
			slide_counter :1
        });
 
    	$.backstretch("<?php //echo $url; ?>");
    </script>
    
    <?php } ?>
	
	
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
               
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"></a>
                </div>
                <div id="nav-trigger">
                        <div>MENU <span class="slicknav_icon"><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span><span class="slicknav_icon-bar"></span></span></div>
                </div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav' ) ); ?>
                <div class="search">
                    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div>
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <input type="text" value="<?php echo get_search_query(); ?>Search" name="s" id="s" />
                            <input type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search" />
                        </div>
                    </form>
                </div>
                </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i>---</i>≡</div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
	</div><!-- #header -->

	<div id="main">
           
            	
      