﻿<?php
/**
 * Header template for Library
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/oge.css" />
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/ogehasan.css" /> 
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
<meta property="og:url" content="<?php echo get_permalink($post->ID); ?>"/>
<meta property="og:title" content="<?php echo $post->post_title; ?>"/>
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/tab.min.css" />
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css">

<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet"/>
<link href="<?php //echo esc_url( get_template_directory_uri() ); ?>/css/lity.css" rel="stylesheet"/>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tab.min.js"></script>

<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css">
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>
<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        $(".mob-std-life #nav-mobile").html($("#nav-main").html());
        $(".mob-std-life #nav-trigger span").click(function(){
            if ($(".mob-std-life nav#nav-mobile ul").hasClass("expanded")) {
                $(".mob-std-life nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".mob-std-life nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
    
</script> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
document.getElementsByClassName("defaultOpen").click();
</script>
<?php /*?><script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "http://www.habib.edu.pk/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://www.habib.edu.pk/?s={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script><?php */?>

</head>
<body <?php body_class(); ?>>
	
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                <?php if(is_front_page()) : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/>
                <?php else : ?>
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"/>
                <?php endif; ?>
                </a>
                </div>
                <div id="nav-trigger" class="fas fa-bars">
                        
                </div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
                        <div class="nav-mid"><?php wp_nav_menu( array('theme_location' => 'mid_nav' ) ); ?></div> 
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav_inner' ) ); ?>
                
      
                <div class="search">
                	<div class="search_mid_nav"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'mid_nav' ) ); ?> </div>
                     <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                       
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" id="s"/>
                            <button type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search"/><i class="fa fa-search"></i></button>
                    </form>                    
                </div>
                </div>
            </div><!--.inner_head-->
            
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU <i class="fas fa-bars"></i></div>
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                    <nav id="nav-mobile">
                    	<?php //wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
                    </nav>
                 </div>   
        	</div>
	</div><!-- #header -->
	<div id="main" class="oge">
    

    	
     