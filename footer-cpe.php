<?php
/**
 * Template for displaying the footer on cpe website
 *
 */
?>
</div>
<!-- #main -->
</div>
<!--.inner_wrap-->
<div id="footercpe" role="contentinfo">

  
  <div class="inner_footercpe">
        <img src="/wp-content/themes/habib/css/images/icon.png"/>
        <div class="footer_left">
        <p>CPE (Center for Pedagogy Experience)</br>
        <a href="mailto:cpe@habib.edu.pk">cpe@habib.edu.pk</a></p>
        </div>
        <div class="footer_right">
            <p><a href="/contact-us/" style="color:#fff !important;">Contact Us </a>| Phone: +92 21 1110 42242 (HABIB)</br>
              	&copy; Habib University - All Rights Reserved | <a href="/privacy-statement/" target="_blank" style="color:#62276f !important;">Powered by HU-IT</a></p>
        </div>
  </div>

</div>
<!-- #footer -->
</div>
<!-- #wrapper -->


</body></html>
