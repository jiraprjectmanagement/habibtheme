//https://habib.edu.pk/playground/resources/  Slider of resources...
jQuery(document).ready(function() {
	"use strict";
  jQuery('.owl-carousel').owlCarousel({
    items:3,
    loop: true,
    margin: 20,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      1000: {
        items:3,
        nav: true,
        loop: true,
        margin: 20
      }
    }
  });
	
	
	
	jQuery.scrollify({
		section : ".panel",
//		sectionName:false,
		interstitialSection:"#header, #footer"
	});
	
});


jQuery('.twoSection .col4 .eventItem').waypoint(function(direction) {
  jQuery('.twoSection .col4 .eventItem').addClass('animated').addClass('fadeInRight');
}, {
  offset: '90%'
});

jQuery('.borderBlack').waypoint(function(direction) {
  jQuery('.borderBlack').addClass('addBorder');
}, {
  offset: '80%'
});

jQuery('.myTitle.top').waypoint(function(direction) {
  jQuery(this.element).addClass('addAnimation');
//  console.log(this.element);
}, {
  offset: '87%'
});
jQuery('.myTitle.bottom').waypoint(function(direction) {
  jQuery(this.element).addClass('addAnimation');
//  console.log(this.element);
}, {
  offset: '87%'
});


var waypoints = jQuery('#inner').waypoint(function(direction) {
  
  console.log(this.element.id + ' hit 25% from '+ direction +' of window'); 
  if(direction == 'up') {
    console.log("up up");
//    jQuery('#secondHeader').removeClass('animated').removeClass('fadeInDown').hide();
    jQuery('#secondHeader').fadeOut('fast').removeClass('animated').removeClass('fadeInDown');
  }
  if(direction == 'down') {
    console.log("down down");
    jQuery('#secondHeader').fadeIn('fast').addClass('animated').addClass('fadeInDown');
    setTimeout(function(){
      jQuery('#secondHeader').removeClass('animated').removeClass('fadeInDown');
    }, 5000);
  }
});


/*
(function($){
    $.fn.backgroundMove=function(options){
            var defaults={
            movementStrength:'50'
        },
        options=$.extend(defaults,options);

         var $this = $(this);

           var movementStrength = options.movementStrength;
            var height = movementStrength / $(window).height();
            var width = movementStrength / $(window).width();
            $this.mousemove(function(e){
                      var pageX = e.pageX - ($(window).width() / 2);
                      var pageY = e.pageY - ($(window).height() / 2);
                      var newvalueX = width * pageX * -1 - 25;
                      var newvalueY = height * pageY * -1 - 50;
                      $this.css("background-position", newvalueX+"px     "+newvalueY+"px");
            });

        }
})(jQuery);
*/


// jQuery('body').backgroundMove({movementStrength:'50'});


var userAgent, ieReg, ie;
userAgent = window.navigator.userAgent;
ieReg = /msie|Trident.*rv[ :]*11\./gi;
ie = ieReg.test(userAgent);

if(ie) {
  $(".img-container").each(function () {
    var $container = $(this),
        imgUrl = $container.find("img").prop("src");
    if (imgUrl) {
      $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
    }
  });
}


jQuery(document).on('lity:ready', function(event, $lightbox, $triggeringElement) {
    var title = jQuery(this).attr('data-title');
    var desc = jQuery(this).attr('data-desc');
    jQuery('.lity-content').append('<h2>' + title + '</h2><p>' + desc + '</p>');
});