<?php
/*
 Template Name: Home page 
*/



get_header(); ?>
<!-------------------------------------------------- -------------------------------------------------->
<!-- if needed -->
   <style>
 
 
#mask {
  position:absolute;
  left:0;
  top:0;
  z-index:9000;
  background-color:#000;
  display:none;
}  
#boxes .window {
  position:absolute;
  left:0;
  top:0;
  width:440px;
  height:200px;
  display:none;
  z-index:9999;
  padding:20px;
}
#boxes #dialog {
  width: 400px;
  height: auto;
  padding: 10px 30px;
  background-color: #ffffff;
}
.close:hover {
 opacity:0.8;
}
.close {
  background: #3ca7df;
  padding: 10px 15px;
  color: #fff !important;
  float: left;
  margin-left: 83px;
  margin-top: -10px;
  font-size: 18px;
  font-weight: normal;
  opacity: 1;
 
}
</style>

<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/popup/jquery-1.10.1.min.js"></script>
<!-- DOWNLOAD file from here
<script type="text/javascript" src="http://omanassessments.com/oa/demo/wp-content/themes/Oman-Assessments/js/jquery-1.10.1.min.js"></script>
-->
	<!-- Add mousewheel plugin (this is optional) -->
    
    	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/popup/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- DOWNLOAD FROM HERE
	<script type="text/javascript" src="http://omanassessments.com/oa/demo/wp-content/themes/Oman-Assessments/js/jquery.mousewheel-3.0.6.pack.js"></script>
 -->
 
 
	<!-- Add fancyBox main JS and CSS files -->
	
	<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/popup/jquery.fancybox.js?v=2.1.5"></script>
	<!-- Download from here 
    <script type="text/javascript" src="http://omanassessments.com/oa/demo/wp-content/themes/Oman-Assessments/js/jquery.fancybox.js?v=2.1.5"></script>
    -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/popup/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- Download from here 
	<link rel="stylesheet" type="text/css" href="http://omanassessments.com/oa/demo/wp-content/themes/Oman-Assessments/js/jquery.fancybox.css?v=2.1.5" media="screen" />
    -->
    
    <script type="text/javascript">
$(document).ready(function() {	

		var id = '#dialog';
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000); 	
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});		
	
});

</script>

<div id="boxes">
<div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
<h3>Admissions for Fall 2015 are open</h3>
<p>Apply now on <a href="http://eapplication.habib.edu.pk/" target="_blank">eapplication.habib.edu.pk</a> or visit <a href="http://habib.edu.pk/academics" target="_blank">www.habib.edu.pk/academics</a> to find out more about our undergraduate programs.</p>
<a class="close btn btn-primary custom-button orange-btn">Close</a>
</div>
<!-- Mask to cover the whole screen -->
<div style="width: 1478px; height: 602px; display: none; opacity: 0.8;" id="mask"></div>

		</div>


<!------------------------------------------- ------------------------------------------------>


		<div class="mw_home_tag">
    		<div class="inner_wraper">
<?php /*?> <?php

			  query_posts(array(
			 'post_type'      => 'home-slider', // You can add a custom post type if you like
			 'posts_per_page' => 1,
			 'end_size'=> 1,
			 'order' => 'DESC'
			  ));
			 if ( have_posts() ) :
			 while (have_posts()) : the_post(); 
?>			            
              <a href="<?php the_permalink(); ?>">	
      			<h1><?php the_title(); ?></h1>
      			<span><?php echo get_post_meta($post->ID, 'tagline', true); ?></span>
              </a>  
			  
 <?php endwhile; endif; wp_reset_postdata(); ?>		<?php */?>	  
 			<?php  if ( have_posts() ) :
			 while (have_posts()) : the_post();
			 	the_content();
			 endwhile; endif; ?>

			</div>
  		</div>
		
		<div class="full">
        	<?php /*?><span class="leftColor">&nbsp;</span>
            <span class="rightColor">&nbsp;</span><?php */?>
			
				<?php if ( is_active_sidebar( 'home-widget-area' ) ) : ?>
					<ul class="home-tabs">
						<?php dynamic_sidebar( 'home-widget-area' ); ?>
					</ul>
				<?php endif; ?>
		</div>
		
		<div class="full clearfix">
			<div class="inner_wraper">
				<?php /*?><!-- <ul id="stats">
					<li>
						<span class="nums">92%</span>
						<p>OF STUDENTS <br>
						RECIEVED FINANCIAL <br>
						AID AND ASSISTANCE</p>
					</li>
					<li>
						<span class="nums">RS. 25M</span>
						<p>HIGHLY SUBSIDIZED <br>
						LOANS</p>
					</li>
					<li>
						<span class="nums">8</span>
						<p>80% FINANCIAL AID <br>
						SCHOLARSHIPS</p>
					</li>
					<li>
						<span class="nums">5</span>
						<p>100% YOHSIN <br>
						SCHOLARSHIPS</p>
					</li>
					<li>
					<a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>"><img src="<?php echo bloginfo(		 					'template_url'); ?>/images/campus.jpg">
					<span class="yellow">CAMPUS</span></a>
				</li>
				</ul>
				<span class="tag-line">FINANCIAL AID AND SCHOLARSHIP PROGRAMS</span>
                --><?php */?>
                <?php if ( is_active_sidebar( 'home2-widget-area' ) ) : ?>
                	<div class="blue-bg">
                		<?php dynamic_sidebar( 'home2-widget-area' ); ?>
                	</div>
				<?php endif; ?>
			</div>	
		</div>
		
			<div class="full grey">
				<div id="home-widget">
					<div id="hw-1">
                    <?php global $theme_options; 
					// print_r($theme_options); ?>
                    
						<div class="footer_social">
							<ul>
                                <li class="li"><a href="<?php echo $theme_options['linkedin_url']; ?>" title="LinkedIn" target="_blank">linked in</a></li>
                                <li class="tw"><a href="<?php echo $theme_options['tw_url']; ?>" title="Twitter" target="_blank">Twitter</a></li>
                                <li class="vm"><a href="<?php echo $theme_options['vimeo_url']; ?>" title="Vimeo" target="_blank">viemo</a></li>
                                <li class="fb"><a href="<?php echo $theme_options['fb_url']; ?>" title="Facebook" target="_blank">facebook</a></li>
                                <li class="tm"><a href="<?php echo $theme_options['instagram_url']; ?>" title="Instagram" target="_blank">tumblr</a></li>
							</ul>
                            <span class="red">SOCIAL MEDIA</span>
                         </div>
						
					</div>
					<div id="hw-2">
						<?php if ( ! dynamic_sidebar( 'Second Home Widget Area' ) ) : ?>
        				<?php endif; ?>
					</div>
					<div id="hw-3">
						<?php if ( ! dynamic_sidebar( 'Third Home Widget Area' ) ) : ?>
        				<?php endif; ?>
					</div>
				</div>
			</div>
			
		

<?php get_footer(); ?>

