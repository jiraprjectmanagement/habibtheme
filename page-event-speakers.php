<?php
/*
 Template Name: Main Speaker 
*/

get_header('ycsd'); ?>
    
    <?php /*?><div class="mw_ycsd_menu">
    	<?php wp_nav_menu( array('container_class' => 'ycsdMenu', 'theme_location' => 'ycsd_nav' ) ); ?>
    </div><?php */?>

    <div class="mw_ycsd_slider">
    	<?php echo do_shortcode(get_field('rev_slider')); ?>
    </div>

    <div class="mw_ycsd_mid">
        <div class="inner_wraper">
        	<?php
            $taxonomy = 'speakers';
            $tax_terms = get_terms($taxonomy);
            ?>
            <h2>Speakers</h2>		        				
            <div class="mwResearchStreams">
            	
                <?php 
					foreach ($tax_terms as $tax_term) { ?>
                    <?php /*?><!-- <?php // var_dump($tax_term); ?> --><?php */?>
                    <article class="ycsdBox speakers">
                    <?php if( get_field('thumbnail', $tax_term ) ) { ?>
                        <a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><img src="<?php the_field('thumbnail', $tax_term ); ?>"  class="top" width="166" height="166" /></a>
                    <?php } ?>    
                        <div class="ycsdInner">
                            <h4 class="ycsdTitle"><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></h4>
                            <?php if(get_field('designation', $tax_term)) { ?>
                            <p class="ycsdText"><?php the_field('designation', $tax_term); ?></p>
                            <?php } ?>
							<?php if(get_field('watchvideo', $tax_term)) { ?>
                            <a class="speakerWatchVideo watchVideo" href="<?php the_field('watchvideo', $tax_term); ?>" rel="wp-video-lightbox"><i class="fa fa-play-circle-o"></i> Watch Lecture</a>
                            <?php } ?>
                        </div>
                    </article>
            <?php  	}
					wp_reset_query();
			?>
			
            	<?php 
				
				?>
            
            </div>
            
			<?php  if ( have_posts() ) :
             while (have_posts()) : the_post();
                // the_content();
             endwhile; endif; ?>
         
        </div>
        
        <div class="ycsdSideBar">
        	<aside id="yscdCalender">
            <?php
				$events = EM_Events::get(array('scope'=>'month', 'category'=> 'ycsd', 'limit'=>3, ));
				# if(!empty($events)) {
				if($events) {	
			?>	
                <h2>Calendar</h2>
                <div class="mwCalendar">
                    <h3><?php echo date('F'); ?></h3>
                    <?php
					
                        foreach( $events as $EM_Event ){ ?>
                            <article class="ycsdRow">
                                <div class="mwDate"><?php echo $EM_Event->output("#_{j}"); ?><sup><?php echo $EM_Event->output("#_{S}"); ?></sup></div>
                                <h4 class="ycsdTitle"><?php echo $EM_Event->output("#_EVENTLINK"); ?></h4>
                                <?php 
									$eventID = $EM_Event->output("#_EVENTPOSTID");
									echo get_the_term_list( $eventID, 'speakers', '<h4 class="ycsdTitle speaker"><strong>Speakers:</strong> ', ', ', '</h4>' ); 
								?>
                            </article>
                    <?php
                        }
					?>
                </div>
                <?php
				} else {
				//	echo '<p style="color: #fff; padding: 15px;">Sorry No Event Found</p>';
				}
				?>
            </aside>
            <aside id="workingPaperSeries">
            <!--
<a href="http://10.100.0.157:8080/jspui/handle/123456789/2" target="_blank">
            	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/wps_icon.jpg" />
                <h2 class="icn_wps">Working Paper Series</h2>
            </a>    
            <p style="padding: 0px 12px 12px 12px; margin: 0; text-align: justify;">Working papers are designed to communicate ongoing research and to share key findings with a broader community. The Working Paper Series constitutes "work in progress." The series aims at assessing theoretical issues and offering critical insights to issues connected to development, social change and specific policies.</p>
           
--> </aside>
            <?php /*?><aside id="fromAhssBlog">
            	<h3>From AHSS Blog</h3>
                <div class="ahssBlogg">
                	<?php echo do_shortcode('[ahss]'); ?>            
                </div>
            </aside><?php */?>
            <aside id="yscdLectureVideos">
				<a href="https://vimeo.com/album/3152429" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/video_lecture_videos.jpg" /></a>
            </aside>
        </div>
        
    </div>
</div>
	

<?php get_footer(); ?>
