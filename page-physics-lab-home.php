<?php
/**
 * Template Name: Physics Lab Home
 *
 */

// get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

		<div id="container" class="phyLab">
			<div id="content" class="fullwidth"><?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<?php /*?><div class="topImg parallax-window" data-parallax="scroll" data-image-src="<?php echo $url; ?>">
					
					<img src="<?php echo $url; ?>" style="visibility: hidden;" />
					<div class="mbox">
						<h1><?php the_title(); ?></h1>
						<div class="subTitle"><?php the_field('sub_title'); ?></div>
					</div>
				</div><?php */?>

<!----------------------- Start bx slider --------------------------->
				<!-- <?php if( have_rows('physics_slider') ): ?>
				<div class="topImg" style="margin-bottom: 25px;">
					<div id="bxslider">
						<ul class="bxslider">
					  		<?php while( have_rows('physics_slider') ): the_row(); 

								// vars
								$image = get_sub_field('image');
								$text = get_sub_field('text');

								?>
								<li><img src="<?php echo $image; ?>" />
									<div class="mbox"><h1><?php echo $text; ?></h1></div></li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>
				<?php endif; ?> -->
<!----------------------- End bx slider --------------------------->

			<?php	if ( have_posts() ) :
						while (have_posts()) : the_post();
			?>
            				<!--<h1 class="title"><?php the_title(); ?></h1>-->
            				<div class="content">
								<?php the_content(); ?>
            				</div>
            <?php				
						endwhile;
					endif; 
			?>

			</div><!-- #content.fullwidth -->
		</div><!-- #container -->

<?php get_footer('footer-live'); ?>

<?php //get_footer(); ?>
