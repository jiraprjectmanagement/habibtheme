<?php
/*
 Template Name: CMD Events 
*/
//get_header(); ?>

<?php get_header('with-megamenu-live'); ?>

<!-- PAST EVENTS IDRAC | START-->
    <div class="mw_ycsd_slider">
   <?php echo do_shortcode('[rev_slider alias="cmd"]');?> 	
    </div>
    <div class="mw_ycsd_mid">
        <div class="inner_wraper">
        	<?php #echo do_shortcode('[events_list scope=\'all\' limit=10 pagination=1 ][/events_list]'); ?>
        <?php
			/*$args['format'] = "<B>#_EVENTNAME</B><br/>";
			$args['scope'] = 'all';
			$args['category'] = 'ycsd';
			$args['orderby'] = 'event_start_date';
			$args['limit'] = 9;
			$args['pagination'] = 1;
			// NO PAGINATION ARGS - USE THE DEFAULT BECAUSE THE DEFAULT WORKS
			echo EM_Events::output( $args );*/
		?>
        <?php
			$args = array('scope'=>'all', 'category'=> 'cmd', 'orderby'=>'event_start_date', 'limit'=>8, 'pagination'=> 1 );
			$args['page'] = (!empty($_REQUEST['pno']) && is_numeric($_REQUEST['pno']) )? $_REQUEST['pno'] : 1;
			$events = EM_Events::get($args);
			if(!empty($events)) {
		?>	
        	<h2>CMD Events </h2>
        	<div class="mwUpcomingEvents"><h2>CMD Events </h2>
            	<?php
					foreach( $events as $EM_Event ){ ?>
						<?php 
								$today = date("Ymd");
								$eventDate = $EM_Event->output("#_{Ymd}");
								if($today < $eventDate) {
									$eventClass = "futureEvent";
								} elseif($today > $eventDate) {
									$eventClass = "pastEvent";
								} elseif($today == $eventDate) {
									$eventClass = "todayEvent";
								}
							?>
                        <article class="ycsdBox <?php echo $eventClass; ?>">
                        	<a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>"><img class="idrac_article_img" src="<?php echo $EM_Event->output("#_EVENTIMAGEURL"); ?>" width="230" height="97" class="top"></a>
                            <div class="ycsdInner">
                            	<?php /*?><h5><i class="fa fa-calendar"></i> <?php echo "Date : ".$EM_Event->output("#_{d-M-Y}"); ?></h5><?php */?>
                                <?php /*?><h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo string_limit_words($EM_Event->output("#_EVENTNAME"), 10); ?></a></h4><?php */?>
                                <h4 class="ycsdTitle"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo $EM_Event->output("#_EVENTNAME"); ?></a></h4>
                                <p class="ycsdText"><?php echo string_limit_words($EM_Event->output("#_EVENTEXCERPT{10}"), 20).'...'; ?></p>
                                <a class="ycsdReadMore" href="<?php echo $EM_Event->output("#_EVENTURL"); ?>">Read More...</a>
                            </div>
                            <div class="ycsdBtn">
                             <?php 
                                   if($EM_Event->output('#_ATT{Album}')) {                                         
                                       ?>
                                      <link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/css/lity.min.css" />
                                       <a class="ycsdViewAlbum" href="#inline<?=$key?>" data-lity><i class="fa fa-image"></i>View Album</a>
                                       <div id="inline<?=$key?>" style="background:#fff" class="lity-hide">
                                           <?php 
                                               $shortcode = ''.$EM_Event->output('#_ATT{Album}').''; 
                                               echo do_shortcode( $shortcode );
                                           ?>
                                       </div>
                                       <?php
                                   } 
                               ?>
                            	<?php  if($EM_Event->output('#_ATT{Video url}') && $EM_Event->output('#_ATT{Video url}') != '#') { ?><a class="watchVideo"  href="<?php echo $EM_Event->output('#_ATT{Video url}'); ?>" rel="wp-video-lightbox"><i class="fa fa-play-circle-o"></i> Watch Video</a><?php } ?>
                                <?php if($EM_Event->output('#_ATT{Poster}')) { ?><a class="pressRelease" href="<?php echo $EM_Event->output('#_ATT{Poster}'); ?>" target="_blank"><i class="fa fa-file-text-o"></i> Poster</a><?php } ?>
                           </div>
                        </article>
				<?php
					}
					$events_count = EM_Events::count($args);
					echo EM_Events::get_pagination_links($args, $events_count);
				?>  
                
            </div>
            <?php } ?>
            
			<?php  if ( have_posts() ) :
             while (have_posts()) : the_post();
                // the_content();
             endwhile; endif; ?>
         
        </div>
        
        
        
    </div>
</div>
	

<!-- PAST EVENTS IDRAC | END-->

<?php get_footer('footer-live'); ?>

<?php// get_footer(); ?>
<?php if(wp_is_mobile()){ ?>
<script>
$('.mw_arzu_tag p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});
</script>
<?php } ?>
