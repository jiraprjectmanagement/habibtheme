﻿<?php
/**
 * Header template for our theme
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		echo ' | ' . sprintf( __( 'Page %s', 'habib' ), max( $paged, $page ) );
	?></title>
	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>


<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css"/>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css"/>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav.css"/>
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/student-government-style.css" /> 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>
<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>

<!-- bxSlider CSS file -->

<?php // } ?>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery.daisynav.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js"></script>
<script>	
	jQuery(document).ready(function($){
		$.daisyNav();
	});
</script>

<script src="https://habib.edu.pk/wp-content/themes/habib/js/owl.carousel.min.js"></script>
<link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/css/owl.carousel.min.css"/>
<style>
ul#menu-mid-menu>li>a {
    color: #fff;
}
</style>
</head>
<body <?php body_class(); ?>>
	
<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
    
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png">
                </a>
                </div>
                <div id="nav-trigger" class="fa fa-bars">
    
                </div>
                
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_nav' ) ); ?>
                
      
                <div class="search">
                <div class="search_mid_nav"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'mid_nav' ) ); ?> </div>
                	<?php
						
						//if ( is_user_logged_in() ) {
					?>
                    <?php		
						//}
						
					?>
                    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                       
                            <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
                            <input type="text" value="<?php echo get_search_query(); ?>" placeholder="Search" name="s" id="s"/>
                            <button type="submit" id="searchsubmit" value="<?php esc_attr_x( 'Search', 'submit button' ); ?>Search"/><i class="fa fa-search"></i></button>
                    </form>
                                     
                </div>
                   
              <nav id="nav-mobile">
                	<?php wp_nav_menu( array('theme_location' => 'top_nav' ) ); ?>
            </nav>
                </div>
            </div><!--.inner_head-->
         
            <div class="vav_wrap">
            <div  class="menu-toggle-button" data-menu-id="demo-menu">MENU  <i style="visibility: hidden;">---</i><i class="fa fa-bars"></i></div>
            
            <?php wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'primary','items_wrap' => '<ul class="menu-list" id="demo-menu"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
                    </div><!-- #access -->
                 </div>   
        	</div>
         
            
	</div><!-- #header -->
	<div id="main">
           
            	
<?php 
	if(!is_tax( 'speakers' ) && get_field('redirection')) {
			$mwlocation = get_field('redirection');
		header("Location: $mwlocation");
	} 
?>       