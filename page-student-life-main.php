<?php
/**
 * Template Name: Student Life Main
 *
 */

// get_header('small'); ?>

<?php get_header('with-megamenu-live'); ?>

		<div id="container" class="phyLab">
			
			<div id="stlife_content" class="innerLayout">
				
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
					if(!empty($url)) {
						$myurl = 'data-parallax="scroll" data-image-src="'.$url.'"';
					} else {
						$myurl = '';
					}
				?>
			  
				<?php if( have_rows('physics_slider') ): ?>
				
				
					<!-- <div class="topImg">
					<div id="bxslider">
						<ul class="bxslider">
					  		<?php while( have_rows('physics_slider') ): the_row(); 

								// vars
								$image = get_sub_field('image');
								$text = get_sub_field('text');

								?>
								<li>
									<img src="<?php echo $image; ?>" />
									<?php if(!empty($text)) { ?>
										<div class="mbox"><h1><?php echo $text; ?></h1></div>
									<?php } ?>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div> -->

				<?php add_revslider('student-life-main'); ?>

                	 <!-- <div id="nav-sl">	
                    <ul>
                    <li><a href="/student-life">About Student Life</a></li>
                    <li><a href="/student-life/life-events/">Life Events</a></li>
                    <li><a href="/student-life/student-leadership-and-mentoring-program/">Student Leadership and Mentoring Program</a></li>
                    <li><a href="/metacurricular/">Meta Curricular Transcript</a></li>
                    <li><a href="/student-life/senior-year-experience/">Senior Year Experience</a></li>
                    <li><a href="/student-life/student-organizations-and-clubs/">Clubs and Organizations</a></li>
                    <li><a href="https://habib.edu.pk/student-life/student-government/">Student Government</a></li>
                    <li><a href="/student-life/sense-of-community/">Sense of Community</a></li>
                    <li><a href="/student-life/student-resources/">Student Resources</a></li>
                    <li><a href="/student-life/week-of-welcome/">Week of Welcomes</a></li>
                    </ul>
                  </div> -->
                
           <div id="menu-sticky-bar" class="vav_wrap01 menu-sticky-bar">
               <!-- <div  class="menu-toggle-button" data-menu-id="demo-menu1">Browse Student Life <i style="visibility: hidden;">-</i><i class="fas fa-chevron-down"></i></div> -->
             <?php //wp_nav_menu( array(  'container_class' => 'res_menu', 'theme_location' => 'stdlifemenu','items_wrap' => '<ul class="menu-list" id="demo-menu1"><li id="item-id">Menu: </li>%3$s</ul>' ) ); ?>
            
            	<div class="inner_wraper">
                    <div id="access" role="navigation">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'stdlifemenu' ) ); ?>
                    </div><!-- #access -->      
                </div> 
            </div>
            
				<?php else: ?>
				
				<div class="topImg parallax-window" <?php echo $myurl; ?>>
					<?php if($url) { ?>
					<img src="<?php echo $url; ?>" />
					<?php } ?>
					<div class="mbox">
						<h1><?php the_title(); ?></h1>
						<div class="subTitle"><?php the_field('sub_title'); ?></div>
					</div>
				</div>
				<?php endif; ?>
			<?php	if ( have_posts() ) :
						while (have_posts()) : the_post();
			?>
            				<!--<h1 class="title"><?php the_title(); ?></h1>-->
            				<div class="content">
								<?php the_content(); ?>
            				</div>
            <?php				
						endwhile;
					endif; 
			?>
			
			</div><!-- #content.fullwidth -->
		</div><!-- #container -->
		<script>
jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()>600)
    {
    //   jQuery('.menu-sticky-bar').show(500);
	jQuery('.menu-sticky-bar').slideDown(500);
	jQuery("div#menu-sticky-bar.vav_wrap01.menu-sticky-bar").css("position", "fixed");
	jQuery(".content").css("padding-top", "90px");
     }
    else
     {
		// jQuery('.menu-sticky-bar').hide(500);
		//jQuery('.menu-sticky-bar').slideUp();
		jQuery("div#menu-sticky-bar.vav_wrap01.menu-sticky-bar").css("position", "initial");
		jQuery(".content").css("padding-top", "15px");
     }
 });
 </script>
<?php //get_footer(); ?>

<?php get_footer('footer-live'); ?>
