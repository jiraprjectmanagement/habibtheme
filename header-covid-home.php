<?php
/**
 * Header template for header_covid_home
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	
<!-- <title>test</title> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">	
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid.css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/> 
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style_covid_mobile.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/style-hpv1.css" />
<link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/menu/daisynav.css"/>
<meta property="og:url" content="<?php echo get_permalink($post->ID); ?>"/>
<meta property="og:title" content="<?php echo $post->post_title; ?>"/>
	

<?php
	wp_head();
?>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js"></script>

<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/tab.min.css" />
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/responsive.css">

<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/oge.css" />
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/habib/css/ogehasan.css" /> 

<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lity.min.css" rel="stylesheet"/>
<link href="<?php //echo esc_url( get_template_directory_uri() ); ?>/css/lity.css" rel="stylesheet"/>

<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/demo.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/daisynav_covid.css">


<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lity.min.js" async></script>

<!-- <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/menu/jquery-1.10.2.min.js" async></script> -->

<script src="<?php  echo esc_url( get_template_directory_uri() ); ?>/rpm/js/modernizr.custom.js" async></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js" async></script>

<!-- <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js" async></script> -->

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lazysizes.min.js" async></script>
<script>
    $(document).ready(function(){
        //$(".vav_wrap #nav-mobile").html($("#nav-main").html());
        $(".vav_wrap #nav-trigger span").click(function(){
            if ($(".vav_wrap nav#nav-mobile ul").hasClass("expanded")) {
                $(".vav_wrap nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".vav_wrap nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
		
		$(".inner_header #nav-trigger").click(function(){
            if ($(".inner_header nav#nav-mobile ul").hasClass("expanded")) {
                $(".inner_header nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
                $(this).removeClass("open");
            } else {
                $(".inner_header nav#nav-mobile ul").addClass("expanded").slideDown(250);
                $(this).addClass("open");
            }
        });
    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51636538-1', 'auto');
  ga('send', 'pageview');
</script>

<style>

html,body{
    font-display: optional;
}

.box {
    background: #e2d7bc;
	text-align: center;
	transition: all 0.5s linear;
	display: block;
	z-index: 999999;
	padding: 3px 0px;
}
#box a {
    font-size: 13px;
	border-bottom: 1px solid hsl(289deg 48% 28% / 41%);
	border-radius: 0;
	padding: 1px 12px;
	margin: 6px;
	color: #5c2568;
	font-weight: 700;
    
}

.hidden {
  display: none;
}

.visuallyhidden {
  opacity: 0;
}

button {
  display: block;
  margin: 0 auto;
  border: none;
}
.searchform {
    display: none;
    position: absolute;
    width: 700px;
    height: 70px;
    line-height: 40px;
    top: 0px;
    right: 0;
    padding: 0 15px;
    cursor: default;
    border-radius: 2px;
    border-style: solid;
    border-width: 1px;
    border-color: #e1e1e1;
    box-shadow: 0 3px 13px 0 rgba(0, 0, 0, 0.2);
    margin-left: -120px;
    z-index: 9999999;
    background-color: #fff;
}

.searchlink.open .searchform {
    display: block;
}

#search {
    display: block;
    position: relative;
}
.searchcst{
    width: 995px;
    float: right;
    text-align: right;
    margin-top: 17px;
    margin-bottom: 10px;
    font-family: 'Open Sans';
}

.sbtn {
    display: block;
    position: relative;
    background: none;
    border: none;
    color: #5c2568;
    font-size: 0.95em;
    font-weight: bold;
    border-radius: 6px;
    right: -8px;
    width: 101px;
    background-color: #f9b516;
    padding: 3px 0;
    margin-bottom: 1px;
}

.sbtn_cross{
    display: block;
    position: absolute;
    background: none;
    border: none;
    color: #5c2568;
    font-size: 0.9em;
    right: 10px;
    top: 14px;
    width: 0px;
    background-color: #fff;
}
.input-group {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: auto;
    float: right;
}
.search-form-wrapper {
    display: none;
    position: absolute;
    right: 40px;
    padding: 20px 15px;
    margin-top: 0px;
}
.search-form-wrapper.open {
    height: auto;
    background: #5c2568;
    display: block;
    position: absolute;
    padding: 12px 15px;
    top: 3px;
    width: 50%;
    border-radius: 12px;
    right: 50px;
}
.logohide{
    visibility: hidden;
}
#wrapper .search{
    width: auto; 
    float: right; 
    text-align: left; 
    margin-top: 17px; 
    margin-bottom: 0px; 
    box-shadow: none;
}
.top_menu {
    width: auto;
    float: right;
    margin-top: 20px;
    margin-bottom: 10px;
}
.menu-header{
    float: left;
}
.top_menu ul {
    width: 100%;
    list-style-type: none;
    margin: 0;
    margin-right: 5px;
}
.cst-input-search{
    float: left;
    width: 330px;
    display: inline-flex;
    margin-right: 36px;
}
.input-group input {
    width: 535px;
    border-bottom: 1px solid #f9b516 !important;
    background: transparent !important;
    color: #fff !important;
}

#box a i {
    font-size: 22px!important;
    margin: 0;
    padding: 0;
    vertical-align: bottom;
}
@media only screen only screen and (max-width: 736px) and (min-width: 360px){
  .logo a img {
    height: 150px;
  }
  .vav_wrap {
    display: none!important;
  }
  #mega-menu-wrap-mega_menu .mega-menu-toggle + #mega-menu-mega_menu { padding: 0px 0px 60px 0px; position: absolute;  width: 100%;   z-index: 9999999;   box-shadow: 2px 5px 62px #5f266b; }
}




</style>
</head>
<body <?php body_class(); ?>>

<div id="box" class="box"><a target="_blank" href="/covid19/">Habib Response - Covid-19 </a><span>|</span><a target="_blank" href="/covid19/app/"><i class="fa fa-mobile" aria-hidden="true"></i> COVID-19 App</a>
    <!--<button style="background-color: #e2d7bc; float: right;"><i class="fa fa-times" aria-hidden="true"></i></button>-->
</div>

<script>
let box = document.getElementById('box'),
    btn = document.querySelector('button');

btn.addEventListener('click', function () {
  
  if (box.classList.contains('hidden')) {
    box.classList.remove('hidden');
    setTimeout(function () {
      box.classList.remove('visuallyhidden');
    }, 20);
  } else {
    box.classList.add('visuallyhidden');    
    box.addEventListener('transitionend', function(e) {
      box.classList.add('hidden');
    }, {
      capture: false,
      once: true,
      passive: false
    });
  }
  
}, false);
</script>


<div id="wrapper" class="hfeed">
	<div class="inner_wrap">
    <div id="header">
    <!--inner_wrap-->
  
        <!--.inner_head-->	
    	<div class="inner_header">
            
                <div class="logo">
                    <a href="<?php echo home_url(); ?>" title="<?php echo bloginfo(); ?>">
                    <?php if(is_front_page() || is_page_template('home-page.php')) : ?>
                    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png" alt="Habib-logo" class="web_logo"/> <!--Running Code-->
                    <?php else : ?>
                    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"  alt="Habib-logo" class="web_logo">
                	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo_new.png"  alt="Habib-logo" class="mbl_logo">
                    <?php endif; ?>
                    </a>
                </div>
              
                <div id="nav-trigger" class="fa fa-bars"></div>
                <nav id="nav-mobile">
                    	<?php wp_nav_menu( array('theme_location' => 'top_mega_menu' ) ); ?>
                </nav>
                <div class="top_menu">
                	<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'top_mega_menu' ) ); ?>



                 <div class="hidden-xs navbar-form navbar-right" style="float: left; ">
                    <!-- <a href="#search" class="search-form-tigger"  data-toggle="search-form"><i class="fa fa-search scst" aria-hidden="true"></i></a> -->

                    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
        <input type="text" value="" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="Search" />
    </div>
</form>

                </div> 
      




                
          
                </div>
                
            </div><!--.inner_head-->
            
            <!--Mega Menu-->
          
                                    
            <div class="vav_wrap">
              <div class="res_menues"> <?php wp_nav_menu( array( 'theme_location' => 'mega_menu' ) ); ?>
              </div>
            </div>
            
        	</div>
	</div><!-- #header -->
            
         
            
          
        	</div>
	</div><!-- #header -->
	<div id="main">      
    
    	
<!-- <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js" async></script> -->
<!-- <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css"/> -->
<link rel="stylesheet" type="text/css" media="all" href="https://habib.edu.pk/wp-content/themes/habib/custom-mobile-style.css" />
     