<?php
/*
 Template Name: Wellness Center Inner
*/

get_header('wellness-center'); ?>
    <?php 
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
		if(!empty($url)) {
	?>
			<div class="topBg" style="background-image: url(<?php echo $url; ?>);">
				<div class="wc_content">
				<h1><?php the_title(); ?></h1>
				</div>
			</div><!-- .topBg -->
	<?php 
		}
	?>
    <div class="container">
        <div class="wc_content">
        	<div class="wc_left_content">
				<?php	
				if ( have_posts() ) :
					while (have_posts()) : the_post();
						the_content();
					endwhile;
				endif; ?>
			</div>
			 <div class="phySidebar wellnessSidebar">
			   <?php dynamic_sidebar('wellness_center'); ?>
			</div>
        </div>
        
    </div>
	

<?php// get_footer(); ?>
<?php get_footer('footer-live'); ?>