<?php
/**
 *
 */
if ( ! isset( $content_width ) )
	$content_width = 640;
/* Tell WordPress to run habib_setup() when the 'after_setup_theme' hook is run. */
    /*FORECEFULLY AJAX ON*/
        add_action( 'wp_ajax_prefix_update_post', 'prefix_update_post' );
        add_action( 'wp_ajax_nopriv_prefix_update_post', 'prefix_update_post' );
    /*FORECEFULLY AJAX ON*/
add_action( 'after_setup_theme', 'habib_setup' );
if ( ! function_exists( 'habib_setup' ) ):

function habib_setup() {
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'habib' ),
	) );
    
	
	register_nav_menus( array(
		'top_nav' => __( 'Top Navigation', 'habib' ),
        'mid_nav' => __( 'Mid Navigation', 'habib' ),
        'cpe_nav' => __( 'CPE - Menu', 'habib' ),
		'about_nav' => __( 'About Navigation', 'habib' ),
		'leaders_nav' => __( 'Leaders Navigation', 'habib' ),
		'students_nav' => __( 'Students Navigation', 'habib' ),
		'prospective_nav' => __( 'Prospective Students Navigation', 'habib' ),
		'ycsd_nav' => __( 'YCSD Navigation', 'habib' ),
                'footer_about' => __( 'Footer About Navigation', 'habib' ),
		'footer_house_of_habib' => __( 'Footer House Of Habib', 'habib' ),
        'footer_academics' => __( 'Footer Academics', 'habib' ),
		'footer_life_habib' => __( 'Footer Life Habib', 'habib' ),
		'footer_curent_student' => __( 'Footer Current Students ', 'habib' ),
		'footer_financial_aid' => __( 'Footer Financial Aid', 'habib' ),
		'footer_library' => __( 'Footer Library', 'habib' ),
		'footer_contribute' => __( 'Footer Contribute', 'habib' ),
		'footer_prospective_students' => __( 'Footer Prospective Students', 'habib' ),
		
		'footer_oge' => __( 'Footer Office Global Excellence', 'habib' ),
		'footer_ehsas' => __( 'Footer Ehsas Center', 'habib' ),
		
		'library_nav' => __( 'Library Navigation', 'habib' ),
		'library_nav_two' => __( 'Library Inner Navigation', 'habib' ),
		
		'oap_nav' => __( 'OAP Navigation', 'habib' ),
        'stdlifemenu' => __( 'Student Life Menu', 'habib' ),
        'quick_link' => __( 'Quick Link', 'habib' ),
		'president_office_top' => __( 'President Office Top', 'habib' ),
		'president_office_footer' => __( 'President Office Footer', 'habib' ),
		'oge_nav' => __( 'OGE Navigation', 'habib' ),
		'oge_nav_two' => __( 'OGE Inner Navigation', 'habib' ),
        
        'wellness_center_top' => __( 'Wellness Center Top', 'habib' ),
        'mega_menu' => __( 'Mega Menu', 'habib' ),
		'academic_affairs_top' => __( 'Academic Affairs Top', 'habib' ),
		// change by shariq at 13 april 2021
		'about_sidebar' => __( 'About', 'habib' ),
		// change by shariq at 21 june 2021
		'top_mega_menu' => __( 'Top Mega Menu', 'habib' ),

	) );
	
	
}
endif;
if ( ! function_exists( 'habib_admin_header_style' ) ) :
/**
 * Style the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in habib_setup().
 *
 * @since Twenty Ten 1.0
 */
////////////////////////////WP Version Remove Version//////////////////////////////////////////////////////
function wp_version_remove_version() {
	return '';
	}
	add_filter('the_generator', 'wp_version_remove_version');


	// remove tag generator
	remove_action( 'wp_head', 'wp_generator');

	// remove Slider Revolution meta tag generator
	add_filter('revslider_meta_generator', '__return_empty_string');

///////////////////////////////////////////////////////////////////////////////////
function habib_admin_header_style() {
?>
<style type="text/css" id="habib-admin-header-css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If header-text was supported, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

//Functions fonts mega menu
function megamenu_dequeue_google_fonts() {
   wp_dequeue_style( 'megamenu-google-fonts' );
}
add_action( 'wp_print_styles', 'megamenu_dequeue_google_fonts', 100 );

function enqueue_open_sans_font_weight($weights, $font) {
    if ( $font == 'Open Sans' ) { // check we're loading the Open Sans font
        $weights[] = 200; // add 200 to the array of required font weights
        $weights[] = 600; // add 600 to the array of required font weights
    }
    return $weights;
}
add_filter('megamenu_google_font_weights', 'enqueue_open_sans_font_weight', 10, 2);

////////////////////////////////////////////////////////////////////////////////////////

function habib_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'habib_page_menu_args' );

function add_author_role_to_associates(){
  global $user_id;
    $user = new WP_User( $user_id );
    if (get_field( 'associate_author' ) == 1) {
      $user->add_role( 'author' );
    }
}

function habib_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'habib_excerpt_length' );
if ( ! function_exists( 'habib_continue_reading_link' ) ) :
/**
 * Return a "Continue Reading" link for excerpts.
 *
 * @since Twenty Ten 1.0
 *
 * @return string "Continue Reading" link.
 */
function habib_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'habib' ) . '</a>';
}
endif;


function habib_auto_excerpt_more( $more ) {
	return ' &hellip;' . habib_continue_reading_link();
}
add_filter( 'excerpt_more', 'habib_auto_excerpt_more' );


function habib_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= habib_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'habib_custom_excerpt_more' );


add_filter( 'use_default_gallery_style', '__return_false' );

function habib_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
// Backwards compatibility with WordPress 3.0.
if ( version_compare( $GLOBALS['wp_version'], '3.1', '<' ) )
	add_filter( 'gallery_style', 'habib_remove_gallery_css' );
if ( ! function_exists( 'habib_comment' ) ) :

function habib_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment, 40 ); ?>
				<?php printf( __( '%s <span class="says">says:</span>', 'habib' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
			</div><!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'habib' ); ?></em>
				<br />
			<?php endif; ?>
			<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php
					/* translators: 1: date, 2: time */
					printf( __( '%1$s at %2$s', 'habib' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'habib' ), ' ' );
				?>
			</div><!-- .comment-meta .commentmetadata -->
			<div class="comment-body"><?php comment_text(); ?></div>
			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div><!-- #comment-##  -->
	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'habib' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'habib' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;
/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override habib_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 *
 * @uses register_sidebar()
 */
function habib_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'habib' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Home page Widget', 'habib' ),
		'id' => 'home-widget-area',
		'description' => __( 'An optional secondary widget area, displays below the primary widget area in your sidebar.', 'habib' ),
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'New Sidebar Widget', 'habib' ),
		'id' => 'newsibar',
		'description' => __( 'An optional secondary widget area, displays below the primary widget area in your sidebar.', 'habib' ),
		'before_widget' => '<div class="newside">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Home page Widget 2', 'habib' ),
		'id' => 'home2-widget-area',
		'description' => __( 'An optional secondary widget area, displays below the primary widget area in your sidebar.', 'habib' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'habib' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'An optional secondary widget area, displays below the primary widget area in your sidebar.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'habib' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'habib' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'habib' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 6, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'habib' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	// Area 7, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fifth Footer Widget Area', 'habib' ),
		'id' => 'fifth-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
		// Area 8, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Sixth Footer Widget Area', 'habib' ),
		'id' => 'sixth-footer-widget-area',
		'description' => __( 'An optional widget area for your site footer.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
		// Area 10, located in the home. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Home Widget Area', 'habib' ),
		'id' => 'fourth-home-widget-area',
		'description' => __( 'An optional widget area for your site content.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title" style="color:#fff;">',
		'after_title' => '</h3>',
	) );
	
	
	// Area 8, located in the home. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Home Widget Area', 'habib' ),
		'id' => 'second-home-widget-area',
		'description' => __( 'An optional widget area for your site content.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Area 9, located in the home. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Home Widget Area', 'habib' ),
		'id' => 'third-home-widget-area',
		'description' => __( 'An optional widget area for your site content.', 'habib' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar(array(
		'name'=> 'Left Sidebar',
		'id' => 'left_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'name'=> 'Arzu Slider',
		'id' => 'arzu-slider',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Arzu Menu - Testimonial',
		'id' => 'arzu_menu_testimonial',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
		register_sidebar(array(
		'name'=> 'Center Logo',
		'id' => 'center_logo',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name'=> 'About',
		'id' => 'about_sidebar',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name'=> 'Top Mega Menu',
		'id' => 'top_mega_menu',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'name'=> 'Event Sidebar',
		'id' => 'event_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'HUIT Sidebar',
		'id' => 'huit_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Career Sidebar',
		'id' => 'career_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Open Faculty Positions Sidebar',
		'id' => 'open-faculty-positions_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Physics Lab',
		'id' => 'physics_lab',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'OAP Sidebar',
		'id' => 'oapbar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'OGE Sidebar',
		'id' => 'ogebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Student Life Sidebar',
		'id' => 'student-life',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
    register_sidebar(array(
		'name'=> 'President Office Sidebar',
		'id' => 'wd-president-office',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Quick Links',
		'id' => 'wd-quick-links',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
        register_sidebar(array(
		'name'=> 'Admissions Sidebar',
		'id' => 'admission_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
     register_sidebar(array(
		'name'=> 'Liberal Core Sidebar',
		'id' => 'lc_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
     register_sidebar(array(
		'name'=> 'Student Affairs Sidebar',
		'id' => 'std_affairs_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
     register_sidebar(array(
		'name'=> 'Student Affairs Sidebar 01',
		'id' => 'std_affairs_1_sidebar', 
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
     register_sidebar(array(
		'name'=> 'Student Affairs Sidebar 02',
		'id' => 'std_affairs_2_sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title side">',
		'after_title' => '</h3>',
	));
    register_sidebar(array(
		'name'=> 'NSO2020 sidebar 01',
		'id' => 'nso2020_sidebar01', 
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title sidenso">',
		'after_title' => '</h3>',
	));
    register_sidebar(array(
		'name'=> 'Wellness Center',
		'id' => 'wellness_center',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
    register_sidebar(array(
		'name'=> 'CPE-sidebar',
		'id' => 'cpe-sidebar', 
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title sidenso">',
		'after_title' => '</h3>',
	));
}
/** Register sidebars by running habib_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'habib_widgets_init' );
/**
 * Remove the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * This function uses a filter (show_recent_comments_widget_style) new in WordPress 3.1
 * to remove the default style. Using Twenty Ten 1.2 in WordPress 3.0 will show the styles,
 * but they won't have any effect on the widget in default Twenty Ten styling.
 *
 * @since Twenty Ten 1.0
 */

/**   This function will allow us to write Hyperlink in Widget Title  **/
/**   This function will allow us to write Hyperlink in Widget Title  **/
add_filter( 'widget_title', 'anchor_in_widget_title' );
function anchor_in_widget_title($widgetTitle) {
// We are replacing  with [anchor][/anchor]
$widgetTitle = str_replace( '[anchor', '<a', $widgetTitle );
$widgetTitle = str_replace( '[/anchor]', '', $widgetTitle );
$widgetTitle = str_replace( ']', '>', $widgetTitle );
return $widgetTitle;
}

function habib_remove_recent_comments_style() {
	add_filter( 'show_recent_comments_widget_style', '__return_false' );
}
add_action( 'widgets_init', 'habib_remove_recent_comments_style' );
if ( ! function_exists( 'habib_posted_on' ) ) :
/**
 * Print HTML with meta information for the current post-date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function habib_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'habib' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'habib' ), get_the_author() ) ),
			get_the_author()
		)
	);
}
endif;
if ( ! function_exists( 'habib_posted_in' ) ) :
/**
 * Print HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function habib_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'habib' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'habib' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'habib' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;
/**
 * Retrieve the IDs for images in a gallery.
 *
 * @uses get_post_galleries() First, if available. Falls back to shortcode parsing,
 *                            then as last option uses a get_posts() call.
 *
 * @since Twenty Ten 1.6.
 *
 * @return array List of image IDs from the post gallery.
 */
function habib_get_gallery_images() {
	$images = array();
	if ( function_exists( 'get_post_galleries' ) ) {
		$galleries = get_post_galleries( get_the_ID(), false );
		if ( isset( $galleries[0]['ids'] ) )
		 	$images = explode( ',', $galleries[0]['ids'] );
	} else {
		$pattern = get_shortcode_regex();
		preg_match( "/$pattern/s", get_the_content(), $match );
		$atts = shortcode_parse_atts( $match[3] );
		if ( isset( $atts['ids'] ) )
			$images = explode( ',', $atts['ids'] );
	}
	if ( ! $images ) {
		$images = get_posts( array(
			'fields'         => 'ids',
			'numberposts'    => 999,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_mime_type' => 'image',
			'post_parent'    => get_the_ID(),
			'post_type'      => 'attachment',
		) );
	}
	return $images;
}
 require_once ( get_stylesheet_directory() . '/theme-options.php' ); 
add_action('init', 'codex_custom_init_home_slider');
function codex_custom_init_home_slider() {
	$labels = array(
    'name' => __('Home Slider', 'habib'),
    'singular_name' => __('Home Slider', 'habib'),
    'add_new' => __('Add New', 'habib'),
    'add_new_item' => __('Add New Slide', 'habib'),
    'edit_item' => __('Edit Slide', 'habib'),
    'new_item' => __('New Slide', 'habib'),
    'all_items' => __('All Slide', 'habib'),
    'view_item' => __('View Slide', 'habib'),
    'search_items' => __('Search', 'habib'),
    'not_found' =>  __('No Slides found', 'habib'),
    'not_found_in_trash' => __('No Slides found in Trash', 'habib'), 
    'parent_item_colon' => '',
    'menu_name' => 'Home Slider' 
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'home-slider'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail')
  ); 
  register_post_type('home-slider',$args);
  flush_rewrite_rules();
} 
add_action('init', 'codex_custom_init_team');
function codex_custom_init_team() {
	$labels = array(
    'name' => __('Board Of Governors', 'habib'),
    'singular_name' => __('Board Of Governors', 'habib'),
    'add_new' => __('Add New', 'habib'),
    'add_new_item' => __('Add New Board Member', 'habib'),
    'edit_item' => __('Edit Board Member', 'habib'),
    'new_item' => __('New Board Members', 'habib'),
    'all_items' => __('All Board Members', 'habib'),
    'view_item' => __('View Board Members', 'habib'),
    'search_items' => __('Search', 'habib'),
    'not_found' =>  __('No Board Members found', 'habib'),
    'not_found_in_trash' => __('No Board Members found in Trash', 'habib'), 
    'parent_item_colon' => '',
    'menu_name' => 'Board Of Governors'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'board-governors'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail','page-attributes')
  ); 
  register_post_type('board-governors',$args);
  flush_rewrite_rules();
}
function admin_init(){ 
    add_meta_box("eat_post_meta_1", "Designation:", "eat_post_meta_1", "board-governors", "side", "high");
	
	add_meta_box("eat_post_meta_5", "Background Image:", "eat_post_meta_5", "board-governors", "normal", "high");
	add_meta_box("eat_post_meta_2", "Sub Title:", "eat_post_meta_2", "page", "side", "high");
	
	//Add custom field for Science Faculty CPT
	add_meta_box("eat_post_meta_3", "Designation:", "eat_post_meta_3", "science-faculty", "side", "high");
	
	add_meta_box("slider_color", "Slider Detail:", "slider_color", "home-slider", "side", "high");
	
	//Add custom field for Arts Faculty CPT
	add_meta_box("eat_post_meta_4", "Designation:", "eat_post_meta_4", "arts-faculty", "side", "high");
    
	} 
add_action("admin_init", "admin_init"); // to register the custom field 
function eat_post_meta_1($callback_args) { 
	global $post; 
	  
	$custom = get_post_custom($post->ID); 
	?>
<p>
  <input id="desig" type="text" name="desig" value="<?php echo get_post_meta($post->ID, 'desig', true); ?>" width:240px;/>
</p>
<?php 
}
function eat_post_meta_5($callback_args) { 
	global $post; 
	  
	$custom = get_post_custom($post->ID); 
	?>
	<p>
      <input id="customimage" type="text" name="customimage" value="<?php echo get_post_meta($post->ID, 'customimage', true); ?>" style="width:340px;"/><input type="button" name="upload" class="button" value="Upload Image" id="Image_button"/>
    </p>
    
<script type="text/javascript">
		jQuery(document).ready(function() {
	
	var formfield;
	
	jQuery('#Image_button').click(function() {
		jQuery('html').addClass('Image');
		formfield = jQuery('#customimage').attr('name');
		tb_show('', 'media-upload.php?type=image&TB_iframe=true');
		return false;
	});
	
	// user inserts file into post. only run custom if user started process using the above process
	// window.send_to_editor(html) is how wp would normally handle the received data
	window.original_send_to_editor = window.send_to_editor;
	window.send_to_editor = function(html){
		if (formfield) {
			fileurl = jQuery('img',html).attr('src');
			
			jQuery('#customimage').val(fileurl);
			tb_remove();
			
			jQuery('html').removeClass('Image');
			
		} else {
			window.original_send_to_editor(html);
		}
	};
});
	</script>    
<?php 
}
function eat_post_meta_2($callback_args) { 
	global $post; 
	$custom = get_post_custom($post->ID); 
	?>
<p>
  <input id="sub_t" type="text"   name="sub_t" value="<?php echo get_post_meta($post->ID, 'sub_t', true); ?>" style="width:240px;"/>
</p>
<?php 
}
function eat_post_meta_3($callback_args) { 
	global $post; 
	$custom = get_post_custom($post->ID); 
	?>
<p>
  <input id="sci_desig" type="text"   name="sci_desig" value="<?php echo get_post_meta($post->ID, 'sci_desig', true); ?>" style=" width:240px;" />
</p>
<p>
  <textarea id="sci_desc" name="sci_desc" style=" width:240px;" ><?php echo get_post_meta($post->ID, 'sci_desc', true); ?></textarea>
</p>
<?php 
}
function eat_post_meta_4($callback_args) { 
	global $post; 
	$custom = get_post_custom($post->ID); 
	?>
<p><textarea style="width: 240px; height: 130px;" id="arts_desig" type="text" name="arts_desig"><?php echo get_post_meta($post->ID, 'arts_desig', true); ?></textarea></p>
<p><textarea placeholder="Courses Taught" style="width: 240px; height: 130px;" id="coursestaught" type="text" name="coursestaught"><?php echo get_post_meta($post->ID, 'coursestaught', true); ?></textarea></p>
<?php 
}

function slider_color($callback_args) { 
global $post; 
$custom = get_post_custom($post->ID); 
?>
<p>
<label for="color">Sub Title</label>	
  <input id="tagline" type="text"   name="tagline" value="<?php echo get_post_meta($post->ID, 'tagline', true); ?>" style="width:240px;"/>
</p>
	
<p>
<label for="color">BG Color</label>	
  <input id="color" type="text" name="color" value="<?php echo get_post_meta($post->ID, 'color', true); ?>" style="width:240px;"/>
</p>
<p>
<label for="color">Text Color</label>	
  <input id="textcolor" type="text"   name="textcolor" value="<?php echo get_post_meta($post->ID, 'textcolor', true); ?>" style="width:240px;"/>
</p>
<p>
<label for="color">Link</label>	
  <input id="link" type="text"   name="link" value="<?php echo get_post_meta($post->ID, 'link', true); ?>" style="width:240px;"/>
</p>
<?php 
}
function save_details($post_id) { 
	global $post;
	if (isset($_POST["color"]) && $_POST["color"] <> "") { $color = $_POST["color"]; } else { $color = ''; }
	update_post_meta($post->ID, "color", $color); 
	if (isset($_POST["tagline"]) && $_POST["tagline"] <> "") { $tagline = $_POST["tagline"]; } else { $tagline = ''; }
	update_post_meta($post->ID, "tagline", $tagline); 
	if (isset($_POST["customimage"]) && $_POST["customimage"] <> "") { $customimage = $_POST["customimage"]; } else { $customimage = ''; }
	update_post_meta($post->ID, "customimage", $customimage); 
	if (isset($_POST["textcolor"]) && $_POST["textcolor"] <> "") { $textcolor = $_POST["textcolor"]; } else { $textcolor = ''; }
	update_post_meta($post->ID, "textcolor", $textcolor); 
	if (isset($_POST["link"]) && $_POST["link"] <> "") { $link = $_POST["link"]; } else { $link = ''; }
	update_post_meta($post->ID, "link", $link); 
	if (isset($_POST["desig"]) && $_POST["desig"] <> "") { $desig = $_POST["desig"]; } else { $desig = ''; }
	update_post_meta($post->ID, "desig", $desig); 
	if (isset($_POST["sub_t"]) && $_POST["sub_t"] <> "") { $sub_t = $_POST["sub_t"]; } else { $sub_t = ''; }
	update_post_meta($post->ID, "sub_t", $sub_t);
	if (isset($_POST["sci_desig"]) && $_POST["sci_desig"] <> "") { $sci_desig = $_POST["sci_desig"]; } else { $sci_desig = ''; }
	update_post_meta($post->ID, "sci_desig", $sci_desig);
	if (isset($_POST["sci_desc"]) && $_POST["sci_desc"] <> "") { $sci_desc = $_POST["sci_desc"]; } else { $sci_desc = ''; }
	update_post_meta($post->ID, "sci_desc", $sci_desc);
	if (isset($_POST["arts_desig"]) && $_POST["arts_desig"] <> "") { $arts_desig = $_POST["arts_desig"]; } else { $arts_desig = ''; }
	update_post_meta($post->ID, "arts_desig", $arts_desig);
    if (isset($_POST["coursestaught"]) && $_POST["coursestaught"] <> "") { $coursestaught = $_POST["coursestaught"]; } else { $coursestaught = ''; }
	update_post_meta($post->ID, "coursestaught", $coursestaught);
	if (isset($_POST["arts_desc"]) && $_POST["arts_desc"] <> "") { $arts_desc = $_POST["arts_desc"]; } else { $arts_desc = ''; }
	update_post_meta($post->ID, "arts_desc", $arts_desc);
	 
}
add_action('save_post', 'save_details');
add_action( 'admin_enqueue_scripts', 'enqueue_admin' );
function enqueue_admin()
{
	wp_enqueue_script( 'thickbox' );
	wp_enqueue_style('thickbox');
	wp_enqueue_script('media-upload');
}
function image_menu_item($atts, $content = null) {  
    extract(shortcode_atts(array(  
		"title" => '',
		"color" => '',
		"bg" => '',
		"link" => '',
		"position" => '',
		"width" => '',
		"target" => ''
		), $atts));  
		$option = '';
		if(!empty($target)) {
			$option = ' target="_'.$target.'"';
		}
    //return '<a href="'.$to.'">'.$content.'</a>'; 
	return '<div class="menu_item '.$width.'"><a href="'.$link.'" '.$option.'><h2 class="'.$bg.' '.$color.'" style="'.$position.':0"><span>'.html_entity_decode($title).'</span></h2>'.$content.'</a></div>';  
} 
add_shortcode("menu-item", "image_menu_item");
//Add custom post type for Science Faculty
	add_action('init', 'codex_custom_init_sci_faculty');
	function codex_custom_init_sci_faculty() {
		$labels = array(
		'name' => __('Science Faculty', 'habib'),
		'singular_name' => __('Science Faculty', 'habib'),
		'add_new' => __('Add New', 'habib'),
		'add_new_item' => __('Add New Faculty', 'habib'),
		'edit_item' => __('Edit Faculty', 'habib'),
		'new_item' => __('New Faculty', 'habib'),
		'all_items' => __('All Faculty', 'habib'),
		'view_item' => __('View Faculty', 'habib'),
		'search_items' => __('Search', 'habib'),
		'not_found' =>  __('No Faculty found', 'habib'),
		'not_found_in_trash' => __('No Faculty found in Trash', 'habib'), 
		'parent_item_colon' => '',
		'menu_name' => 'Science Faculty'
  	);
  	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array('slug' => 'SSE'),
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail', 'page-attributes')
  	); 
  register_post_type('science-faculty',$args);
  flush_rewrite_rules();
} 
//Add custom post type for Arts Faculty
	add_action('init', 'codex_custom_init_arts_faculty');
	function codex_custom_init_arts_faculty() {
		$labels = array(
		'name' => __('Arts Faculty', 'habib'),
		'singular_name' => __('Arts Faculty', 'habib'),
		'add_new' => __('Add New', 'habib'),
		'add_new_item' => __('Add New Faculty', 'habib'),
		'edit_item' => __('Edit Faculty', 'habib'),
		'new_item' => __('New Faculty', 'habib'),
		'all_items' => __('All Faculty', 'habib'),
		'view_item' => __('View Faculty', 'habib'),
		'search_items' => __('Search', 'habib'),
		'not_found' =>  __('No Faculty found', 'habib'),
		'not_found_in_trash' => __('No Faculty found in Trash', 'habib'), 
		'parent_item_colon' => '',
		'menu_name' => 'Arts Faculty'
  	);
  	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array('slug' => 'AHSS'),
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail', 'page-attributes')
  	); 
  register_post_type('arts-faculty',$args);
  flush_rewrite_rules();
} 


//Add custom post type for Staff Positions
add_action('init', 'codex_custom_init_staff_jobs');
function codex_custom_init_staff_jobs() {
	$labels = array(
	'name' => __('Staff Positions', 'habib'),
	'singular_name' => __('Staff Positions', 'habib'),
	'add_new' => __('Add New', 'habib'),
	'add_new_item' => __('Add Job Title', 'habib'),
	'edit_item' => __('Edit Job', 'habib'),
	'new_item' => __('New Job', 'habib'),
	'all_items' => __('All Jobs', 'habib'),
	'view_item' => __('View Jobs', 'habib'),
	'search_items' => __('Search', 'habib'),
	'not_found' =>  __('No Job found', 'habib'),
	'not_found_in_trash' => __('No Job found in Trash', 'habib'), 
	'parent_item_colon' => '',
	'menu_name' => 'Staff Jobs'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true, 
	'show_in_menu' => true, 
	'query_var' => true,
	'rewrite' => array('slug' => 'hu-careers/staff-positions'),
	'capability_type' => 'page',
	'has_archive' => true, 
	'hierarchical' => false,
	'show_in_rest' => true,
	'menu_position' => null,
	'supports' => array('title','editor','excerpt','thumbnail')
  ); 
register_post_type('staff-jobs',$args);
flush_rewrite_rules();
} 

// custom taxonomy for Staff Positions
function staff_jobs_taxonomy() {  
	register_taxonomy(  
	 'staff-jobs-categories',  
	 'staff-jobs',  
	 array(  
		 'hierarchical' => true,  
		 'label' => 'Staff Jobs Categories',  
		 'query_var' => true,
		 'rewrite' => array( 'slug' => 'staff-jobs-categories' )
	 )  
 );  
	 flush_rewrite_rules(false);
 }  
 add_action( 'init', 'staff_jobs_taxonomy' );
//  End custom taxonomy for Staff Positions
// post Continue reading move to Read More 

//



/* Convert hexdec color string to rgb(a) string */
function hex2rgba($color, $opacity = false) {
	$default = 'rgb(0,0,0)';
	//Return default if no color provided
	if(empty($color))
          return $default; 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
        //Return rgb(a) color string
        return $output;
}
// Remove Canonical Link Added By Yoast WordPress SEO Plugin
add_filter( 'wpseo_canonical', '__return_false' );

function box_scode( $atts, $content = null) {
	extract(shortcode_atts( array(
   		'class' => '',
		'width' => '',
   ), $atts ));
   
	$mwzone = '<div class="'.$class.'" style="width:'.$width.'">'.$content.'</div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'box', 'box_scode' );



function box_list_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
   		'class' => '',
		'title' => ''
   ), $atts ));
   
	$mwzone = '<div class="box_list '.$class.'"><h3>'.$title.'</h3>'.$content.'</div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'box_list', 'box_list_scode' );

function boxx_scode( $atts, $content = null) {
	extract(shortcode_atts( array(
   		'class' => '',
		'width' => '',
   ), $atts ));
   
	$mwzone = '<div class="'.$class.'" style="width:'.$width.'">'.$content.'</div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'boxx', 'boxx_scode' );

function boxx_list_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
   		'class' => '',
		'title' => '',
		'padding' => ''
   ), $atts ));
   
	$mwzone = '<div class="box_list '.$class.'" style="padding:'.$padding.';"><h3>'.$title.'</h3>'.$content.'</div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'boxx_list', 'boxx_list_scode' );
function list_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
		   'class' => ''
   ), $atts ));
   
	$mwzone = '<div class="blist '.$class.'" >'.$content.'</div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'list', 'list_scode' );


function newsevents_scode( $atts, $content ) {
	extract(shortcode_atts( array(
		'id' => '',
		'img' => '',
	), $atts ));
	$wpdb2 = new wpdb('hu_news', 'wbbw75MERLy1lfx5', 'hu_news', 'localhost'); 
	$query = "SELECT ID, post_title, guid FROM hunw_posts
          WHERE ID = {$id}
          AND post_type = 'post'
          ORDER BY post_date DESC LIMIT 1";
	$posts = $wpdb2->get_results($query, OBJECT);
?>
<?php if( $posts ) :
	ob_start ();
?>
<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
	<article id="post-<?php echo $post->ID; ?>">
		<?php if($img) { ?>
        <a target="_blank" href="<?php echo $post->guid; ?>"><img src="<?php echo $img; ?>" /></a>
        <?php } ?>
        <div class="art_content">
            <h3><?php echo $post->post_title; ?></h3>
            <a target="_blank" href="<?php echo $post->guid; ?>">Read more on HU News</a>
        </div>
    </article>
	<?php endforeach;		
//	endwhile;
	wp_reset_postdata();
		endif;
	// Reset Post Data
//	wp_reset_query();
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'hunews', 'newsevents_scode' );

function huevents_scode($atts, $content = null ) {
	extract(shortcode_atts( array(
		'id' => '',
		'img' => '',
		'title' => '',
		'link' => '',
		'linktext' => ''
	), $atts ));
	
	$mwpost = get_post($id);
	ob_start();
?>	
	<article id="post-<?php echo $mwpost->ID; ?>">
		<?php if($img) { ?>
        	<?php if($link) { ?>
            	<a target="_blank" href="<?php echo $link; ?>"><img src="<?php echo $img; ?>" /></a>
            <?php } else { ?>
            	<a target="_blank" href="<?php echo get_permalink($mwpost->ID); ?>"><img src="<?php echo $img; ?>" /></a>
            <?php } ?>
        <?php } ?>
        <div class="art_content">
        	<?php if($title) { 
				echo "<h3>".$title."</h3>";	
			} else {
			?>
            <h3><?php echo $mwpost->post_title; ?></h3>
            <?php } ?>
            <?php 
				if($linktext){
					$mw_linktext = $linktext;
				} else {
					$mw_linktext = "View Upcoming Event";
				}
			?>
            <?php if($link) { ?>
            	<a target="_blank" href="<?php echo $link; ?>"><?php echo $mw_linktext; ?></a>
            <?php } else { ?>
            	<a target="_blank" href="<?php echo get_permalink($mwpost->ID); ?>"><?php echo $mw_linktext; ?></a>
            <?php } ?>
        </div>
    </article>
<?php
	$mwzone = ob_get_contents();
	ob_end_clean();
	return $mwzone;
}
add_shortcode( 'huevent', 'huevents_scode' );

function huFeatureImg($id) {
	$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/media/$id" );
	if( is_wp_error( $response ) ) {
		return;
	}
	$fimg = json_decode( wp_remote_retrieve_body( $response ) );
	
	return $fimg->guid->rendered;
}

function huNewsByApi_scode($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'num' => '',
   ), $atts ));
		
	ob_start();
	
	if(empty($num)) { 
		$mwNum = 5;
	} else {
		$mwNum = $num;
	}
	//http://habib.edu.pk/HU-news/wp-json/wp/v2/posts?filter[category__not_in]=255
    //$response = wp_remote_get( "http://habib.edu.pk/HU-news/wp-json/wp/v2/posts?filter[category__not_in]=255&per_page=$mwNum" );
	$response = wp_remote_get( "http://habib.edu.pk/HU-news/wp-json/wp/v2/posts/?&categories_exclude=255,257&per_page=$mwNum");
	if( is_wp_error( $response ) ) {
		return;
	}
	$posts = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $posts ) ) {
		return;
	}
	
	foreach( $posts as $post ) {
?>
		<article id="post-<?php echo $post->id; ?>">
		<?php 
			$img = $post->acf_field->home_page_thumbnail;
			if($img) {
		?>
        	<a target="_blank" href="<?php echo $post->link; ?>"><img src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" /></a>
        <?php } ?>
        	<div class="art_content">
            	<h3><?php echo $post->title->rendered; ?></h3>
            	<a target="_blank" href="<?php echo $post->link; ?>">Read more on HU News</a>
        	</div>
    	</article>
<?php
	//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
	}
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}


//social hub start section
function huNewsByApi_scode_h($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'num' => '',
   ), $atts ));
		
	ob_start();
	
	if(empty($num)) { 
		$mwNum = 3;
	} else {
		$mwNum = $num;
	}
	$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?categories=252&per_page=$mwNum" );
	if( is_wp_error( $response ) ) {
		return;
	}
	$posts = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $posts ) ) {
		return;
	}
	
	foreach( $posts as $post ) {
?>
		<article id="post-<?php echo $post->id; ?>">
      
		<?php 
			$img = $post->acf_field->home_page_thumbnail;
			if($img) {
		?>
        	<a target="_blank" href="<?php echo $post->link; ?>">
            <div class="tooltip_h">
            <img class="widget_img" src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" /></a>
            <span class="tooltiptext"><?php echo wp_trim_words( $post->title->rendered, 18 , '' ); ?></span>
            </div>
        <?php } ?>
        <div class="art_content">
         <h3><?php echo mb_strimwidth( $post->title->rendered, 0, 60 , '...' ); ?></h3>
        	</div>
    	</article>
        
<?php
	//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
	}
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
function huVideofromFB__scode_h($atts, $link, $content = null ) {
	extract(shortcode_atts( array('title' => '','link' => ''), $atts, $link ));
		
	ob_start();
	
	
    ?>
		<div class="video_div">
        <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FHabibUniversity%2Fvideos%2F<?=$link?>%2F&show_text=0&width=300&height=200" width="300" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="false" data-show-captions="true" data-show-text="true"></iframe>
        <div class="art_content"><h3><?php echo $title; ?></h3>
        <p class="artcontent_p">
        </p>
        </div>
        </div>
    <?php
$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
function hu_CS_Cat_scode_h($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'num' => '',
   ), $atts ));
		
	ob_start();
	
	if(empty($num)) { 
		$mwNum = 3;
	} else {
		$mwNum = $num;
	}
	//https://habib.edu.pk/HU-news/wp-json/wp/v2/categories/33
    $response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?categories=251&per_page=$mwNum" );
	//$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?categories=33?per_page=$mwNum" );
	if( is_wp_error( $response ) ) {
		return;
	}
	$posts = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $posts ) ) {
		return;
	}
	
	foreach( $posts as $post ) {
?>
		<article id="post-<?php echo $post->id; ?>">
      
		<?php 
			$img = $post->acf_field->home_page_thumbnail;
			if($img) {
		?>
        	<a target="_blank" href="<?php echo $post->link; ?>">
            <div class="tooltip_h">
            <img class="widget_img" src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" /></a>
            <span class="tooltiptext"><?php echo wp_trim_words( $post->title->rendered, 18 , '' ); ?></span>
            <!--<?php echo wp_trim_words( get_the_title(), 5 ); ?> -->
            </div>
        <?php } ?>
        <div class="art_content">
                <h3><?php echo mb_strimwidth( $post->title->rendered, 0, 60 , '...' ); ?></h3>
        	</div>
    	</article>
        
<?php
	//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
	}
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
//social hub end section
add_shortcode( 'huNewsByApi', 'huNewsByApi_scode' );
//HassanAli
add_shortcode( 'huNewsByApi_h', 'huNewsByApi_scode_h' );
add_shortcode( 'huVideofromFB', 'huVideofromFB__scode_h' );
add_shortcode( 'hu_cs_cat', 'hu_CS_Cat_scode_h' );
//HassanAli
function eventRegistration_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
   		'id' => '',
   ), $atts ));
	$mwzone = get_field( "registration_form" );;
	// $mwzone = do_shortcode($mwzone);
	return $mwzone;

}
add_shortcode( 'eventregistration', 'eventRegistration_scode' );

/* Working Paper Series */

add_action('init', 'working_paper_series_post_type');
	function working_paper_series_post_type() {
		$labels = array(
		'name' => __('Working Paper Series', 'habib'),
		'singular_name' => __('Working Paper Series', 'habib'),
		'add_new' => __('Add New', 'habib'),
		'add_new_item' => __('Add New WPS', 'habib'),
		'edit_item' => __('Edit WPS', 'habib'),
		'new_item' => __('New WPS', 'habib'),
		'all_items' => __('All WPS', 'habib'),
		'view_item' => __('View WPS', 'habib'),
		'search_items' => __('Search', 'habib'),
		'not_found' =>  __('No Faculty found', 'habib'),
		'not_found_in_trash' => __('No Faculty found in Trash', 'habib'), 
		'parent_item_colon' => '',
		'menu_name' => 'Working Paper Series'
  	);
  	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array('slug' => 'wps'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor', 'excerpt','thumbnail')
  	); 
  register_post_type('wps',$args);
  flush_rewrite_rules();
}



function wps_categories_taxonomy() {  
   register_taxonomy(  
	'wps_categories',  
	'wps',  
	array(  
		'hierarchical' => true,  
		'label' => 'WPS Categories',  
		'query_var' => true,
		'rewrite' => array( 'slug' => 'wps_category' )
	)  
);  
	flush_rewrite_rules(false);
}  

add_action( 'init', 'wps_categories_taxonomy' );

function wps_archives_taxonomy() {
   register_taxonomy(  
	'wps_archives',  
	'wps',  
	array(  
		'hierarchical' => true,  
		'label' => 'WPS Archive',  
		'query_var' => true,
		'rewrite' => array( 'slug' => 'wps_archives' )
	)  
);  
	flush_rewrite_rules(false);
}  

add_action( 'init', 'wps_archives_taxonomy' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Working Paper Options',
		'menu_title'	=> 'WPS Option',
		'parent_slug'	=> 'edit.php?post_type=wps',
	));
}

function string_limit_words($string, $word_limit) {
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}
function fb_opengraph() {
    global $post;
 
    if(is_single()) {
        if(has_post_thumbnail($post->ID)) {
            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
        } else {
            $img_src = get_stylesheet_directory_uri() . '/img/opengraph_image.jpg';
        }
        if($excerpt = $post->post_excerpt) {
            $excerpt = strip_tags($post->post_excerpt);
            $excerpt = string_limit_words($excerpt,15)."...";
        } else {
            $excerpt = get_bloginfo('description');
        }
        ?>
 
    <meta property="og:title" content="<?php echo the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo $img_src; ?>"/>
 
<?php
    } else {
        return;
    }
}
add_action('wp_head', 'fb_opengraph', 5);

function googleMap_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
   		'id' => '',
		'title' => ''
   ), $atts ));
   
   $mid = rand(100, 999);
?>
<script> 
    google.maps.event.addDomListener(window, 'load', init);
    var map;
    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(24.906567,67.143895),
            zoom: 15,
            zoomControl: false,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            panControl: true,
            streetViewControl: true,
            draggable : true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            'styles': [{
			stylers: [{
				gamma: 0.00
			}, {
				hue: "#321438"
			}, {
				invert_lightness: false
			}, {
				lightness: 10
			}, {
				saturation: 0
			}, {
				visibility: "on"
			}]
		}],
        } // mapOptions
        var mapElement = document.getElementById('map<?php echo $mid; ?>');
        var map = new google.maps.Map(mapElement, mapOptions);
        var locations = [
['Habib University', 'Habib University remains the hub of Liberal Arts and Sciences Education in Pakistan, ever since its inception in 2014.', '+92 21 111 042242 (HABIB)', 'undefined', 'habib.edu.pk', 24.906000,  67.137592, 'https://habib.edu.pk/fav.png']
        ];
        for (i = 0; i < locations.length; i++) {
			if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
			if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
			if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
           if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
           if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
            marker = new google.maps.Marker({
                icon: markericon,
                position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                map: map,
                title: locations[i][0],
                desc: description,
                tel: telephone,
                email: email,
                web: web
            });
link = '';     }

}
	
//	setInterval(function(){google.maps.event.trigger(window, 'load'); }, 4000);
		
</script>
<style>
    #map<?php echo $mid; ?> {
        height:350px;
        width:100%px;
    }
    .gm-style-iw * {
        display: block;
        width: 100%;
    }
    .gm-style-iw h4, .gm-style-iw p {
        margin: 0;
        padding: 0;
    }
    .gm-style-iw a {
        color: #4272db;
    }
</style>
<?php   
	$mwzone = '<div class="accordion"><h3 class="panel-title" id="'.$id.'">'.$title.'</h3><div class="panel-content"><div id="map'.$mid.'"></div></div></div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
} 
add_shortcode( 'googleMap', 'googleMap_scode' );


function custGoogleMap_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
   		'id' => '',
		'title' => ''
   ), $atts ));
   
   $mid = rand(100, 999);
?>
<script> 
    google.maps.event.addDomListener(window, 'load', init);
    var map;
    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(24.906567,67.143895),
            zoom: 15,
            zoomControl: false,
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            panControl: true,
            streetViewControl: true,
            draggable : true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            'styles': [{
			stylers: [{
				gamma: 0.00
			}, {
				hue: "#321438"
			}, {
				invert_lightness: false
			}, {
				lightness: 10
			}, {
				saturation: 0
			}, {
				visibility: "on"
			}]
		}],
        } // mapOptions
        var mapElement = document.getElementById('map<?php echo $mid; ?>');
        var map = new google.maps.Map(mapElement, mapOptions);
        var locations = [
['Habib University', 'Habib University remains the hub of Liberal Arts and Sciences Education in Pakistan, ever since its inception in 2014.', '+92 21 111 042242 (HABIB)', 'undefined', 'habib.edu.pk', 24.906000,  67.137592, 'https://habib.edu.pk/fav.png']
        ];
        for (i = 0; i < locations.length; i++) {
			if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
			if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
			if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
           if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
           if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
            marker = new google.maps.Marker({
                icon: markericon,
                position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                map: map,
                title: locations[i][0],
                desc: description,
                tel: telephone,
                email: email,
                web: web
            });
link = '';     }

}
</script>
<style>
    #map<?php echo $mid; ?> {
        height:350px;
        width:100%;
		margin-bottom:20px;
    }
    .gm-style-iw * {
        display: block;
        width: 100%;
    }
    .gm-style-iw h4, .gm-style-iw p {
        margin: 0;
        padding: 0;
    }
    .gm-style-iw a {
        color: #4272db;
    }
</style>
<?php   
	$mwzone = '<div id="map'.$mid.'"></div>';
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
} 
add_shortcode( 'CustGoogleMap', 'custGoogleMap_scode' );



/* Course Catlog */

add_action('init', 'course_catlog_post_type');

function course_catlog_post_type() {
	$labels = array(
		'name' => __('Course Desc', 'habib'),
		'singular_name' => __('Course Desc', 'habib'),
		'add_new' => __('Add New', 'habib'),
		'add_new_item' => __('Add New CC', 'habib'),
		'edit_item' => __('Edit CC', 'habib'),
		'new_item' => __('New Course', 'habib'),
		'all_items' => __('All Courses', 'habib'),
		'view_item' => __('View Course', 'habib'),
		'search_items' => __('Search', 'habib'),
		'not_found' =>  __('No Course found', 'habib'),
		'not_found_in_trash' => __('No Course found in Trash', 'habib'), 
		'parent_item_colon' => '',
		'menu_name' => 'Course Desc'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array('slug' => 'course-descriptions'),
		'capability_type' => 'page',
		'has_archive' => true, 
		'hierarchical' => false,
		'show_in_rest' => true,
		'menu_position' => null,
		'supports' => array('title','editor', 'excerpt','thumbnail')
	);
	
	register_post_type('course_catlog',$args);
	flush_rewrite_rules();
}

add_action( 'wp_ajax_ajaxify', 'prefix_ajax_ajaxify' );
add_action( 'wp_ajax_nopriv_ajaxify', 'prefix_ajax_ajaxify' );
function prefix_ajax_ajaxify() {	
	$post_id = $_POST['post_id'];
	$post_data = get_post($post_id);
	
	ob_start();
	
// 	var_dump($post_data);
//	echo "<h2>".$post_data->post_title."</h2>";
	$field = get_field_object('course_type', $post_id);
	$value = get_field('course_type', $post_id);
	$cdprogram = get_field('course_school', $post_id);
	$course_type = $field['choices'][ $value ];
	// var_dump($value);
	
	echo '<div class="mw_content"><div class="mw_conten">'.$post_data->post_excerpt.'</div>';
	if(!empty($post_data->post_content)) {
		echo '<div class="show-more">'.apply_filters('the_content', $post_data->post_content).'</div><a href="#" onClick="more();" class="morebtn">more</a> <a href="#" style="display: none;" onClick="more();" class="lessbtn">less</a>';
	}
	echo '</div><div class="mw_cust">Course Type: <b>';
	echo implode(', ', array_map(function ($entry) {
		return $entry['label'];
	}, $value));
	echo '</b>. <br />Program: <b>';
	echo implode(', ', array_map(function ($entry) {
		return $entry['label'];
	}, $cdprogram));
	echo '</b><br>'.get_field('course_description', $post_id).'</div>';
	
	$response = ob_get_contents();
	ob_end_clean();
	echo $response;
	die(1);	
}

/* Research Streams */

add_action('init', 'research_streams_post_type');

function research_streams_post_type() {
	$labels = array(
		'name' => __('Research Streams', 'habib'),
		'singular_name' => __('Research Stream', 'habib'),
		'add_new' => __('Add New', 'habib'),
		'add_new_item' => __('Add New RS', 'habib'),
		'edit_item' => __('Edit RS', 'habib'),
		'new_item' => __('New Research', 'habib'),
		'all_items' => __('All Research', 'habib'),
		'view_item' => __('View Research', 'habib'),
		'search_items' => __('Search', 'habib'),
		'not_found' =>  __('No Research found', 'habib'),
		'not_found_in_trash' => __('No Research found in Trash', 'habib'), 
		'parent_item_colon' => '',
		'menu_name' => 'Research Streams'
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array('slug' => 'idrac/research-themes'),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor', 'excerpt','thumbnail')
	);
	register_post_type('research-streams',$args);
	flush_rewrite_rules();
}

function speakers_taxonomy() {  
   register_taxonomy(  
	'speakers',  
	'event',  
	array(  
		'hierarchical' => true,  
		'label' => 'Speakers',  
		'query_var' => true,
		'rewrite' => array( 'slug' => 'idrac/speaker' )
	)  
);  
	flush_rewrite_rules(false);
}  

add_action( 'init', 'speakers_taxonomy' );


function ahss_scode( $atts, $content ) {
	extract(shortcode_atts( array(
		'id' => '',
		'img' => '',
	), $atts ));
	$wpdb3 = new wpdb('habibedu_bl0gahs', 'mxKx7+,{uTe[', 'habibedu_ahssblog', 'localhost'); 
	$query = 'SELECT p1.*, wm2.meta_value FROM k48h7d_posts p1 LEFT JOIN
k48h7d_postmeta wm1 ON (
wm1.post_id = p1.id
AND wm1.meta_value IS NOT NULL
AND wm1.meta_key = "_thumbnail_id"
)
LEFT JOIN k48h7d_postmeta wm2 ON (wm1.meta_value = wm2.post_id AND wm2.meta_key = "_wp_attached_file"
AND wm2.meta_value IS NOT NULL) WHERE p1.post_status="publish" AND p1.post_type="post"
ORDER BY p1.post_date DESC LIMIT 1';
	$posts = $wpdb3->get_results($query, OBJECT);
?>
<?php if( $posts ) :
	ob_start ();
	
//	var_dump($postswp);
?>
<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
	<article id="post-<?php echo $post->ID; ?>">
    	<img src="https://habib.edu.pk/ahssblog/wp-content/uploads/<?php echo $post->meta_value; ?>" />
		<?php if($img) { ?>
        <a target="_blank" href="<?php echo $post->guid; ?>"><img src="<?php echo $img; ?>" /></a>
        <?php } ?>
        <div class="art_content">
            <h3><?php echo $post->post_title; ?></h3>
            <a target="_blank" href="<?php echo $post->guid; ?>">Read more on HU News</a>
        </div>
    </article>
	<?php endforeach;		
//	endwhile;
	wp_reset_postdata();
		endif;
	// Reset Post Data
//	wp_reset_query();
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'ahss', 'ahss_scode' );

function faculty_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'id' => '',
		'type' => '',
		'view' => ''
	), $atts ));
	
	if($type == 'sse') {
		$posttype = "science-faculty";
	} elseif( $type == 'ahss' ) {
		$posttype = "arts-faculty";
	}
	
	$numpost = explode(',', $id);
    
	if(empty($id)){
		$args = array(
				'posts_per_page' => -1,
				'post_type' => $posttype,
				'order' => 'DESC',
				'orderby' => 'menu_order'
			);
	} else {
		$args = array(
				'posts_per_page' => -1,
				'post_type' => $posttype,
				'post__in' => $numpost,
				'order' => 'DESC',
				'orderby' => 'menu_order'
			);
	}
	
	$myposts = get_posts($args);
	
	ob_start ();
	
	// var_dump($myposts);
//	echo '<div class="facBox '. $view .'">';
	foreach( $myposts as $page ) {
		echo '<div class="facInBox"><a target="_blank" href="'.get_permalink($page->ID).'">';
		$url = wp_get_attachment_url( get_post_thumbnail_id($page->ID) );
?>		
		<img src="<?php echo $url; ?>" alt="<?php echo $page->post_title; ?>" />
<?php
		if($view == "list") { echo '<div class="mwInnerBox">'; }
		echo '<h3>{$page->post_title}</h3><p class="facinbox-dec">';
		echo get_post_meta($page->ID, 'arts_desig', true);
        echo get_post_meta($page->ID, 'sci_desig', true);
		echo get_post_meta($page->ID, 'coursestaught', true);
		if($view == "list") { echo "<p style='margin-top: 10px;'>". get_post_meta($page->ID, 'arts_desc', true) . get_post_meta($page->ID, 'sci_desc', true) ."</p></div>"; }
		echo '</a></p></div>';
	
	}
//	echo '</div>';
	wp_reset_query();
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
			
}
add_shortcode( 'faculty', 'faculty_scode' );

function facultyCust_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'img' => '',
		'title' => '',
		'designation' => '',
		'link' => '',
		'view' => ''
	), $atts ));
	
	ob_start ();
	?>
    <div class="facInBox">
<?php if($link) {  ?>   
    <a target="_blank" href="<?php echo $link; ?>">
<?php } ?>    
    	<img src="<?php echo $img; ?>" alt="<?php echo $title; ?>" />
<?php if($view == "list") { echo '<div class="mwInnerBox">'; } ?>        
		<h3><?php echo $title; ?></h3>
        <?php echo $designation; ?>
<?php if($view == "list") { echo "<p style='margin-top: 10px;'>". $content ."</p></div>"; } ?>        
 <?php if($link) {  echo "</a>"; } ?>  	
    </div>
<?php	
    $mwzone = ob_get_contents();
	ob_end_clean();	
	
	return $mwzone;
}
add_shortcode( 'facultyCust', 'facultyCust_scode' );

function video_scode( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'img' => '',
		'url' => '',
	), $atts ));
	
	$mwzone = '<a href="'.$url.'" class="mwVideo" rel="wp-video-lightbox"><img src="'.$img.'" /></a>';
	
	return $mwzone;
}
add_shortcode( 'video', 'video_scode' );



function artfaclty( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'id' => '',
		'url' => '',
	), $atts ));
	
    $myposts = get_post($id);
    $url = wp_get_attachment_url( get_post_thumbnail_id($myposts->ID) );

    ?>
    <?php
   
    //var_dump($myposts);
    echo '<div class="facInBox"><a target="_blank" href="'.get_permalink($myposts->ID).'">';
    echo "<img src='".$url."'>";
    echo '<h3 style="fach3">'.$myposts->post_title.'</h3><p class="facinbox-dec">';
    echo get_post_meta($myposts->ID, 'arts_desig', true);
    echo '</p></a></div>';

}
add_shortcode( 'artfaclty', 'artfaclty' );
 

function scifaclty( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'id' => '',
		'url' => '',
	), $atts ));
	
    $myposts = get_post($id);
    $url = wp_get_attachment_url( get_post_thumbnail_id($myposts->ID) );

    ?>
    <?php
   
    //var_dump($myposts);
    echo '<div class="facInBox"><a target="_blank" href="'.get_permalink($myposts->ID).'">';
    echo "<img src='".$url."'>";
    echo '<h3 style="fach3">'.$myposts->post_title.'</h3><p class="facinbox-dec">';
    echo get_post_meta($myposts->ID, 'sci_desig', true);
    echo '</p></a></div>';

}
add_shortcode( 'scifaclty', 'scifaclty' );
/* Start Three HU CS Posts */

function hucs_scode( $atts, $content ) {
	extract(shortcode_atts( array(
		'id' => '',
		'img' => '',
	), $atts ));
	$wpdb3 = new wpdb('habibedu_hun3ws1', 'hP=gZ5oo}aE(', 'habibedu_hunews', 'localhost'); 
	$query = 'SELECT p1.*, wm2.meta_value FROM wp_posts p1 LEFT JOIN
wp_postmeta wm1 ON (
wm1.post_id = p1.id
AND wm1.meta_value IS NOT NULL
AND wm1.meta_key = "_thumbnail_id"
)
LEFT JOIN wp_postmeta wm2 ON (wm1.meta_value = wm2.post_id AND wm2.meta_key = "_wp_attached_file"
AND wm2.meta_value IS NOT NULL) WHERE p1.post_status="publish" AND p1.post_type="post"
ORDER BY p1.post_date DESC LIMIT 3';
	$posts = $wpdb3->get_results($query, OBJECT);
?>
<?php if( $posts ) :
	ob_start ();
	
//	var_dump($postswp);
?>
<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
	<article id="post-<?php echo $post->ID; ?>">
    	<img src="https://habib.edu.pk/HU-news/wp-content/uploads/<?php echo $post->meta_value; ?>" />
		<?php if($img) { ?>
        <a target="_blank" href="<?php echo $post->guid; ?>"><img src="<?php echo $img; ?>" /></a>
        <?php } ?>
        <div class="art_content">
            <h3><?php echo $post->post_title; ?></h3>
            <a target="_blank" href="<?php echo $post->guid; ?>">Read more on HU News</a>
        </div>
    </article>
	<?php endforeach;		
//	endwhile;
	wp_reset_postdata();
		endif;
	// Reset Post Data
//	wp_reset_query();
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'hucs', 'hucs_scode' );

/* End Three HU CS Posts */



add_action( 'admin_init', 'stop_access_profile__DUMPD');
function stop_access_profile__DUMPD() {
	if(!current_user_can( 'create_users')){
		// global $current_user;
		// $user_roles = $current_user->roles;
		$disabled =true;	
		if (get_option('AllOrInd__DUMPP') == 'all') {	$disabled =true;	}
		if (get_option('AllOrInd__DUMPP') != 'all') {	$ids = explode( ',', get_option('DisabledIds__DUMPP'));
			if ($GLOBALS['current_user']->ID == 0 || in_array( $GLOBALS['current_user']->ID, $ids)){  $disabled =true;  }
		}
		if (isset($disabled)) { ///////////////START DISABLINGG/////////////////
			remove_menu_page('profile.php');	
			remove_submenu_page( 'users.php', 'profile.php' );
			if( (defined( 'IS_PROFILE_PAGE' ) && IS_PROFILE_PAGE && IS_PROFILE_PAGE === true)) {
				wp_redirect( admin_url() );
				wp_die( 'You are not permitted to change your own profile information.<br />Please contact Business Computing Department to have your profile information changed. <a href="'.admin_url().'">Dashboard url</a>' );
			}
		}
	}
}


function mysite_custom_sort( $orderby, $wp_query ) {
  if ( isset( $wp_query->query_vars['sort_custom'] ) ) {
    $orderby = 'mt1.meta_value, mt2.meta_value ASC';
  }
  return $orderby;
}
add_filter( 'posts_orderby', 'mysite_custom_sort', 10, 2 );




add_action( 'init', 'my_event_cpt' );
function my_event_cpt() {
    $args = array(
      'public'       => true,
      'show_in_rest' => true,
      'label'        => 'Event'
    );
    register_post_type( 'event', $args );
}

/**
  * Add REST API support to an already registered taxonomy.
  */


add_action( 'init', 'event_taxonomy_rest_support', 25 );
function event_taxonomy_rest_support() {
	global $wp_taxonomies;

	//be sure to set this to the name of your taxonomy!
	$taxonomy_name = 'event-categories';

	if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
		$wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
		$wp_taxonomies[ $taxonomy_name ]->rest_base = $taxonomy_name;
		$wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
	}
}

// ACF in WP-API\
add_action( 'rest_api_init', 'my_event_name' );
function my_event_name() {
    register_rest_field( 'event',
        'mw_category_name',
        array(
            'get_callback'    => 'event_name',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function event_name( $object, $request ) {
    $post_id = $object[ 'id' ];
    $ecat = $object[ 'event-categories' ];
    global $post, $EM_Event;
	$term = get_term( $ecat[0], 'event-categories');
	$cat_img = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
	
	$EMEventID = $EM_Event->event_id;
	$EM_Location = $EM_Event->get_location();
	
	$eventStartDateTime = $EM_Event->start;
	
//	$EM_Event = em_get_event(get_queried_object_id(), $post_id);
	
	$eventDate = date('d/m/Y', $EM_Event->start);
	$eventTime = date('h:i A - ', $EM_Event->start).date('h:i A', $EM_Event->end); 
	
	$start_time = date('D M d Y H:i:s', $EM_Event->start);
	$end_time = date('D M d Y H:i:s', $EM_Event->end);
	
	$timestamp = time();
	$upcoming = FALSE;
	$upcoming = FALSE;

	if($EM_Event->start > $timestamp) {
		$upcoming = TRUE;
	}
    $scope = null;

	if(!empty(get_field( "event_status", $post_id ))) {
        $scope = get_field( "event_status", $post_id );
    }

    $result["event_scope"] = $scope;

	$result["cat_name"] = $term->name;
	$result["cat_id"] = $ecat[0];
	$result["cat_img"] = $cat_img;
	
	$result["event_date"] = $eventDate; #$EM_Event->start;
	$result["event_time"] = $eventTime;
	
	$result["start_time"] = $start_time; 
	$result["end_time"] = $end_time;
	$result["startimestamp"] = $eventStartDateTime;
	
	$result["event_upcoming"] = $upcoming;

    $result["event_scope"] = $scope;

//	$result["event_info"] = $EM_Evento;
	$result["event_location"] = $EM_Location->location_name;
	
	return $result; #$post_id;
}


function ms_image_editor_default_to_gd( $editors ) {
	$gd_editor = 'WP_Image_Editor_GD';
	$editors = array_diff( $editors, array( $gd_editor ) );
	array_unshift( $editors, $gd_editor );
	return $editors;
}
add_filter( 'wp_image_editors', 'ms_image_editor_default_to_gd' );

function lib_box_scode($atts, $content) {
	extract(shortcode_atts( array(
		'image' => '',
		'title' => '',
		'link' => '',
	), $atts));	
	
	ob_start ();
	
	echo '<div class="mlbox col">';
	echo '<img src="'.$image.'" alt="'.$title.'" />';
	echo '<h4><a href="'.$link.'">'.$title.'</a></h4>';
	echo '</div>';
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'lbox', 'lib_box_scode');

/* Single News get by category ID using API */  

function huNewsByCatIdFrmApi_scode($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'num' => '',
   		'catid' => ''
   ), $atts ));
		
	ob_start();

	if(empty($catid)) {
		$myCat = '';
	} else {
		$myCat = $catid;
	}
	if(empty($num)) { 
		$mwNum = 1;
	} else {
		$mwNum = $num.'&categories='.$myCat;
	}

	
	$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?per_page=$mwNum" );
	if( is_wp_error( $response ) ) {
		return;
	}
	$posts = json_decode( wp_remote_retrieve_body( $response ) );

	if( empty( $posts ) ) {
		return;
	}
	
	foreach( $posts as $post ) {
?>
		<article id="post-<?php echo $post->id; ?>">
		<?php 
			$img = $post->acf_field->home_page_thumbnail;
			if($img) {
		?>
        	<a target="_blank" href="<?php echo $post->link; ?>"><img src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" /></a>
        <?php } ?>
        	<div class="art_content">
            	<h4 style="color: #5c2568;margin-bottom: 6px;margin-top: 8px;font-size: 16px;"><?php echo $post->title->rendered; ?></h4>
            	<a style="font-size: 12px;" target="_blank" href="<?php echo $post->link; ?>">Read more</a>
        	</div>
    	</article>
<?php
	//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
	}
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
add_shortcode( 'huNewsByCatIdFrmApi', 'huNewsByCatIdFrmApi_scode' );

function poBox_scode($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'class' => '',
		'image' => '',
		'link' => '',
		'title' => '',
		'text' => ''
	), $atts ));
		
	ob_start();
	
	// echo '<div class="'.$class.'">';
	
	echo '<div class="po_bog"><div class="poBogItem"><img src="'.$image.'" /><h3><a href="' . $link . '">' . $title . '</a></h3><p>' . $text . '</p></div></div>';
	
	// echo '</div>';
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;

}
add_shortcode( 'poBox', 'poBox_scode' );

function full_scode($atts, $content=null) {
	extract(shortcode_atts( array(
		'class' => ''
	), $atts));
	$full = '<div class="full '.$class.'">'.$content.'</div>';
	$full = do_shortcode($full);
	return $full;
}
add_shortcode( 'full', 'full_scode');	
function hmbox_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'title'	=> '',
		'link'	=> '',
		'class'	=> '',
	), $atts));	
	
	ob_start ();
?>	
		<div class="hmbox <?php echo $class; ?>"><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" /><span class="indigo"><?php echo $title; ?></span></a></div>
<?php	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'hmbox_one', 'hmbox_scode');

function mbox_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'link' => '',
		'date' => '',
		'title' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="mbox">
			<div class="wc_img">
				<?php if($link) { ?><a href="<?php echo $link; ?>" target="_blank"><?php } ?>
				<img src="<?php echo $image; ?>" />
				<?php if($link) { ?></a><?php } ?>
			</div>
			<div class="wc_content_area">
				<?php
				if($title){ echo "<h3>$title</h3>"; } 
				if($date){ echo "<date>$date</date>"; } 
				?>
				<p><?php echo $content; ?></p>
			</div>			
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'mbox', 'mbox_scode');

function stats_giving_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'ntitle'	=> '',
		'glink'	=> '',
		'gimage'	=> '',
		'gtitle'	=> '',
	), $atts));	
	
	ob_start ();
?>	
		<div class="stats_giving"><div class="stats"><ul id="stats"><?php echo $content; ?></ul><span class="tag-line"><?php echo $ntitle; ?></span> </div><div class="hm_giving"><a title="Habib University" href="<?php echo $glink; ?>"><img src="<?php echo $gimage; ?>" alt="giving" /><span class="yellow"><?php echo $gtitle; ?></span></a></div></div>
<?php	
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'stats_giving', 'stats_giving_scode');

function stat_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'title'	=> '',
		'link'	=> '',
		'num'	=> '',
	), $atts));	
	
	ob_start ();
?>	
		<li><span class="nums"><p><?php echo $title; ?></p><a target="_blank" href="<?php echo $link; ?>"><?php echo $num; ?></a></span><p><?php echo $content; ?></p></li>
<?php	
	$mwzone = ob_get_contents();
	ob_end_clean();
//	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'stat', 'stat_scode');

//student committees
function committees_slider( $atts, $content ) {
	extract(shortcode_atts( array(
   		'class' => '',
   	), $atts ));
    ?>
    
    <!--<section class="customer-logos slider">
        <?=$content?>
      <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
   </section>-->
   <script>
    jQuery(document).ready(function(){
        jQuery('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });
   </script>
    <?php
}
add_shortcode( 'c_slider', 'committees_slider' );

function bxslider_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'title'	=> '',
		'link'	=> '',
		'num'	=> '',
	), $atts));	
	
	ob_start ();
?>	
		<div id="bxslider" class="full">
			<ul class="bxslidertwo">
			  <?php echo $content; ?>
			</ul>
		</div>
		
<?php	
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'bxslider', 'bxslider_scode');


function bxslide_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'link' => '',
		'target' => ''
	), $atts));	
	
	ob_start();
		
	if($link == '') {
	?>
		<li><img src="<?php echo $image; ?>" /></li>
	<?php
	} else {
	?>
		<li><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><img src="<?php echo $image; ?>" /></a></li>
	<?php
	}
?>	
		
<?php	
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'bxslide', 'bxslide_scode');

//CUSTOM SLIDER SHORTCODE
function custom_slider($atts, $content = null) {
	extract(shortcode_atts( array(
        'class' => "",
		'image1' => '',
		'image2' => '',
        'image3' => '',
        'image4' => '',
        'image5' => '',
        'image6' => '',
        'image7' => '',
        'image8' => '',
		'image9' => '',
        'image10' => ''
	), $atts));	
	?>
        <link href="<?php echo get_template_directory_uri(); ?>/css/flexslider.min.css" rel="stylesheet" />
        
        <style>
            .culture-section {
                background: linear-gradient(rgba(0, 0, 0, 0.45),rgba(0, 0, 0, 0.45)), url(https://www.solodev.com/assets/culture.jpg) top no-repeat;
                height: 100%;
                background-size: cover;
                color: #fff;
            	  text-align: center;
                padding: 250px 0px;
            }
            .culture-section h2, .culture-section h3, {
                font-family: 'proxima-nova', sans-serif;
            }
            .culture-section h2 {
                font-weight: 800;
                font-size: 50px;
            }
            .culture-section h3 {
                font-size: 25px;
                font-weight: 700;
                margin: 14px 0;
                font-family: 'proxima-nova', sans-serif;
                text-transform: capitalize;
            }
            .culture-section p {
                margin: 36px auto;
                font-size: 18px;
                width: 75%;
            }
            .btn {
                font-size: 18px !Important;
            }
            .btn-slider-<?=$class?> {
                background-color: #656565;
                color: #fff;
            }
            img.close_btn{margin-left: 605px!important;}
            /* Popup CSS Below */
            .flexslider .slides img {
              margin: 0 auto;
              max-width: 100% !important;
            }
            .ct-sliderPop {
              border-radius:0px;
              width:1260px !important;
              height:530px !important;
            }
            .ct-sliderPop > .inner > .cow-map-img {
              margin-top: 75px;
            }
            .ct-sectional .col-lg-5.col-lg-offset-1 > img {
              cursor: pointer;
              border-radius: 50%;
            }
            .ct-sliderPop .inner {
              color: #fff;
              margin-top: 55px;
            }
            .map-white-border {
              border-bottom: 1px solid #fff;
              width: 30%;
              margin: 50px auto;
            }
            .ct-sliderPop-container {
              display: none;
              left: 0px;
              opacity: 0;
              top: 0px;
              z-index: 9998;
            }
            .ct-sliderPop-container.open {
              animation-duration: 0.35s;
              animation-fill-mode: both;
              animation-name: fadeIn;
              display: block;
            }
            .ct-sliderPop-container {
              overflow:hidden;
              max-width: 100% !important;
              left: 50%;
              position: fixed !important; /* Required to override style inline style added by Flexslider */
              text-align: center;
              top: 50%;
              transform: translate(-50%, -50%);
              z-index: 9997;
            }
            .ct-sliderPop-slide1 {
              background: #000000ab;
            }
            .ct-sliderPop .inner .ct-sliderPop-close {
              display: inline-block;
              margin: 1% auto 30px;
              transition: opacity 0.25s ease-in-out 0s;
              width: 75px;
            }
            .ct-sliderPop h1 {
              font-family: "nimbus-sans-condensed", sans-serif;
              font-size: 75px;
              font-weight: 700;
              line-height: 1;
              text-transform: uppercase;
              margin-top: 0;
            }
            .ct-sliderPop h1 span {
              font-family: 'coquette', fantasy;
              text-transform: capitalize;
              line-height: 0.8;
            }
            .ct-sliderPop h2,
            .ct-sliderPop p {
              font-family: "nimbus-sans-condensed", sans-serif;
              font-size: 30px;
              font-weight: 400;
              line-height: 1;
            }
            .ct-sliderPop.fa {
                font-size: 84px;
                margin-bottom: 10px;
            }
            .ct-sliderPop p {
              font-size: 18px;
              padding-bottom: 30px;
              width: 70%;
              line-height: 24px;
              margin: 0 auto;
            }
            .ct-sliderPop-container .flex-direction-nav a {
              overflow: visible;
            }
            .flex-direction-nav a,
            .flex-direction-nav a.flex-next::before,
            .flex-direction-nav a.flex-prev::before {
              color: #fff !important;
            }
            .flexslider .slides img {
              width: auto !important;
            }
            .sliderPop-close {
              width: 60px;
            }
            
            
            /* =================================
              # Media Queries 
            ================================= */
            @media (max-width: 1200px){
            
            }
            @media (max-width: 991px){
            
            }
            @media (max-width: 767px){
            
              .ct-sliderPop {
                height: 500px;
                max-width: 500px !important;
              }
              .ct-sliderPop > .inner > .cow-map-img {
                margin-top: 50px;
              }
              .ct-sliderPop-description span {
                font-size: 50px;
              }
              .ct-sliderPop h2,
              .ct-sliderPop-description {
                font-size: 20px;
              }
              .ct-sliderPop h2 {
                line-height: 0.2;
              }
              .ct-sliderPop-description {
                padding-bottom: 0;
              }
              .ct-sliderPop .inner {
                margin-top: 10px;
              }
              .ct-sliderPop-description .map-fontSize {
                font-size: 80px;
              }
            }
            
            @-webkit-keyframes fadeIn{0%{opacity:0}100%{opacity:1}}
            @keyframes fadeIn{0%{opacity:0}100%{opacity:1}}
        </style>
        <a class="ycsdViewAlbum btn-slider-<?=$class?>"  href="#"><i class="fa fa-image"></i>View Album</a>
        <div class="sliderPop <?=$class?>">
            <div class="ct-sliderPop-container">
                <?php 
                if(isset($image1) && !empty($image1)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1 open">
                        <div class="inner">
                            <img src="<?=$image1?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                            <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
                <?php 
                if(isset($image2) && !empty($image2)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image2?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                            <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
                <?php 
                if(isset($image3) && !empty($image3)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            
                            <img src="<?=$image3?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
                <?php 
                if(isset($image4) && !empty($image4)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            
                            <img src="<?=$image4?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                    
                <?php 
                if(isset($image5) && !empty($image5)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image5?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?> 
                
                <?php 
                if(isset($image6) && !empty($image6)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image6?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php 
                if(isset($image7) && !empty($image7)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image7?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php 
                if(isset($image8) && !empty($image8)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image8?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php 
                if(isset($image9) && !empty($image9)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image9?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php 
                if(isset($image10) && !empty($image10)){
                    ?>
                    <div class="ct-sliderPop ct-sliderPop-slide1">
                        <div class="inner">
                            <img src="<?=$image10?>" />
                            <a class="ct-newsletter-close ct-sliderPop-close" href="#">
                                <img alt="close" class="close_btn" src="/wp-content/themes/habib/images/popup-close.png">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                
            </div>
        </div>
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
        <script>
            $( document ).ready(function() {
            	$('.btn-slider-<?=$class?>').on('click', function() {
                    $('.<?=$class?>').show();
                    $('.ct-sliderPop-container').addClass('open');
                    $('.sliderPop').addClass('flexslider');
                    $('.sliderPop .ct-sliderPop-container').addClass('slides');
            
                    $('.<?=$class?>').flexslider({
                        selector: '.ct-sliderPop-container > .ct-sliderPop',
                        slideshow: true,
                        controlNav: false,
                        controlsContainer: '.ct-sliderPop-container'
                    });
            	});
        
            	$('.ct-sliderPop-close').on('click', function() {
                    $('.<?=$class?>').hide();
                    $('.<?=$class?> .ct-sliderPop-container').removeClass('open');
                    $('.sliderPop-<?=$class?>').removeClass('flexslider');
                    $('.<?=$class?> .ct-sliderPop-container').removeClass('slides');
                    $(document).keydown(function(e) {
                                                        // ESCAPE key pressed
                                                        if (e.keyCode == 27) {
                                                            $('.<?=$class?>').hide();
                                                        }
                                                    });
            	});
            });
        </script>
    <?php
}
add_shortcode( 'customslider', 'custom_slider');
//END CUSTOM SHORTCODE

/* OGE Custom Functions 
--------------------------------------------------------- */

function oge_section_scode ($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'class' => '',
		'id' => ''
	), $atts ));
	
	ob_start();
	
	?>
		<div 
		<?php if($id){ echo 'id="'.$id.'"'; } 
			  echo 'class="ogeSection '.$class.'"'; ?>
		>
			<?php echo $content; ?>
		</div>
	<?php
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'oge_section', 'oge_section_scode' );


function oge_news_event_visits ($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'type' => '',
		'image' => '',
		'link' => '',
		'title' => '',
		'class' => ''
   ), $atts ));
		
	ob_start();

	
	if($type == "news") 
	{
		$response = wp_remote_get( "https://habib.edu.pk/HU-news/wp-json/wp/v2/posts?per_page=1" );
		if( is_wp_error( $response ) ) {
			return;
		}
		$posts = json_decode( wp_remote_retrieve_body( $response ) );

		if( empty( $posts ) ) {
			return;
		}
		foreach( $posts as $post ) {
		?>
			<article class="oge_box">
				<i class="mwNewsIcon">News</i>
			<?php 
				$img = $post->acf_field->home_page_thumbnail;
				?>
				<a target="_blank" href="<?php echo $post->link; ?>">
					<img src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" />		
					<h3><?php echo $post->title->rendered; ?></h3>
				</a>
			</article>
	<?php
		//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
		}
	}
	if ($type == "event") 
	{
		$response = wp_remote_get( "https://habib.edu.pk/wp-json/wp/v2/event?per_page=1" );
		if( is_wp_error( $response ) ) {
			return;
		}
		$posts = json_decode( wp_remote_retrieve_body( $response ) );

		if( empty( $posts ) ) {
			return;
		}
		foreach( $posts as $post ) {
		?>
			<article class="oge_box">
				<i class="mwEventIcon">Events</i>
			<?php 
				$img = $post->mw_category_name->cat_img;
			?>
				<a target="_blank" href="<?php echo $post->link; ?>">
					<img src="<?php echo $img; ?>" alt="<?php echo $post->title->rendered; ?>" />
					<h3><?php echo $post->title->rendered; ?></h3>
				</a>
			</article>
	<?php
		//	echo '<div><img src="'.huFeatureImg($post->featured_media).'" /><a href="' . $post->link. '">' . $post->title->rendered . '</a></div>';
		}
	}
	if($type == "visit")
	{
	?>
		<article class="oge_box">
			<i class="mwVisitIcon">Visits</i>
			<a target="_blank" href="<?php echo $link; ?>">
				<img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" />
				<h3><?php echo $title; ?></h3>
			</a>
		</article>
	<?php	
	}
	
	if($type == "cnews")
	{
	?>
		<article class="oge_box">
			<i class="mwNewsIcon">News</i>
			<a target="_blank" href="<?php echo $link; ?>">
				<img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" />
				<h3><?php echo $title; ?></h3>
			</a>
		</article>
	<?php	
	}
	if($type == "cevent")
	{
	?>
		<article class="oge_box">
			<i class="mwEventIcon">Events</i>
			<a target="_blank" href="<?php echo $link; ?>">
				<img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" />
				<h3><?php echo $title; ?></h3>
			</a>
		</article>
	<?php	
	}
	if($type == "cbox")
	{
	?>
		<article class="oge_box <?php echo $class; ?>">
			<!-- <i class="mwEventIcon">Events</i> -->
			<a href="<?php echo $link; ?>">
				<img src="<?php echo $image; ?>" alt="<?php echo $image; ?>" />
				<h3><?php echo $title; ?></h3>
			</a>
		</article>
	<?php	
	}
	
	
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
add_shortcode( 'oge', 'oge_news_event_visits' );


function oge_testimonial_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>
	<div class="testimonial">
<!--		<div class="owl-image"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/oge_image_005.jpg" /></div>-->
		<div class="owl-carousel"><?php echo $content; ?></div>
	</div>
	<script>
      var owl = jQuery('.owl-carousel');
      owl.owlCarousel({
        margin: 0,
        loop: true,
		nav:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1
          },
          1000: {
            items: 1
          }
        }
      })
    </script>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'oge_testimonial', 'oge_testimonial_scode');


function testimonial_item_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
		'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
			<div class="owl-image"><a href="<?php echo $link; ?>" data-lity><img src="<?php echo $image; ?>"></a></div>
			<blockquote><?php echo $content; ?><h5><?php echo $from; ?></h5></blockquote>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'testimonial_item', 'testimonial_item_scode');


function oge_cal_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'title' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="oge_cal">
			<h2><?php echo $title; ?></h2>
			<ul>
				<?php echo $content; ?>
<!--
				<li><a href="http://habib.edu.pk/questioningsouthasia/" target="_blank">Habib University President’s Conference 2017 - Questioning South Asia <br><br>

					<em>Date: 22<sup>nd</sup> to 24<sup>th</sup> November, 2017</em></a></li>
-->
			</ul>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'oge_cal', 'oge_cal_scode');















/*Office of the president video boxes start*/
/*coded by Sohail*/

/*Office of the president video boxes with popup start*/
function president_office_vdo_nopop($atts, $content = null) {
	extract(shortcode_atts( array(
		'title' => '',
        'img' => '',
        'content'=>'',
        'link'=>'',
		'target'=>'_blank'
	), $atts));	
	
	ob_start();
?>	
            <a href="<?php echo $link; ?>"  target="<?php echo $target; ?>">
            <div class="main-vd-banner">
                <div class="vd-thumb">
                    <img src="<?php echo $img; ?>" />
                </div>
            <div class="vd-content">
                <h3><?php echo $title; ?></h3>
                <p><?php echo $content; ?></p>
            </div></div></a>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'pres-vdo-nopop', 'president_office_vdo_nopop');
/*Office of the president video boxes with popup end*/





/*Office of the president video boxes with popup start*/
function president_office_vdo1_nopop($atts, $content = null) {
	extract(shortcode_atts( array(
		'title' => '',
        'img' => '',
        'content'=>'',
        'link'=>'',
		'target'=>'_blank'
	), $atts));	
	
	ob_start();
?>	
            <a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><div class="main-vd-banner">
            <div class="vd-content1">
                <h3><?php echo $title; ?></h3>
                <p><?php echo $content; ?></p>
            </div>
                <div class="vd-thumb1">
                <img src="<?php echo $img; ?>" />
                </div></a>
</div>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'pres-vdo1-nopop', 'president_office_vdo1_nopop');
/*Office of the president video boxes end*/

/*ends**/

function president_office_vdo($atts, $content = null) {
	extract(shortcode_atts( array(
		'title' => '',
        'img' => '',
        'content'=>'',
        'link'=>''
	), $atts));	
	
	ob_start();
?>	
            <a href="<?php echo $link; ?>" data-lity>
            <div class="main-vd-banner">
                <div class="vd-thumb">
                    <img src="<?php echo $img; ?>" />
                </div>
            <div class="vd-content">
                <h3><?php echo $title; ?></h3>
                <p><?php echo $content; ?></p>
            </div></div></a>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'pres-vdo', 'president_office_vdo');
/*Office of the president video boxes end*/

/*Office of the president video boxes start*/
function president_office_vdo1($atts, $content = null) {
	extract(shortcode_atts( array(
		'title' => '',
        'img' => '',
        'content'=>'',
        'link'=>''
	), $atts));	
	
	ob_start();
?>	
            <a href="<?php echo $link; ?>" data-lity><div class="main-vd-banner">
            <div class="vd-content1">
                <h3><?php echo $title; ?></h3>
                <p><?php echo $content; ?></p>
            </div>
                <div class="vd-thumb1">
                <img src="<?php echo $img; ?>" />
                </div></a>
</div>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'pres-vdo1', 'president_office_vdo1');
/*Office of the president video boxes end*/












function oge_cal_item_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'link'	=> '',
		'text' => '',
		'date' => '',
	), $atts));	
	
	ob_start();
	
	//http://habib.edu.pk/questioningsouthasia/
	//Habib University President’s Conference 2017 - Questioning South Asia
	//22<sup>nd</sup> to 24<sup>th</sup> November, 2017
?>	
		<li><a href="<?php echo $link; ?>" target="_blank"><?php echo $text; ?> <br><br>
			<?php /*?><em>Date: 20 november<?php echo $date; ?></em><?php */?></a></li>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'oge_cal_item', 'oge_cal_item_scode');

function custom_spot_map_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>	
		<div class="custom_spot_map">
			<img src="<?php echo $image; ?>" alt="map" />
			<?php echo $content; ?>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'custom_spot_map', 'custom_spot_map_scode');

function spot_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'top' => '',
		'left' => ''
	), $atts));	
	
	ob_start();
?>		
<aside class="cspot" style="top:<?php echo $top; ?>; left: <?php echo $left; ?>;"><span class="marker"></span><span class="tooltip"><img src="<?php echo $image; ?>" /></span></aside>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'spot', 'spot_scode');

function custom_map_box_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>	
		<div class="custom_map_box">
			<?php echo $content; ?>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'custom_map_box', 'custom_map_box_scode');


function oapRandPosts_scode($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'section' => ''
	), $atts ));
		
	ob_start();



	$mypages = [];
	if($section == "one") {
		$mypages = array(12524,12527,12529,12533);
//		$mypages = array(12047,12061,12072,804,7890,7892,7894,7896,7898,7900,7902);
	}
	if($section == "two") {
		$mypages = array(12047,12061,12072);
//		$mypages = array(12524,12527,12529,12533,804,7890,7892,7894,7896,7898,7900,7902);
	}
	if($section == "three") {
		$mypages = array(804,7890,7892,7894,7896,7898,7900,7902);
//		$mypages = array(12524,12527,12529,12533,12047,12061,12072);
	}



$args = array(
   'orderby' => 'rand',
   'post_type' => 'page',
   'posts_per_page' => 1,
   'post__in' => $mypages
);

$rand_query = new WP_Query( $args );

while ( $rand_query->have_posts() ) : $rand_query->the_post();
   echo '<div class="oapRandBox"><img src="'.get_field('thumbnail_images').'" /><a href="' . get_permalink() . '">' . get_the_title() . '</a></div>';
endwhile;
wp_reset_postdata();

	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
	

}
add_shortcode( 'oapRandPosts', 'oapRandPosts_scode' );

/*
--- Library Shortcode Function
*/ 
function logo_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'link' => '',
		'class' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="mw_logo">
			<a href="<?php echo $link; ?>" target="_blank"><img class="<?php echo $class; ?>" src="<?php echo $image; ?>" /></a>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'logo', 'logo_scode');

function jgrid_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
        'image2'	=> '',
		'link' => '',
		'class' => '',
        'textcell' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="mwjGrid">
			<div class="mwjImage">
                <p class="mwjImagepara"><?php echo $textcell; ?></p>
				<a href="<?php echo $link; ?>" target="_blank"><img class="<?php echo $class; ?>" src="<?php echo $image; ?>" /></a>
			</div>
			<div class="mwjContent">
				<?php echo $content; ?>
			</div>			
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'jgrid', 'jgrid_scode');


//HA Open Resources Shortcode for Library Page 9-Feb-2021 Start
function lib_open_resources($atts, $content = null) {
	extract(shortcode_atts( array(
		'heading'	=> '',
        'discipline'=> '',
		'linktext' => '',
        'link' => '',
        'acc_journals' => '',
        'acc_books' => '',
        'acc_articles' => '',
        'description' => ''
	), $atts));	
	
	ob_start();
?>	
        
        <div class="col-sm-6 margin-bottom-xl">
                  <h2 class="li_heading"><?php echo $heading; ?></h2>
                  <p class="li_disabled"><i class="fa fa-book" aria-hidden="true" style="color: #000;"></i><?php echo $discipline; ?></p>
                  <p class="li_disabled" style="    width: 25%;    float: left;"><i class="fa fa-link" aria-hidden="true"></i><a target="_blank" href='<?php echo $link; ?>'>URL</a></p>
                  <?php
                  if(!empty($acc_journals)){?>
                        <p class="li_disabled"><i class="fa fa-link" aria-hidden="true"></i><a target="_blank" href='<?php echo $acc_journals; ?>'>URL to access Journal Titles</a></p>
                  <?php } ?>
                  
                  <?php
                  if(!empty($acc_books)){?>
                        <p class="li_disabled"><i class="fa fa-link" aria-hidden="true"></i><a target="_blank" href='<?php echo $acc_books; ?>'>URL to access Books</a></p>
                  <?php } ?>
                  
                  <?php
                  if(!empty($acc_articles)){?>
                        <p class="li_disabled"><i class="fa fa-link" aria-hidden="true"></i><a target="_blank" href='<?php echo $acc_articles; ?>'>URL to access Articles</a></p>
                  <?php } ?>
                  
                  <div>
                  <p style="float: right; width: 100%;"><?php echo $description; ?></p>
                  </div>
        </div>


<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'lib_open', 'lib_open_resources');
//HA Open Resources Shortcode for Library Page 9-Feb-2021 END

/*
--- Library Shortcode Function
*/ 

function fc_popup_scode($atts, $content = null) {
	extract(shortcode_atts( array(
		'id'	=> '',
		'image' => '',
		'title' => '',
		'width' => ''
	), $atts));	
	
	ob_start();
?>	

		<div id="<?php echo $id; ?>" style="background:#fff; width: <?php echo $width; ?>; padding: 20px;" class="lity-hide">
			<h2><?php echo $title; ?></h2>
			<!--<img class="fullImg" src="<?php echo $image; ?>" />-->
			<?php echo $content; ?>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'fc-popup', 'fc_popup_scode');


/*PG Slider Functionality, CENTERS Sliders Hassan*/
function slider_main_scode( $atts, $content ) {
	extract(shortcode_atts( array(
   		'class' => '',
   	), $atts ));
	ob_start();
	?>
	<div class="owl-carousel owl-theme one-col mainpage <?php echo $class; ?>"><?php echo do_shortcode($content); ?></div>
<script>
jQuery(document).ready(function() {
	jQuery('.one-col').owlCarousel({
	   startPosition:0,
		loop: false,
        rewind: true,
		margin: 0,
        autoplaySpeed:500, 
        autoplayTimeout:47000,
		autoplay: true,
		autoplayHoverPause: true,
		dots: false,
        video: true,
        nav:true,
		responsiveClass: false,
		responsive:{
               0:{
                   items:1,
                   loop:false
               }
		}
  	});	
});	
	
</script>
<?php
	$mwzone = ob_get_contents();
	ob_end_clean();
	
	return $mwzone;
}
add_shortcode( 'slider_main', 'slider_main_scode' );
 

function studentlife_inner($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>
	<div class="slier_stdlife">
		<div class="owl-carousel"><?php echo $content; ?></div>
	</div>
	<script>
      var owl = jQuery('.owl-carousel');
      owl.owlCarousel({
        margin: 0,
        loop: false,
		nav:true,
        autoplay:true,
        responsive: {
          0: {
            items: 6
          },
          600: {
            items: 6
          },
          1000: {
            items: 6
          },
          1400: {
            items: 0
          }
        }
      })
    </script>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'std_life_inner', 'studentlife_inner');


function slider_item_stdlife($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'bgimage' => '',
	    'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
    		<div class="owl-image">
                <div class="badge_comite">
                    <a href="#<?php echo $link; ?>" data-lity> <img class="img_icon" src="<?php echo $image; ?>"/><h2><?php echo $from; ?></h2></a>
                </div> 
            </div>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'slide_item', 'slider_item_stdlife');

function slider_item_stdlife1($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'bgimage' => '',
	    'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
    		<div class="owl-image">
                <div class="badge_comite">
                    <img class="img_icon" src="<?php echo $image; ?>"/><h2><?php echo $from; ?></h2>
                </div> 
            </div>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'slide_item1', 'slider_item_stdlife1');

function student_life_section ($atts, $content = null ) {
	extract(shortcode_atts( array(
   		'class' => '',
		'id' => ''
	), $atts ));
	
	ob_start();
	
	?>
		<div 
		<?php if($id){ echo 'id="'.$id.'"'; } 
  			echo 'class=" '.$class.'"'; ?>>
			<?php echo $content; ?>
		</div>
	<?php
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'std_life_outer', 'student_life_section' ); 

// students affair courosel
function student_affair_inner($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>
	<div class="slier_stdlife">
		<div class="owl-carousel"><?php echo $content; ?></div>
	</div>
	<script>
      var owl = jQuery('.owl-carousel');
      owl.owlCarousel({
        margin: 0,
        loop: true,
		nav:true,
        autoplay:true,
        responsive: {
          0: {
            items: 3
          },
          600: {
            items: 3
          },
          1000: {
            items: 3
          }
        }
      })
    </script>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'std_affair_inner', 'student_affair_inner');
// students affair courosel

function student_senator_inner($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>
	<div class="slier_stdlife">
		<div class="owl-carousel"><?php echo $content; ?></div>
	</div>
	<script>
      var owl = jQuery('.owl-carousel');
      owl.owlCarousel({
        margin: 0,
        loop: false,
		nav:true,
        autoplay:true,
        responsive: {
          0: {
            items: 3
          },
          600: {
            items: 3
          },
          1000: {
            items: 3
          }
        }
      })
    </script>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'senator_inner', 'student_senator_inner');


function sponser_logo($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> ''
	), $atts));	
	
	ob_start();
?>
	<div class="slier_stdlife">
		<div class="owl-carousel"><?php echo $content; ?></div>
	</div>
	<script>
      var owl = jQuery('.owl-carousel');
      owl.owlCarousel({
        margin: 0,
        loop: false,
		nav:true,
        autoplay:true,
        responsive: {
          0: {
            items: 8
          },
          600: {
            items: 8
          },
          1000: {
            items: 8
          }
        }
      })
    </script>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'sponser_logo', 'sponser_logo');

// Student affair
function slider_item_std_affair($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'description'=>'',
        'heading' => '',
        'date' => '',
        'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
		<div class="owl-image">
        <a data-lity class="stdaffairs_links" href="<?php echo $link; ?>"><div class="senetor_section_std_affair">
            <img class="senator_img_std_affair" src="<?php echo $image; ?>"/>
             <h2 class="std_affairs_h2"><?php echo $description; ?></h2>
             <p class="std_aff_dates"><?php echo $date; ?></p>
           </div></a></div>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'slide_item_stdaffair', 'slider_item_std_affair');
// Student affair

//Apply for jobs positng
function jobs($atts, $content = null) { extract(shortcode_atts( array(
		'title'	=> '',
		'desc' => '',
        'jd'=>'',
        'date'=>''
        
	), $atts));	
	
	ob_start();
?>	
<h3><?php echo $title; ?></h3>
<p><?php echo $desc; ?></p>
<div class="jobs_buttons">
<!--<span class="jdate tooltip" data-tip="Apply before">
<i class="fa fa-calendar"></i><?php echo $date; ?></span>-->
<a data-lity class="jjd tooltip" href="<?php echo $jd; ?>" 
target="_blank" rel="noopener" data-tip="View Job Description">JD</a><a class="japply tooltip" href="/hu-careers/apply-online/" data-tip="Apply for the position">Apply</a> [addtoany] </div> <hr/>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'jobpost', 'jobs');
//Apply for jobs posting

/* faculty add shortcode in a format OLIN*/
function add_facultyv2($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'designation' => '',
        'school'=>'',
        'email'=>'',
        'ext'=>''                
	), $atts));	
	
	ob_start();
?>	
		<div class="facv2-prof-main-lft">
        <div class="facv2-prof-inner"><img src="<?php echo $image;?>"/></div>
        <div class="facv2-prof-main-rgt"><?php echo $designation;?>
        <div class="facv2-school"><?php echo $school;?></div>
        <div class="facv2-em"><i class="fa fa-envelope" aria-hidden="true" style="color: #fff;"></i><a class="media-cent-prof" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
        <?php if(!empty($ext)){ echo '<div class="facv2-tel">
        <i class="fa fa-phone-alt hidden" aria-hidden="true" style="color: #fff;"></i>
        <a class="media-cent-prof" href="tel:<?php echo $ext; ?>"><?php echo $ext; ?></a></div>';}?>
        </div></div>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'add_facultyv2', 'add_facultyv2');
/* faculty add shortcode in a format OLIN*/


function add_facv2_col2($atts, $content = null) {
	extract(shortcode_atts( array(
		'content'	=> '',
        'title'=>''                
	), $atts));	
	
	ob_start();
?>	

<div class="facv2-prof-main-col2"><div class="col2_title"><?php echo $title?></div><?php echo $content;?></div>
    
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'add_facv2_col2', 'add_facv2_col2');

function fac_visit_btn($atts, $content = null) {
	extract(shortcode_atts( array(
		'text'	=> '',
        'link'=>''                
	), $atts));	
	
	ob_start();
?>	

<a class="fac-btn-link" target="_blank" href="<?php echo $link;?>"><i class="fas fa-globe" aria-hidden="true" style="color: #fff;"></i>
<?php echo $text;?></a>
    
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'fac_visit_btn', 'fac_visit_btn');

/* faculty add shortcode in a format*/
function add_faculty($atts, $content = null) {
	extract(shortcode_atts( array(
		'image'	=> '',
		'designation' => '',
        'school'=>'',
        'email'=>'',
        'ext'=>''
	), $atts));	
	
	ob_start();
?>	
			<div class="facv2-prof-main-lft">
        <div class="facv2-prof-inner"><img src="<?php echo $image;?>"/></div>
        <div class="facv2-prof-main-rgt"><?php echo $designation;?>
        <div class="facv2-school"><?php echo $school;?></div>
        <div class="facv2-em"><i class="fas fa-envelope" aria-hidden="true" style="color: #fff;"></i><a class="media-cent-prof" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
        <div class="facv2-tel hidden"><i class="fas fa-phone-alt" aria-hidden="true" style="color: #fff;"></i><a class="media-cent-prof" href="tel:<?php echo $ext; ?>"><?php echo $ext; ?></a></div>                
        </div></div>

<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'add_faculty', 'add_faculty');
/* faculty add shortcode in a format*/

function slider_item_senator($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'description'=>'',
        'heading' => '',
        'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
		<div class="owl-image">
        <div class="senetor_section">
        <h3><?php echo $heading; ?></h3>
            <img class="senator_img" src="<?php echo $image; ?>"/>
             <h4><?php echo $description; ?></h4>
           </div></div>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'slide_item_senator', 'slider_item_senator');

function slide_item_logo($atts, $content = null) {
	extract(shortcode_atts( array(
		'image' => '',
        'link' => ''
	), $atts));	
	
	ob_start();
?>	
		<div class="item">
		<div class="owl-image">
        <div class="std-life-logo">
            <img class="stdlife-logo_img" src="<?php echo $image; ?>"/>
             <h4><?php echo $description; ?></h4>
           </div></div>
		</div>
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'slide_item_logo', 'slide_item_logo');


//ENABLE DISABLE XMLRPC
//add_filter( 'xmlrpc_enabled', '__return_false' );

// Disable X-Pingback to header
add_filter( 'wp_headers', 'disable_x_pingback' );
function disable_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );

return $headers;
}

/*********home page 4 cols section footer*****************/
function homecols($atts, $content = null) {  
    extract(shortcode_atts(array(  
		"title" => '',
		"color" => '',
		"bg" => '',
		"link" => '',
		"position" => '',
		"width" => '',
		"target" => ''
		), $atts));  
		$option = '';
		if(!empty($target)) {
			$option = ' target="_'.$target.'"';
		}
    //return '<a href="'.$to.'">'.$content.'</a>'; 
	return '<div class="center_section">
    <div class="homeNewsEvents_h">
    <div class="hucontent_h">
    <h2 class="col_heading">GLOBAL</h2>'.do_shortcode('[huNewsByApi_h num="3"]' ).'
<div class="btn_of_cols"><a href="/HU-news/category/office-for-global-engagement/" target="_blank" rel="noopener"><span class="col_btm_btn">VIEW MORE STORIES</span></a></div>
</div>
<div class="hucontent_h_col_2">
<h2 class="col_heading">STUDENT ACHIEVEMENTS</h2>'.do_shortcode('[hu_cs_cat num="3"]' ).'

<div class="btn_of_cols"><a href="/HU-news/category/student-work/" target="_blank" rel="noopener"> <span class="col_btm_btn">VIEW STUDENT ACHIEVEMENTS</span></a></div>
</div>
<div class="hucontent_h_col_3">
<h2 class="col_heading">MOHSINEEN</h2>
<div class="">
<a href="/HU-news/transforming-higher-education-through-philanthropy/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/mohsineen-philanthropy-thumb.jpg" />
<span class="tooltiptext">Transforming Higher Education through Philanthropy</span></a></div>
</div>
<div class="art_content_3">
<h3>Transforming Higher Education through Philanthropy</h3>
</div>
<div class="">
<a href="/HU-news/habib-university-foundation-holds-fundraising-gala-in-houston-texas/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/mohsineen-houston-thumb.jpg" />
<span class="tooltiptext">Habib University Foundation Holds Fundraising Gala in Houston, Texas</span></a></div>
</div>
<div class="art_content_3">
<h3>Habib University Foundation Holds Fundraising Gala in Houston, Texas</h3>
</div>
<div class="">
<a href="/HU-news/creating-a-legacy-unveiling-the-wall-of-mohsineen/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/mohsineen-wall-thumb.jpg" />
<span class="tooltiptext">Creating a Legacy: Unveiling The Wall of Mohsineen</span></div>
</div>
<div class="art_content_3">
<h3>Creating a Legacy: Unveiling The Wall of Mohsineen</h3>
</div>
<div class="btn_of_cols_3"><a href="/HU-news/category/donors/" target="_blank" rel="noopener"><span class="col_btm_btn">View More Stories</span></a></div>
</div>
<div class="hucontent_h_col_4">
<h2 class="col_heading">short videos</h2>
<!--VideoBOXStart -->
<div class="video-thumbnail">
<a href="https://www.facebook.com/HabibUniversity/videos/377535579803160/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-cls-thumb.jpg" />
<span class="tooltiptext">Comparative Liberal Studies</span></a></div>
</div>
<div class="art_content_4">
<h3>Comparative Liberal Studies</h3>
</div>
<!--VideoBOXEnd -->

<div class="video-thumbnail">
<a href="https://www.facebook.com/HabibUniversity/videos/388720728484499/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-orientation-thumb.jpg" />
<span class="tooltiptext">New Student Orientation 2019</span></a></div>
</div>
<div class="art_content_4">
<h3>New Student Orientation 2019</h3>
</div>
<div class="video-thumbnail">
<a href="https://www.facebook.com/HabibUniversity/videos/383614388969562/" target="_blank">
<div class="tooltip_h"><img src="/wp-content/uploads/2019/10/video-parents-thumb.jpg" />
<span class="tooltiptext">Parents Night 2019</span></a></div>
</div>
<div class="art_content_4">
<h3>Parents Night 2019</h3>
</div>
<div class="btn_of_cols_3"><a href="https://www.facebook.com/pg/HabibUniversity/videos/" target="_blank" rel="noopener"> <span class="col_btm_btn">VIEW MORE SHORT VIDEOS</span></a></div>
</div>
</div>
</div>
</div>';  
} 
add_shortcode("colshome", "homecols");
/*********home page 4 cols section footer*****************/

function defer_parsing_of_js ( $url ) {
if ( FALSE === strpos( $url, '.js' ) ) return $url;
if ( strpos( $url, 'jquery.js' ) ) return $url;
return "$url' defer ";
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );

//add_action('after_setup_theme','remove_core_updates');
//function remove_core_updates()
//{
// if(! current_user_can('update_core')){return;}
// add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
// add_filter('pre_option_update_core','__return_null');
// add_filter('pre_site_transient_update_core','__return_null');
//}

function the_breadcrumbs() {
 
        global $post;
 
        if (!is_home()) {
         if (is_category() || is_single()) {
 
               
                $cats = get_the_category( $post->ID );
 
                foreach ( $cats as $cat ){
                    echo $cat->cat_name;
                  
                }
                if (is_single()) {
                    the_title();
                }
            } elseif (is_page()) {
 
                if($post->post_parent){
                    $anc = get_post_ancestors( $post->ID );
                    $anc_link = get_page_link( $post->post_parent );
 
                    foreach ( $anc as $ancestor ) {
                        $output = " <a href=".$anc_link.">".get_the_title($ancestor)."</a> > ";
                    }
 
                    echo $output;
                    the_title();
 
                } else {
                    echo ' > ';
                    echo the_title();
                }
            }
        }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"Archive: "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"Archive: "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"Archive: "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"Author's archive: "; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "Blogarchive: "; echo'';}
    elseif (is_search()) {echo"Search results: "; }
}

function get_breadcrumb() {
    echo '<ul class="breadcrumb-ul"><li class="breadcrumb-li-first home"><a href="'.home_url().'" rel="nofollow"><i class="fa fa-home" style="font-size:18px"></i></a></li>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        // echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
		global $post,$wp_query;
		$separator = '&gt;';
		if( $post->post_parent ){               
		$anc = get_post_ancestors( $post->ID );      // If child page, get parents 
		$anc = array_reverse($anc);    // Get parents in the right order
		if ( !isset( $parents ) ) $parents = null;  // Parent page loop
			foreach ( $anc as $ancestor ) {
				$parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
				$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
			}
			echo $parents;   // Display parent pages
	// Current page
		echo '<li class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></li></ul>';
		} else {
	// Just display current page if not parents
		echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li></ul>';
		}

    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

function senate_popup($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'description'=>'',
        'heading' => '',
        'description2'=>'',
        'heading2' => '',        
        'id' => '',
        'link' => ''
	), $atts));	
	
	ob_start();
?>	
            <div class="committee_popup lity-hide" id="<?=$id?>"> 
                <div class="committee_border"> 
                    <div class="img_popup">
                        <img class="pop_icon" src="<?php echo $image; ?>"/>            
                    </div>
                    <div class="desc_popup">
                        <h2>Purpose:</h2>
                        <p class="std_life p"><?=$description?></p>
                        <h2>Chair:</h2>   
                        <p class="std_life p"><?=$description2?></p>            
                    </div>
                </div>
            </div>
 		
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'std_popup', 'senate_popup');

//Non Standing Comet Student Life:

function senate_popup1($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		'image' => '',
        'description'=>'',
        'heading' => '',
        'description2'=>'',
        'heading2' => '',        
        'id' => '',
        'link' => ''
	), $atts));	
	
	ob_start();
?>	
            <div class="committee_popup lity-hide" id="<?=$id?>"> 
                <div class="committee_border"> 
                    <div class="img_popup">
                        <img class="pop_icon" src="<?php echo $image; ?>"/>            
                    </div>
                    <div class="desc_popup">
                        <h2>Purpose:</h2>
                        <p class="std_life p"><?=$description?></p>
                                 
                    </div>
                </div>
            </div>
 		
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'nsc', 'senate_popup1');


function stdlifevent($atts, $content = null) {
	extract(shortcode_atts( array(
		'from'	=> '',
		
	), $atts));	
	
	ob_start();
?>	
            
            <?php
			$events = EM_Events::get(array('scope'=>'all', 'category'=> 'studentlife', 'orderby'=>'event_start_date', 'limit'=>5, 'pagination'=> 1));
			if(!empty($events)) {
		?>	
        <div class="std-life-func">
            	<?php
					foreach( $events as $key=>$EM_Event  ){ ?>
					<?php 
						$today = date("Ymd");
						$eventDate = $EM_Event->output("#_{Ymd}");
						if($today < $eventDate) {
							$eventClass = "futureEvent";
						} elseif($today > $eventDate) {
							$eventClass = "pastEvent";
						} elseif($today == $eventDate) {
							$eventClass = "todayEvent";
						}
					?>
                        <article class=" <?php echo $eventClass; ?>">
                        	<a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>"></a>
                                                      
                            <div class="life-fw">
                            <div class="life-eve">
                            <h2 class="lif-month"><?php echo string_limit_words($EM_Event->output("#_{M}"), 20); ?></h2>
                            <h2 class="life-date"><?php echo string_limit_words($EM_Event->output("#_{d}"), 20); ?></h2>
                            <h2 class="life-year"><?php echo string_limit_words($EM_Event->output("#_{Y}"), 20); ?></h2>
                            </div>
                            <div class="life-eve2"><h2 class="eve2h2"><a href="<?php echo $EM_Event->output("#_EVENTURL"); ?>" title="<?php echo $EM_Event->output("#_EVENTNAME"); ?>"><?php echo $EM_Event->output("#_EVENTNAME"); ?></a></h2>
                            <date><?php echo $EM_Event->output("#_12HSTARTTIME");?></date>
                            <p><?php echo string_limit_words($EM_Event->output("#_EVENTEXCERPT{10}"), 20); ?></p>
                            </div></div>
                            
                            <div class="">
                       <?php 
                                   if($EM_Event->output('#_ATT{Album}')) {                                         
                                       ?>
                                      <link rel="stylesheet" href="https://habib.edu.pk/wp-content/themes/habib/css/lity.min.css" />
                                       
                                           <?php 
                                               $shortcode = ''.$EM_Event->output('#_ATT{Album}').''; 
                                               echo do_shortcode( $shortcode );
                                           ?>
                                       
                                       <?php
                                   } 
                               ?>
                              <?php if($EM_Event->output('#_ATT{Poster}')) { ?><?php } ?>
                               
                            </div>
                        </article>
				<?php
					}
				?>  
                <?php } ?>
            
 		
<?php		
	$mwzone = ob_get_contents();
	ob_end_clean();
	$mwzone = do_shortcode($mwzone);
	return $mwzone;
}
add_shortcode( 'stdlifevent', 'stdlifevent');


?>

<?php
if( is_admin() ) {
 
    add_filter( 'script_loader_tag', 'tp_theme_filter_overrides' , 10, 4 );
 
}
 
function tp_theme_filter_overrides( $tag, $handle ) {
 
    return str_replace( 'defer', '', $tag );
 
}



// changes on 24 june 2021

// function lt_html_excerpt($text) { // Fakes an excerpt if needed
//     global $post;

//     if ('' == $text ) {

//         $text = get_the_content('');
//         $text = apply_filters('the_content', $text);
//         $text = str_replace('\]\]\>', ']]&gt;', $text);
//         $text = str_replace('Continue reading','', $text);
//         /*just add all the tags you want to appear in the excerpt --
//         be sure there are no white spaces in the string of allowed tags */
//         $text = strip_tags($text,'<p><br><b><a><em><strong>');
//         /* you can also change the length of the excerpt here, if you want */
//         $excerpt_length = 1000000000; 
//         $words = explode(' ', $text, $excerpt_length + 1);
//         if (count($words)> $excerpt_length) {
//             array_pop($words);
//             array_push($words, '[...]');
//             $text = implode(' ', $words);
//         }
//     }
//     return $text;
// }

// add_filter('get_the_excerpt', 'lt_html_excerpt');

add_filter('the_excerpt', 'do_shortcode');



// function my_excerpt_length($length) {
// 	return 1000;
// }
// add_filter('excerpt_length', 'my_excerpt_length');



// add_image_size( 'post-thumbnails', 940, 450, true );
// add_theme_support('post-thumbnails');
			
add_action('template_redirect', 'redirect_to_home_from_about_page');
function redirect_to_home_from_about_page() {
  if( is_page('giving')) {
      wp_redirect('https://giving.habib.edu.pk/');
    exit();
  }
}


add_action('template_redirect', 'redirect_to_home_from_president_page');
function redirect_to_home_from_president_page() {
  if( is_page('office-of-the-president')) {
      wp_redirect('https://habib.edu.pk/about-us/office-president/');
    exit();
  }
}



// This add wp_nav_menu() in two locations. admission rgulual and hutops in admission sep template
register_nav_menus( array(  
	'admission-sep-regular' => __( 'Admission sep Regular', 'habib' ),  
	'admission-sep-hutops' => __('Admission sep HUTOPS', 'habib')  
  ) );


// main tab shortcode function

  $main_tabs_divs = '';
  function main_tabs_group($main_atts, $main_content = null ) {
	  global $main_tabs_divs;
	  $main_tabs_divs = '';
	  $main_h_id = 'main-h-'.rand(1,10);
	  $main_output = '<div id="' . $main_h_id . '" class="arconix-tabs-horizontal"><ul class="arconix-tabs"';
	  $main_output.='>'.do_shortcode($main_content).'</ul>';
	  $main_output.= '<div class="arconix-panes">'.$main_tabs_divs.'</div></div>';
	  return $main_output;  
  }
  function main_tab($main_atts, $main_content = null) {  
	//$main_content = str_get_html($main_content);
	  global $main_tabs_divs;
	  extract(shortcode_atts(array(  
		  'id' => '',
		  'title' => '', 
	  ), $main_atts));  
	  if(empty($main_id))
		  // $s_id = 'side-tab'.rand(100,999);
			  $main_id = 'title'.rand(21,50);
	  $main_output = '
		  <li class="arconix-tab tab-' . $main_id . '">
			  <a id="tab-' . $main_id . '" class="" href="#'.$main_id.'">'.$title.'</a>
		  </li>';
		  $main_tabs_divs.= '<div id="pane-tab-'.$main_id.'" class="arconix-pane pane-' . $main_id . '">'. do_shortcode( $main_content ) .'</div>';
	 // $main_tabs_divs.= '<div id="pane-tab-'.$main_id.'" class="arconix-pane pane-' . $main_id . '">' . remove_wpautop( $main_content ) .'</div>';

	  return $main_output;
  }
  add_shortcode('main-tabs', 'main_tabs_group');
  add_shortcode('main-tab', 'main_tab');
  



  // inner tab shortcode function
$tabs_divs = '';
function s_tabs_group($s_atts, $s_content = null ) {
    global $tabs_divs;
	$tabs_divs = '';
    $s_output = '<div class="arconix-tabs-vertical"><ul class="v-arconix-tabs"';
    $s_output.='>'.do_shortcode($s_content).'</ul>';
    $s_output.= '<div class="v-arconix-panes">'.$tabs_divs.'</div></div>';
    return $s_output;  
}  
function s_tab($s_atts, $s_content = null) {  
    global $tabs_divs;
    extract(shortcode_atts(array(  
        'id' => '',
        'title' => '', 
    ), $s_atts));  
    if(empty($s_id))
        // $s_id = 'side-tab'.rand(100,999);
			$s_id = 'title'.rand(11,99);
    $s_output = '
        <li class="v-arconix-tab tab-' . $s_id . '">
            <a id="tab-' . $s_id . '" class="" href="#'.$s_id.'">'.$title.'</a>
        </li>';
    $tabs_divs.= '<div id="v-pane-tab-'.$s_id.'" class="v-arconix-pane pane-' . $s_id . '">'. do_shortcode( $s_content ) .'</div>';
    return $s_output;
}
add_shortcode('inner-tabs', 's_tabs_group');
add_shortcode('inner-tab', 's_tab');

  



// function sha_gallery_func($atts) {
// 	extract( shortcode_atts( array(
// 	 'sha_gallery_link' => '',
//  ), $atts ) );

//  $id = get_the_id();
//  $sha_gallery_link_var = get_field($sha_gallery_link, $id );

//  if ( $sha_gallery_link_var == '') return; 

//  $output = "<h4><strong>{$sha_gallery_link_var} </strong>null</h4>";

//  return $output;
// }
// add_shortcode( 'sha-gallery', 'sha_gallery_func');
//[sha-gallery sha_gallery_link="A hu"] a




add_action('wp', function(){
    remove_action('wp_head', 'rsd_link');
}, 11);

add_filter('bloginfo_url', function($output, $property){
	error_log("====property=" . $property);
	return ($property == 'pingback_url') ? null : $output;
  }, 11, 2);

?> 


